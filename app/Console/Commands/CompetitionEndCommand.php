<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Modals\Competition; 
use App\Modals\CompetitionUserJudge; 
use App\Modals\CompetitionUser; 
use App\Modals\Taxonomie;
use DB;
class CompetitionEndCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'competition:end';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // echo 'dfsd';
        $competition = Competition::where('is_active',1)->where('comopetition_by_type','auto')->get();
        if(count($competition) > 0){
            foreach ($competition as $key => $value) {
                $to = \Carbon\Carbon::now();
                $from = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->end_at);
                // dd($to);
                $diff_in_minutes = $to->diffInMinutes($from);
                // echo $to;
                // echo $diff_in_minutes;exit;
                if($diff_in_minutes < 5){

                    $competition = Competition::where('id',$value->id)->update(['is_active'=>0]);
                    foreach ($value->competitionDetail as $key => $data) {
                        $data->userContent->update(['is_competition'=>0]);
                        $data->userContent->userTier->update(['is_competition'=>0]);
                        if($key == 0){
                            $competition_user_winner  =  DB::table(DB::raw('(select count("competition_user_id") as winner,competition_user_id
                                                 from competition_user_judge
                                                  where competition_id = '.$value->id.' and expression = 1 GROUP BY competition_user_id
                                                ) AS temp '))->select(DB::raw( '
                                             Max(winner),temp.competition_user_id'))->groupBy('competition_user_id')->first();
                            if($competition_user_winner){
                                $c_id = $competition_user_winner->competition_user_id;
                                $competition_user = CompetitionUser::where('id',$c_id)->with('userContent.userTier')->first();
                                $id = (int)$competition_user->userContent->userTier->taxonomie_id + (int)1;
                                $taxnomie = Taxonomie::where('id',$id)->first();
                                $user_level = (int)$taxnomie->level - (int)1;
                                if($user_level < 10){
                                   $competition_user->userContent->userTier->update(['taxonomie_id'=>$taxnomie->id]);
                                   echo 'done';
                                }  
                            } 
                        }
                    }
                    
                }else{
                    echo 'time left';
                }
            }
        }

    }
}
