<?php

use App\Modals\CompetitorSet;
use App\Modals\Country;
use App\Modals\Taxonomie;
use App\Modals\UserDetail;
use Google\Cloud\Vision\V1\ImageAnnotatorClient;
use Twilio\Rest\Client;
// use Exception;
// $path = 'gs://path/to/your/image.jpg'

function detect_safe_search_gcs($path)
{
    $cre = '{
              "type": "service_account",
              "project_id": "livegong",
              "private_key_id": "1a8227930032454c6b77b1963ffa83019f52a0e3",
              "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC8OAdumbXTEGZg\nClASg647gw/x9mi3qRE5A4pgG8LjXttXXW1Qo0IjQij9Fjo4D0wrrXpim/vYCThO\n97ef54b3iWkXbCW3P1WXunpyCs+2oSolTXRMIrjiLjTJu6R63An9MsFGZ9EpaUU2\nr0PwQU4Ca0qt2YwDu3O4GOKUfa312evbJMfQkuOQYxjLXREYy6UZz7jdha+fbQ8z\nQWbnxFe4o8bEu3FjkM+vWMq7+Cv4Ua8o4vcMMelCJrToGAhG3M2NCgexlwYFX74n\njj/7Zm45cphfSQXgSb9u2VG3qbPZjRS50C7cWQwKc+U9QGzh7VZhHptr06oy0WTO\nMpiylBh/AgMBAAECggEAQZndrUTRxMDAjdTKV8mhnohttE3H7ct1f0OmpRzH7muP\nU4HyqdpdIODcXGm03Pkn+Uk7kMcBHikIgS2ByK/vsLMp98nv01C1ym5qYvk1sFe3\n0syOLkMj4WFtcKqAB3pN2PwptRpWInLYdtjbew+n2tv+aV2bmWlqSc4SmuK7ESbK\nmUhDxXFTu0gY2c/85zrnRkGihCST/3XOOeLTCRs+4Au+E88KZjTW6WbUV9q52dH2\nTMSsUDKju6mT/WGWyPgIXjX/ajX4VVhuAfD4gfb2+GXaRXeHMI8kBjcHBKl3RvOG\ngmBbQyLcCQ6Vfcrr0NyDWpoYrd5SPi23ts9zdmEpgQKBgQD9rEw34biSYk0R8y2N\n2IPehIUK+iYpvlxeeJ+ydb9I0nnmh3P+eytOwbmYqR6hDXd2XkgAPQyICE6OTvQI\nQp9uh59zPGEBX3812pfiJeMRY3S7QpYod/SrOT8nmkpnJPrryrikfV+CXpnOaFY7\ndBupl83cVpRXqRYngxkzdYsYnwKBgQC98gZYDlsTx2fRKXPTPG1cGYG4sdL1Npo7\nO90pP/yV6WJnoU3Vss+l1790E+Qrq6rErKAycgBrIUNR8BH0SY9IT+tQBeIivIHt\nNUgE0G5N+2SPobAwrYA4ktBTz1F+Jdi6VxZPzoJ6uLYKOvcKiqnL6HpsdlZXWgK4\nudw+pFaUIQKBgQDFlD6lQ91DvubydSrlEV/cn/EUFLRGzSsate8Ey+taJ/2kTZpN\n+tdHd2I8gnUF0fxbKKIB9YreJUIyW1J9YI7XQzExSzTvIdduNb0Y+pN6hrFz7TAR\ngyUBfAwxp14OKmJ55yKbk78hRtebSe7oTc4B8Of5l0HhaSSFT/n/96yIVwKBgQCS\nGCAnBEurB9li+BYdJ8IZb4jl7OgMMQYpfx7ZBl0ZVNuaR2fdcN3jefDIsC/0IuKC\nyR+iDSsjOv9Te2nj0dxByy57azsGaqUoNK4C/emJU02CZ+NjXV9cpBdWDwRoGWX7\n1NXVZknXaFCjx7yRQBvfOR1IZTB9mT60pEexaqco4QKBgC1iiapmGmnMhFeNqj1R\n0fvt5j6b7TnAXSoQR5QJh5ckTzbH0YFabzKSb5zLmdcbmo/a1qL9MYy8WXTgMpXw\njJn70REmt9uiZGP0fLwGYhQx05QQZr5VowWXeJf4kfUVkwtI5dQ0ssX/CH8b11u1\nxJkwliaG/ElSefKU9CTfQB2Z\n-----END PRIVATE KEY-----\n",
              "client_email": "livegong@appspot.gserviceaccount.com",
              "client_id": "110426010858303348973",
              "auth_uri": "https://accounts.google.com/o/oauth2/auth",
              "token_uri": "https://oauth2.googleapis.com/token",
              "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
              "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/livegong%40appspot.gserviceaccount.com"
            }';
    $imageAnnotator = new ImageAnnotatorClient([
        'credentials' => json_decode($cre, true),
    ]);

    # annotate the image
    $response = $imageAnnotator->safeSearchDetection($path);
    $safe = $response->getSafeSearchAnnotation();

    if ($safe) {
        $adult = $safe->getAdult();
        // $medical = $safe->getMedical();
        // $spoof = $safe->getSpoof();
        // $violence = $safe->getViolence();
        // $racy = $safe->getRacy();

        # names of likelihood from google.cloud.vision.enums
        $likelihoodName = ['UNKNOWN', 'VERY_UNLIKELY', 'UNLIKELY',
            'POSSIBLE', 'LIKELY', 'VERY_LIKELY'];

        if ($likelihoodName[$adult] == 'LIKELY' || $likelihoodName[$adult] == 'VERY_LIKELY') {
            return 1;
        } else {
            return 0;
        }
    } else {
        return 0;
    }

    $imageAnnotator->close();
}

function detect_safe_search_local_img($path, $flg)
{
    $cre = '{
              "type": "service_account",
              "project_id": "livegong",
              "private_key_id": "1a8227930032454c6b77b1963ffa83019f52a0e3",
              "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC8OAdumbXTEGZg\nClASg647gw/x9mi3qRE5A4pgG8LjXttXXW1Qo0IjQij9Fjo4D0wrrXpim/vYCThO\n97ef54b3iWkXbCW3P1WXunpyCs+2oSolTXRMIrjiLjTJu6R63An9MsFGZ9EpaUU2\nr0PwQU4Ca0qt2YwDu3O4GOKUfa312evbJMfQkuOQYxjLXREYy6UZz7jdha+fbQ8z\nQWbnxFe4o8bEu3FjkM+vWMq7+Cv4Ua8o4vcMMelCJrToGAhG3M2NCgexlwYFX74n\njj/7Zm45cphfSQXgSb9u2VG3qbPZjRS50C7cWQwKc+U9QGzh7VZhHptr06oy0WTO\nMpiylBh/AgMBAAECggEAQZndrUTRxMDAjdTKV8mhnohttE3H7ct1f0OmpRzH7muP\nU4HyqdpdIODcXGm03Pkn+Uk7kMcBHikIgS2ByK/vsLMp98nv01C1ym5qYvk1sFe3\n0syOLkMj4WFtcKqAB3pN2PwptRpWInLYdtjbew+n2tv+aV2bmWlqSc4SmuK7ESbK\nmUhDxXFTu0gY2c/85zrnRkGihCST/3XOOeLTCRs+4Au+E88KZjTW6WbUV9q52dH2\nTMSsUDKju6mT/WGWyPgIXjX/ajX4VVhuAfD4gfb2+GXaRXeHMI8kBjcHBKl3RvOG\ngmBbQyLcCQ6Vfcrr0NyDWpoYrd5SPi23ts9zdmEpgQKBgQD9rEw34biSYk0R8y2N\n2IPehIUK+iYpvlxeeJ+ydb9I0nnmh3P+eytOwbmYqR6hDXd2XkgAPQyICE6OTvQI\nQp9uh59zPGEBX3812pfiJeMRY3S7QpYod/SrOT8nmkpnJPrryrikfV+CXpnOaFY7\ndBupl83cVpRXqRYngxkzdYsYnwKBgQC98gZYDlsTx2fRKXPTPG1cGYG4sdL1Npo7\nO90pP/yV6WJnoU3Vss+l1790E+Qrq6rErKAycgBrIUNR8BH0SY9IT+tQBeIivIHt\nNUgE0G5N+2SPobAwrYA4ktBTz1F+Jdi6VxZPzoJ6uLYKOvcKiqnL6HpsdlZXWgK4\nudw+pFaUIQKBgQDFlD6lQ91DvubydSrlEV/cn/EUFLRGzSsate8Ey+taJ/2kTZpN\n+tdHd2I8gnUF0fxbKKIB9YreJUIyW1J9YI7XQzExSzTvIdduNb0Y+pN6hrFz7TAR\ngyUBfAwxp14OKmJ55yKbk78hRtebSe7oTc4B8Of5l0HhaSSFT/n/96yIVwKBgQCS\nGCAnBEurB9li+BYdJ8IZb4jl7OgMMQYpfx7ZBl0ZVNuaR2fdcN3jefDIsC/0IuKC\nyR+iDSsjOv9Te2nj0dxByy57azsGaqUoNK4C/emJU02CZ+NjXV9cpBdWDwRoGWX7\n1NXVZknXaFCjx7yRQBvfOR1IZTB9mT60pEexaqco4QKBgC1iiapmGmnMhFeNqj1R\n0fvt5j6b7TnAXSoQR5QJh5ckTzbH0YFabzKSb5zLmdcbmo/a1qL9MYy8WXTgMpXw\njJn70REmt9uiZGP0fLwGYhQx05QQZr5VowWXeJf4kfUVkwtI5dQ0ssX/CH8b11u1\nxJkwliaG/ElSefKU9CTfQB2Z\n-----END PRIVATE KEY-----\n",
              "client_email": "livegong@appspot.gserviceaccount.com",
              "client_id": "110426010858303348973",
              "auth_uri": "https://accounts.google.com/o/oauth2/auth",
              "token_uri": "https://oauth2.googleapis.com/token",
              "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
              "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/livegong%40appspot.gserviceaccount.com"
            }';
    $imageAnnotator = new ImageAnnotatorClient([
        'credentials' => json_decode($cre, true),
    ]);
    # annotate the image
    if ($flg == 1) {
        $image = $path;
    } else {
        $image = file_get_contents($path);
    }
    // $image = file_get_contents($path);

    $response = $imageAnnotator->safeSearchDetection($image);
    $safe = $response->getSafeSearchAnnotation();

    $adult = $safe->getAdult();
    // $medical = $safe->getMedical();
    // $spoof = $safe->getSpoof();
    // $violence = $safe->getViolence();
    // $racy = $safe->getRacy();

    # names of likelihood from google.cloud.vision.enums
    $likelihoodName = ['UNKNOWN', 'VERY_UNLIKELY', 'UNLIKELY',
        'POSSIBLE', 'LIKELY', 'VERY_LIKELY'];
    if ($safe) {
        if ($likelihoodName[$adult] == 'LIKELY' || $likelihoodName[$adult] == 'VERY_LIKELY') {
            return 1;
        } else {
            return 0;
        }
    } else {
        return 0;
    }
    $imageAnnotator->close();
}
function sendVerificationMessage($phone_no, $message)
{
    $sid = 'ACb110b855d2174dfd5ec3c49f46cee10e';
    $token = 'e33b8c91d8d10343ffd5633e5c76c7d5';

    $client = new Client($sid, $token);

    // try{
    // Use the client to do fun stuff like send text messages!
    $client->messages->create(
        // the number you'd like to send the message to
        $phone_no,
        [
            // A Twilio phone number you purchased at twilio.com/console
            'from' => '+13237035190',
            // the body of the text message you'd like to send
            'body' => $message,
        ]
    );

}

function ip_info($ip = null, $purpose = "location", $deep_detect = true)
{
    $output = null;
    if (filter_var($ip, FILTER_VALIDATE_IP) === false) {
        $ip = $_SERVER["REMOTE_ADDR"];
        if ($deep_detect) {
            if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP)) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }

            if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP)) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            }

        }
    }
    $purpose = str_replace(array("name", "\n", "\t", " ", "-", "_"), null, strtolower(trim($purpose)));
    $support = array("country", "countrycode", "state", "region", "city", "location", "address");
    $continents = array(
        "AF" => "Africa",
        "AN" => "Antarctica",
        "AS" => "Asia",
        "EU" => "Europe",
        "OC" => "Australia (Oceania)",
        "NA" => "North America",
        "SA" => "South America",
    );
    if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
        $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
        if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
            switch ($purpose) {
                case "location":
                    $output = array(
                        "city" => @$ipdat->geoplugin_city,
                        "state" => @$ipdat->geoplugin_regionName,
                        "country" => @$ipdat->geoplugin_countryName,
                        "country_code" => @$ipdat->geoplugin_countryCode,
                        "continent" => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                        "continent_code" => @$ipdat->geoplugin_continentCode,
                    );
                    break;
                case "address":
                    $address = array($ipdat->geoplugin_countryName);
                    if (@strlen($ipdat->geoplugin_regionName) >= 1) {
                        $address[] = $ipdat->geoplugin_regionName;
                    }

                    if (@strlen($ipdat->geoplugin_city) >= 1) {
                        $address[] = $ipdat->geoplugin_city;
                    }

                    $output = implode(", ", array_reverse($address));
                    break;
                case "city":
                    $output = @$ipdat->geoplugin_city;
                    break;
                case "state":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "region":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "country":
                    $output = @$ipdat->geoplugin_countryName;
                    break;
                case "countrycode":
                    $output = @$ipdat->geoplugin_countryCode;
                    break;
            }
        }
    }
    return $output;
}

function get_client_ip()
{
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP'])) {
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    } else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else if (isset($_SERVER['HTTP_X_FORWARDED'])) {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    } else if (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    } else if (isset($_SERVER['HTTP_FORWARDED'])) {
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    } else if (isset($_SERVER['REMOTE_ADDR'])) {
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    } else {
        $ipaddress = 'UNKNOWN';
    }

    return $ipaddress;
}

function sendMail($to_email, $sender_name = 'Erp Pos', $sender_email = 'erppos1122@gmail.com', $subject = "", $body_html = "dd", $cc = '', $bcc = '', $attachments = array())
{

    require_once 'phpmailer/PHPMailerAutoload.php';

    $mail = new PHPMailer;
    $mail->isSMTP(); // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
    $mail->SMTPAuth = true; // Enable SMTP authentication.p
    $mail->Username = 'erppos1122@gmail.com'; // SMTP username
    $mail->Password = 'erp123456'; // SMTP password
    $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587; // TCP port to connect to

    $mail->From = $sender_email;
    $mail->FromName = $sender_name;
    //$mail->addAddress('joe@example.net', 'Joe User');     // Add a recipient
    $mail->addAddress($to_email); // Name is optional
    // echo $sender_email; exit;
    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
    $mail->isHTML(true); // Set email format to HTML

    $mail->Subject = $subject;
    $mail->Body = $body_html;

    $mail->AltBody = $body_html;

    if (!$mail->send()) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo . '';
        exit;
    }

}

function getVal($col, $table, $where = '', $criteria = '')
{

    $results = \DB::table($table)->where($where, $criteria)->value($col);
    if ($results) {
        return $results;
    } else {
        return false;
    }
}

function checkImage($path = '', $placeholder = '')
{
//    dd($path);

    if (empty($placeholder)) {
        $placeholder = 'placeholder.png';
    }

    if (!empty($path)) {
        $url = explode('storage', $path);
        $url = public_path() . '/storage' . $url[1];
        $isFile = explode('.', $url);
        if (file_exists($url) && count($isFile) > 1) {
            return $path;
        } else {
            return asset('img/' . $placeholder);
        }

    } else {
        return asset('img/' . $placeholder);
    }
}

function get_sidebar_dynamic_values()
{

    $data = [];
    $user = auth()->user();
    $taxnomies = Taxonomie::where('type', 'video_level')->with('mediaGenre')->first();
    $user_taxnomies = Taxonomie::where('type', 'user_level')->get();
    $competitors = CompetitorSet::all()->take(20);
    $data['message'] = 'dashboard data';
    $data['user'] = [
        'email' => $user->email,
        'phone' => $user->phone,
        'tier' => ($user->currentUserTier) ? $user->currentUserTier->tier->name : '',
        'name' => ($user->userDetail) ? $user->userDetail->name : '-',
        'username' => ($user->userDetail) ? $user->userDetail->username : '-',
        'date_of_birth' => ($user->userDetail) ? $user->userDetail->date_of_birth : '-',
        'profile_photo' => ($user->userDetail) ? $user->userDetail->profile_photo : 'no-image.png',
        'gender' => ($user->userDetail) ? $user->userDetail->gender : '-',
        'region' => ($user->userDetail) ? '' : '-',
        'country' => ($user->userDetail) ? (($user->userDetail->country) ? $user->userDetail->country->name : '') : '-',
        'mediaTypes' => Taxonomie::where('type', 'video_level')->where('parent_id', 0)->get(),
        'genres' => ($taxnomies) ? (($taxnomies->mediaGenre) ? $taxnomies->mediaGenre : '') : '',
        'tiers' => $user_taxnomies,
        'competitors' => $competitors,
    ];
    $countries = Country::all();
    $data['countries'] = $countries;

    return $data;
}

function userProfilePhoto($user)
{
    $dp = ($user->userDetail) ? $user->userDetail->profile_photo : 'no-image.png';
    return $dp;
}
function userName($user)
{
    $username = ($user->userDetail) ? $user->userDetail->username : '-';
    return $username;
}
function userTier($user)
{
    $user_taxnomies = Taxonomie::select("name")->where(['type' => 'user_level', 'id' => $user->taxonomie_id])->first();

    return $user_taxnomies->name;
}
function gender($user)
{
    $gender = ($user->userDetail) ? $user->userDetail->gender : '-';
    return $gender;
}
function dateOfBirth($user)
{
    $date_of_birth = ($user->userDetail) ? $user->userDetail->date_of_birth : '-';
    return $date_of_birth;
}
function region($user)
{
    $region = ($user->userDetail) ? '' : '-';
    return $region;
}
function country($user)
{
    $country = ($user->userDetail) ? (($user->userDetail->country) ? $user->userDetail->country->name : '') : '-';
    return $country;
}
//    Private Function
function encryptString($data, $key)
{
    $encryption_key = base64_decode($key);
    $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
    $encrypted = openssl_encrypt($data, 'aes-256-cbc', $encryption_key, 0, $iv);
    return base64_encode($encrypted . '::' . $iv);
}

function decryptString($data, $key)
{
    $encryption_key = base64_decode($key);
    list($encrypted_data, $iv) = explode('::', base64_decode($data), 2);
    return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
}
