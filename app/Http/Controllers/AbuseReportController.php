<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modals\UserReport;
use Auth;
class AbuseReportController extends Controller
{
    public function saveReport(){
    	$user_content_id = request()->user_content_id;
    	$report_id = request()->report_id;
    	$user_report_found = UserReport::where('user_content_id',$user_content_id)
    									->where('report_by',Auth::user()->id)
    									->first();
    	if(!$user_report_found){
    		$user_report = new UserReport();
	    	$user_report->user_content_id = $user_content_id;
	    	$user_report->report_id = $report_id;
	    	$user_report->report_by = Auth::user()->id;
	    	$user_report->save(); 
	    	return response()->json(['status' => 1,'message' => 'Report Saved successfully']);
    	}else{
	    	return response()->json(['status' => 0,'message' => 'Your Report already saved']);
    	}
    }
}
