<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\LoginUserRequest;
use App\Http\Requests\UserDetailFormRequest;
use App\Modals\Taxonomie;
use App\Modals\User;
use App\Modals\UserDetail;
use App\Modals\UserTier;
use App\Utils\GoogleCloudRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Modals\Traits\ResponseMapper;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    use  ResponseMapper;

    public function login(LoginUserRequest $request)
    {

        $validatedData = $request->validated();

        if ($token = Auth::guard()->attempt($validatedData)) {
            $response = [
                'token' => [
                    'access_token' => $token,
                    'token_type' => 'bearer',
                    'expires_in' => Auth::guard()->factory()->getTTL() * 60
                ],
                'user' => Auth::guard()->user()
            ];
            $this->message = 'Login successfull';
            $this->payload = $response;
            $this->responseCode = 200;
            return $this->sendJsonResponse();
        }

        $this->error = "Unauthorised";
        $this->message = 'Invalid email or password.';
        $this->responseCode = 400;
        return $this->sendJsonResponse();
    }

    public function register(Request $request)
    {
        $validator = $request->validated();

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $data = [];

        $taxonomie = Taxonomie::where('type', 'user_level')
            ->where('name', 'Premier')
            ->first();
        $request->merge(['taxonomie_id' => $taxonomie->id, 'phone' => request()->full_number]);

        $user = User::create($request->all());



        if ($user) {

            $token = auth()->login($user);

            $category_media = Taxonomie::where('type', 'video_level')
                ->where('parent_id', 0)
                ->get();
            $taxno = Taxonomie::where('type', 'user_level')
                ->where('name_key', 'premier')
                ->where('parent_id', 0)
                ->first();
            foreach ($category_media as $key => $value) {
                $user_tier = new UserTier();
                $user_tier->category_id = $value->id;
                $user_tier->taxonomie_id = $taxno->id;
                $user_tier->user_id = $user->id;
                $user_tier->save();
            }

            $rand = rand(111111, 999999);
            sendVerificationMessage(request()->full_number, 'Your Verification Code is ' . $rand);
            $response = [
                'token' => [
                    'access_token' => $token,
                    'token_type' => 'bearer',
                    'expires_in' => Auth::guard()->factory()->getTTL() * 60
                ],
                'user' => Auth::guard()->user()
            ];
            $this->message = 'Login successfull';
            $this->payload = $response;
            $this->responseCode = 200;
            return $this->sendJsonResponse();
        } else {
            $this->error = "Unauthorised";
            $this->message = 'Invalid email or password.';
            $this->responseCode = 400;
            return $this->sendJsonResponse();

        }

    }



    public function verifyPhoneNumber(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required',
        ]);
        $response = [];
        if ($validator->fails()) {
            $errors = [];
            $errors['status'] = 2;
            $errors['message'] = 'Validation Error';
            $errors['errors'] = $validator->messages();
            return response()->json(['status',$errors['status'],
                'message', $errors['message'],
                'error', $errors['errors']]);

        }
        $code = request()->code;
        $user = User::where('verification_code', $code)->first();
        if ($user) {
            $user->phone_status = 'verified';
            $user->save();
            $response['status'] = 200;
            return response()->json($response);
        } else {
            $response['status'] = 400;
            return response()->json($response);
        }
    }

    public function userDetail(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'first_name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'username' => 'required|unique:user_details,username|max:20',
            'date_of_birth' => ((request()->agent_role_hidden == 'agent') || (request()->spoonser_role_hidden == 'spoonser')) ? '' : 'required|date|before:-13 years',
            'gender' => 'required',
            'profile_photo' => 'required|mimes:jpeg,jpg,png',
            'region_id' => 'required',
            'country_id' => 'required',
        ],
            [
                'date_of_birth.before' => 'You must be 13 years or Older',
            ]);

        if ($validator->fails()) {

            $errors = [];
            $errors['status'] = 2;
            $errors['message'] = 'Validation Error';
            $errors['errors'] = $validator->messages();
            return response()->json($errors, 422);
        }

        $data = [];
        $cloudPath = '';
        if (request()->file('profile_photo')) {
            //dd('test in if block');
            if (request()->file_canvas) {
               // dd('test in file canvas');
                $img = str_replace('data:image/png;base64,', '', request()->file_canvas);
                $img = str_replace(' ', '+', $img);
                $img_decoded = base64_decode($img);
                $bucketName = config('constant.GCS_BUCKET');
                $bucketName = env('GCS_BUCKET', 'gong_bucket');
                $fileContent = file_get_contents(request()->file_canvas);
                $cloudPath = 'uploads/' . $request->file_type . '/' . time() . rand(101010100101, 545454547) . 'png';

                $googleRequest = new GoogleCloudRequest();
                // $isSucceed = $googleRequest->uploadFile($bucketName, $img_decoded, $cloudPath);

            } else {
                //dd('test in else');
                $file = $request->file('profile_photo');

                $bucketName = config('constant.GCS_BUCKET');
                $bucketName = env('GCS_BUCKET', 'gong_bucket');
                $fileContent = file_get_contents($file->getRealPath());

                $cloudPath = 'uploads/' . $request->file_type . '/' . time() . $file->getClientOriginalName();

                $googleRequest = new GoogleCloudRequest();

                $isSucceed = $googleRequest->uploadFile($bucketName, $fileContent, $cloudPath);


            }

        }
        $userDetail = new UserDetail();
        $userDetail->user_id = request()->user_id;

        $userDetail->first_name = request()->first_name;
        $userDetail->last_name = request()->last_name;
        $userDetail->username = request()->username;
        $userDetail->date_of_birth = ((request()->agent_role_hidden == 'agent') || (request()->spoonser_role_hidden == 'spoonser')) ? '1359-07-29' : request()->date_of_birth;
        $userDetail->gender = request()->gender;
        $userDetail->profile_photo = $cloudPath;


        $userDetail->region_id = request()->region_id;
        $userDetail->country_id = request()->country_id;
        $userDetail->notify_by_app = request()->app_notify;
        $userDetail->notify_by_text = request()->text_notify;
        $userDetail->save();

        $user = User::where('id', request()->user_id)->first();

        if ($userDetail) {
            $data['status'] = 1;
            $data['message'] = 'new account created';
            $data['data'] = [
                'user' => $userDetail->user()->first(),
            ];
        } else {
            $data['status'] = 0;
            $data['message'] = 'new account created';
            $data['data'] = [];
        }
        if ($data['status'] == 1) {

            auth()->login($data['data']['user']);
            $data['role'] = (Auth::user()->hasRole('user')) ? 1 : 0;
        }
        return response()->json(['status',$data['status'],'message', $data['message'],'data', $data['data']]);

    }
    public function forgot_password(Request $request)
    {
        $input = $request->all();
        $rules = array(
            'email' => "required|email",
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $arr = array("status" => 400, "message" => $validator->errors()->first(), "data" => array());
        } else {
            try {
                $response = Password::sendResetLink($request->only('email'), function (Message $message) {
                    $message->subject($this->getEmailSubject());
                });
                switch ($response) {
                    case Password::RESET_LINK_SENT:
                        return \Response::json(array("status" => 200, "message" => trans($response), "data" => array()));
                    case Password::INVALID_USER:
                        return \Response::json(array("status" => 400, "message" => trans($response), "data" => array()));
                }
            } catch (\Swift_TransportException $ex) {
                $arr = array("status" => 400, "message" => $ex->getMessage(), "data" => []);
            } catch (Exception $ex) {
                $arr = array("status" => 400, "message" => $ex->getMessage(), "data" => []);
            }
        }
        return \Response::json($arr);
    }


}
