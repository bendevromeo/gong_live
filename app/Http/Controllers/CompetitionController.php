<?php

namespace App\Http\Controllers;

use App\Modals\Comment;
use App\Modals\Competition;
use App\Modals\CompetitionUser;
use App\Modals\CompetitorSet;
use App\Modals\Country;
use App\Modals\Taxonomie;
use App\Modals\UserContent;
use App\Modals\UserDetail;
use App\Modals\UserTier;
use App\Utils\GoogleCloudRequest;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CompetitionController extends Controller
{

    private $user;

    /**
     * UserController constructor.
     * @param User $user
     */
    public function index()
    {
        $data = [];
        $user = auth()->user();
        $taxnomies = Taxonomie::where('type','video_level')->with('mediaGenre')->first();

        // dd( $taxnomies);
        $user_taxnomies = Taxonomie::where('type','user_level')->get();
        $competitors = CompetitorSet::all()->take(20);
            $data['message'] = 'dashboard data';
            $data['data'] = [
                'user' => [
                    'email' => $user->email,
                    'phone' => $user->phone,
                    'tier' => ($user->currentUserTier) ? $user->currentUserTier->tier->name : '',
                    'name' => ($user->userDetail) ?  $user->userDetail->name : '-',
                    'username' => ($user->userDetail) ?  $user->userDetail->username : '-',
                    'date_of_birth' => ($user->userDetail) ?  $user->userDetail->date_of_birth : '-',
                    'profile_photo' => ($user->userDetail) ?  $user->userDetail->profile_photo : 'no-image.png',
                    'gender' => ($user->userDetail) ?  $user->userDetail->gender : '-',
                    'region' => ($user->userDetail) ?  '' : '-',
                    'country' => ($user->userDetail) ?  (($user->userDetail->country) ? $user->userDetail->country->name : '') : '-',
                    'mediaTypes' => Taxonomie::where('type','video_level')->where('parent_id',0)->get(),
                    'genres'    => ($taxnomies) ? (($taxnomies->mediaGenre) ? $taxnomies->mediaGenre : '') : '',
                    'tiers'    => $user_taxnomies,
                    'competitors' => $competitors
                ]
            ];
            $countries = Country::all();
            $data['data']['countries'] = $countries;
            // dd($data);
        return view('frontend.users.pages.dashboard-tabs.create-competition',$data);
    }



    public function saveCompetion(Request $request){
        // dd(request()->all());
        $validator = Validator::make($request->all(), [
            'competition_name' => 'required|max:50',
            'create_competition_type' => 'required',
            'create_category_id' => 'required',
            'create_genre_id' => 'required',
            'file_type' => 'required',
            'competition_set' => 'required',
            'no_of_competitior' => 'required',
            // 'no_of_round' => 'required',
            'voting_time' => 'required',
            'competition_duration' => 'required',
            'access' => 'required',
            'eligible_tier' => 'required',
            'countries' => 'required',
            // 'region' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 2, 'error' => $validator->errors()]);
        }
        $code = '';
        $competitors_s = CompetitorSet::where('id', request()->no_of_competitior)->first();

        if ($competitors_s) {
            $competition = new Competition();
            $competition->competition_name = request()->competition_name;
            $competition->competition_end_type = request()->create_competition_type;
            $competition->category_id = request()->create_category_id;
            $competition->parent_id = 0;
            $competition->genre_id = request()->create_genre_id;
            $competition->competition_media = request()->file_type;
            $competition->no_of_competitiors = request()->no_of_competitior;
            $competition->no_of_rounds = request()->no_of_round;
            $competition->competitior_set_id = $competitors_s->id;
            $competition->voting_interval = request()->voting_time;
            $competition->duration = request()->competition_duration;
            $competition->competition_type = request()->access;
            $competition->competition_set = request()->competition_set;
            $competition->eligible_tier_id = request()->eligible_tier;
            $competition->competition_user_level_id = request()->eligible_tier;
            $competition->country_id = request()->countries;
            $competition->region_id = request()->region;
            $competition->comopetition_by_type = 'custom';
            $competition->competition_by = Auth::user()->id;
            $competition->is_active = 4;
            $competition->save();
            $no_round = $competitors_s->no_of_rounds; // 3
            $round = json_decode($competitors_s->round_details);
            $number_of_competitor_each = 0;
            $number_of_competition = 0;
            if (request()->access == 'private') {
                $code = rand(1021145, 2546498);
            }
//            foreach ($round->round1 as $key => $value) {
            //                $number_of_competitor_each = $value->no_of_competitor;
            //                for($i = 1; $i<=$value->total_competition; $i++){
            //                    $competition_sub = new Competition();
            //                    $competition_sub->competition_name      = request()->competition_name;
            //                    $competition_sub->competition_end_type  = request()->create_competition_type;
            //                    $competition_sub->category_id           = request()->create_category_id;
            //                    $competition_sub->parent_id             = $competition->id;
            //                    $competition_sub->genre_id              = request()->create_genre_id;
            //                    $competition_sub->competition_media     = request()->file_type;
            //                    $competition_sub->no_of_competitiors    = $value->no_of_competitor;
            //                    $competition_sub->no_of_rounds          = 1;
            //                    $competition_sub->competitior_set_id    = $competitors_s->id;
            //                    $competition_sub->voting_interval       = request()->voting_time;
            //                    $competition_sub->duration              = request()->competition_duration;
            //                    $competition_sub->competition_type      = request()->access;
            //                    $competition_sub->competition_set       = request()->competition_set;
            //                    $competition_sub->eligible_tier_id      = request()->eligible_tier;
            //                    $competition_sub->competition_user_level_id      = request()->eligible_tier;
            //                    $competition_sub->country_id            = request()->countries;
            //                    $competition_sub->region_id             = request()->region;
            //                    $competition_sub->comopetition_by_type  = 'custom';
            //                    $competition_sub->competition_by        = Auth::user()->id;
            //                    $competition_sub->is_active             = 3;
            //                    if(request()->access == 'private'){
            //
            //                        $competition_sub->joining_code = $code;
            //                    }
            //                    $is_saved = $competition_sub->save();
            //                }
            //            }
        }
        return response()->json(['status' => 1, 'joining_code' => $code]);
    }

    public function joinPrivate()
    {
        $data = [];
        $user = auth()->user();
        $taxnomies = Taxonomie::where('type', 'video_level')->with('mediaGenre')->first();
        $user_taxnomies = Taxonomie::where('type', 'user_level')->get();
        $data['message'] = 'dashboard data';
        $data['data'] = [
            'user' => [
                'email' => $user->email,
                'phone' => $user->phone,
                'tier' => ($user->currentUserTier) ? $user->currentUserTier->tier->name : '',
                'name' => ($user->userDetail) ? $user->userDetail->name : '-',
                'username' => ($user->userDetail) ? $user->userDetail->username : '-',
                'date_of_birth' => ($user->userDetail) ? $user->userDetail->date_of_birth : '-',
                'profile_photo' => ($user->userDetail) ? $user->userDetail->profile_photo : 'no-image.png',
                'gender' => ($user->userDetail) ? $user->userDetail->gender : '-',
                'region' => ($user->userDetail) ? '' : '-',
                'country' => ($user->userDetail) ? (($user->userDetail->country) ? $user->userDetail->country->name : '') : '-',
                'mediaTypes' => Taxonomie::where('type', 'video_level')->where('parent_id', 0)->get(),
                'genres' => ($taxnomies) ? (($taxnomies->mediaGenre) ? $taxnomies->mediaGenre : '') : '',
                'tiers' => $user_taxnomies,
            ],
        ];
        $countries = Country::all();
        $data['data']['countries'] = $countries;
        return view('frontend.users.pages.dashboard-tabs.private-join', $data);
    }

    public function getPublicCompetition()
    {
        $competition = Competition::where('comopetition_by_type', 'custom')
            ->where('is_active', 3)
            ->where('competition_type', 'public')
            ->where('competition_media', request()->type)
            ->where('category_id', request()->category_id)
            ->where('genre_id', request()->genre_id)
            ->whereHas('elegible', function ($qry) {
                $qry->where('user_id', Auth::user()->id)
                    ->where('category_id', request()->category_id);
            })
            ->paginate(4);
        // dd($competition);
        $html = view('frontend.users.pages.dashboard-tabs.ajax-public-competition', compact('competition'))->render();
        // dd($html);
        return response()->json(['status' => 1, 'html' => $html]);
    }

    public function getPublicCompetitionDetail()
    {
        $competition = Competition::where('id', request()->id)
            ->where('is_active', 3)
        // ->whereHas('elegible',function($qry){
        //     $qry->where('user_id',Auth::user()->id)
        //         ->where('category_id',request()->category_id);
        // })
            ->with('competitionDetail', 'tier', 'country', 'region', 'genre')
            ->first();
        // dd($competition);
        if ($competition) {
            return response()->json(['status' => 1, 'competition' => $competition]);
        } else {
            return response()->json(['status' => 0]);
        }
    }
    public function codeVerify(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 2, 'error' => $validator->errors()]);
        }

        $competition = Competition::where('joining_code', request()->code)
            ->where('is_active', 3)
            ->whereHas('elegible', function ($qry) {
                $qry->where('user_id', Auth::user()->id)
                    ->where('category_id', request()->category_id);
            })
            ->with('competitionDetail', 'tier', 'country', 'region', 'genre')
            ->first();

        if ($competition) {
            $child_competition = Competition::where('id', $competition->parent_id)
                ->with('childCompetition.competitionDetail')
                ->first();
            $arr = [];
            $total = 0;
            $total_enroll = 0;
            foreach ($child_competition->childCompetition as $key => $value) {
                $arr[$value->id] = [
                    'total' => count($value->competitionDetail),
                    'no_of_competitiors' => $value->no_of_competitiors,
                    'id' => $value->id,
                ];
                $total_enroll = count($value->competitionDetail) + $total_enroll;
                $total = $value->no_of_competitiors + $total;
            }

            $new_arr = array_filter($arr, function ($a) {
                if ($a['total'] != $a['no_of_competitiors']) {
                    return $a;
                }
            });
            // dd(count($new_arr));
            if (count($new_arr) == 0) {
                return response()->json(['status' => 0, 'message' => 'Number of Competition is already full in this competition.']);
            }
            // if($competition->competitionDetail){
            //     if($competition->competitionDetail->count() >= $competition->no_of_competitiors){
            //          return response()->json(['status' => 0,'message' => 'Number of Competition is already full in this competition']);
            //     }
            // }

            if ($competition->competition_media == request()->type) {
                if ($competition->category_id == request()->category_id) {
                    if ($competition->genre_id == request()->genre_id) {
                        return response()->json(['status' => 1, 'competition' => $competition]);
                    } else {
                        return response()->json(['status' => 0, 'message' => 'Please Choose ' . $competition->genre->name . ' Genre for this Competition']);
                    }

                } else {
                    return response()->json(['status' => 0, 'message' => 'Please Choose ' . $competition->tier->name . ' category for this Competition']);
                }

            } else {
                return response()->json(['status' => 0, 'message' => 'Your Media Type is not valid for this Competition.']);
            }

        } else {
            return response()->json(['status' => 0, 'message' => 'Code is invalid']);
        }
    }
    public function joinPrivateCompetition(Request $request)
    {
        // dd(request()->all());
        $competition_ = Competition::where('joining_code', request()->code)
            ->where('is_active', 3)
        // ->where('competition_media',request()->type)
            ->first();
        if ($competition_) {} else {
            return response()->json(['status' => 0]);
        }
        $child_competition = Competition::where('id', $competition_->parent_id)
            ->with('childCompetition.competitionDetail')
            ->first();
        $arr = [];
        $total = 0;
        $total_enroll = 0;
        foreach ($child_competition->childCompetition as $key => $value) {
            $arr[$value->id] = [
                'total' => count($value->competitionDetail),
                'no_of_competitiors' => $value->no_of_competitiors,
                'id' => $value->id,
            ];
            $total_enroll = count($value->competitionDetail) + $total_enroll;
            $total = $value->no_of_competitiors + $total;
        }

        $new_arr = array_filter($arr, function ($a) {
            if ($a['total'] != $a['no_of_competitiors']) {
                return $a;
            }
        });
        $final_index = array_rand($new_arr);
        $competition = Competition::where('id', $new_arr[$final_index]['id'])->first();
        if ($competition) {
            $data = [];
            $file = $request->file('file');
            $bucketName = config('constant.GCS_BUCKET');
            // $bucketName = env('GCS_BUCKET', 'gong_bucket');
            $fileContent = file_get_contents($file->getRealPath());
            if (request()->file_type == 'video') {
                $image = $request->img_thum; // your base64 encoded
                $image = str_replace('data:image/png;base64,', '', $image);
                $image = str_replace(' ', '+', $image);
                $imageName = rand(111452, 15465460) . '.' . 'png';
                // \File::put(storage_path(). '/' . $imageName, base64_decode($image));
            }
            $cloudPath = 'uploads/' . $request->file_type . '/' . time() . $file->getClientOriginalName();

            $googleRequest = new GoogleCloudRequest();
            if (request()->file_type == 'video') {
                $img_path = 'uploads/' . 'img' . '/' . time() . $imageName;
                $googleRequest->uploadFile($bucketName, base64_decode($image), $img_path);
            }
            $isSucceed = $googleRequest->uploadFile($bucketName, $fileContent, $cloudPath);

            if ($isSucceed) {
                $data['status'] = true;
                $data['message'] = 'content uploaded successfully';
                $fileInfo = $googleRequest->getFileInfo($bucketName, $cloudPath);
                $user_tier = UserTier::where('user_id', Auth::user()->id)
                    ->where('category_id', request()->category_id)
                    ->first();
                $userContent = new UserContent();
                $userContent->user_tier_id = $user_tier->id;
                $userContent->file_type = request()->file_type;
                $userContent->file_gcs_self_link = $fileInfo['selfLink'];
                $userContent->file = $cloudPath;
                if (request()->file_type == 'video') {
                    $userContent->thumbnail = $img_path;
                }
                $userContent->category_id = request()->category_id;
                $userContent->name = request()->name;
                $userContent->genre_id = request()->genre_id;
                $userContent->is_competition = 1;
                $userContent->save();

                $user_tier->is_competition = 1;
                $user_tier->save();

                $competititon_user = new CompetitionUser();
                $competititon_user->user_id = Auth::user()->id;
                $competititon_user->user_content_id = $userContent->id;
                $competititon_user->competition_id = $competition->id;
                $is_c_saved = $competititon_user->save();
                $total_enroll = $total_enroll + 1;
                if ($total == $total_enroll) {
                    Competition::where('parent_id', $competition_->parent_id)->update(['is_active' => 1, 'start_at' => date('Y-m-d H:i:s'), 'end_at' => date("Y-m-d H:i:s", strtotime('+' . $competition->voting_interval . ' hours'))]);
                }
                // if($competition->competitionDetail->count() == $competition->no_of_competitiors){
                //     $competition->is_active = 1;
                //     $competition->start_at = date('Y-m-d H:i:s');
                //     $competition->end_at = date("Y-m-d H:i:s", strtotime('+'.$competition->voting_interval.' hours'));
                //     $competition->save();
                // }
                $data['data'] = [
                    'file_link' => config('constant.GCS_BASE_URL') . config('constant.GCS_BUCKET') . '/' . $cloudPath,
                    // 'file_link' => env('GCS_BASE_URL') .env('GCS_BUCKET') . '/' . $cloudPath,
                    'content_id' => $userContent->id,
                ];
            } else {
                $data['status'] = false;
                $data['message'] = 'There is problem uploading content';
                $data['data'] = [];
            }
            return json_encode($data);
        } else {
            return response()->json(['status' => 0]);
        }

    }

    public function joinPublicCompetition(Request $request)
    {
        $competition_ = Competition::where('id', request()->id)
            ->where('is_active', 3)
            ->first();
        $child_competition = Competition::where('id', $competition_->parent_id)
            ->with('childCompetition.competitionDetail')
            ->first();
        $arr = [];
        $total = 0;
        $total_enroll = 0;
        foreach ($child_competition->childCompetition as $key => $value) {
            $arr[$value->id] = [
                'total' => count($value->competitionDetail),
                'no_of_competitiors' => $value->no_of_competitiors,
                'id' => $value->id,
            ];
            $total_enroll = count($value->competitionDetail) + $total_enroll;
            $total = $value->no_of_competitiors + $total;
        }

        $new_arr = array_filter($arr, function ($a) {
            if ($a['total'] != $a['no_of_competitiors']) {
                return $a;
            }
        });
        $final_index = array_rand($new_arr);
        $competition = Competition::where('id', $new_arr[$final_index]['id'])->first();
        if ($competition) {
            $data = [];
            $file = $request->file('file');
            $bucketName = config('constant.GCS_BUCKET');
            // $bucketName = env('GCS_BUCKET', 'gong_bucket');
            $fileContent = file_get_contents($file->getRealPath());
            if (request()->file_type == 'video') {
                $image = $request->img_thum; // your base64 encoded
                $image = str_replace('data:image/png;base64,', '', $image);
                $image = str_replace(' ', '+', $image);
                $imageName = rand(111452, 15465460) . '.' . 'png';
                // \File::put(storage_path(). '/' . $imageName, base64_decode($image));
            }
            $cloudPath = 'uploads/' . $request->file_type . '/' . time() . $file->getClientOriginalName();

            $googleRequest = new GoogleCloudRequest();
            if (request()->file_type == 'video') {
                $img_path = 'uploads/' . 'img' . '/' . time() . $imageName;
                $googleRequest->uploadFile($bucketName, base64_decode($image), $img_path);
            }
            $isSucceed = $googleRequest->uploadFile($bucketName, $fileContent, $cloudPath);

            if ($isSucceed) {
                $data['status'] = true;
                $data['message'] = 'content uploaded successfully';
                $fileInfo = $googleRequest->getFileInfo($bucketName, $cloudPath);
                $user_tier = UserTier::where('user_id', Auth::user()->id)
                    ->where('category_id', request()->category_id)
                    ->first();
                $userContent = new UserContent();
                $userContent->user_tier_id = $user_tier->id;
                $userContent->file_type = request()->file_type;
                $userContent->file_gcs_self_link = $fileInfo['selfLink'];
                $userContent->file = $cloudPath;
                if (request()->file_type == 'video') {
                    $userContent->thumbnail = $img_path;
                }
                $userContent->category_id = request()->category_id;
                $userContent->name = request()->name;
                $userContent->genre_id = request()->genre_id;
                $userContent->is_competition = 1;
                $userContent->save();

                $user_tier->is_competition = 1;
                $user_tier->save();

                $competititon_user = new CompetitionUser();
                $competititon_user->user_id = Auth::user()->id;
                $competititon_user->user_content_id = $userContent->id;
                $competititon_user->competition_id = $competition->id;
                $is_c_saved = $competititon_user->save();
                $total_enroll = $total_enroll + 1;
                if ($total == $total_enroll) {
                    Competition::where('parent_id', $competition_->parent_id)->update(['is_active' => 1, 'start_at' => date('Y-m-d H:i:s'), 'end_at' => date("Y-m-d H:i:s", strtotime('+' . $competition->voting_interval . ' hours'))]);
                    // $competition->start_at = date('Y-m-d H:i:s');
                    // $competition->end_at = date("Y-m-d H:i:s", strtotime('+'.$competition->voting_interval.' hours'));
                    // $competition->save();
                }
                $data['data'] = [
                    'file_link' => config('constant.GCS_BASE_URL') . config('constant.GCS_BUCKET') . '/' . $cloudPath,
                    // 'file_link' => env('GCS_BASE_URL') .env('GCS_BUCKET') . '/' . $cloudPath,
                    'content_id' => $userContent->id,
                ];
            } else {
                $data['status'] = false;
                $data['message'] = 'There is problem uploading content';
                $data['data'] = [];
            }
            return json_encode($data);
        } else {
            return response()->json(['status' => 0]);
        }
    }

    public function getCommentPost(Request $request)
    {

        $currentUser = Auth::user();
        $userDetails = UserDetail::select("username", "profile_photo")->where("user_id", $currentUser->id)->first();

        $comment = new Comment();
        $comment->competition_id = request()->competition_id;
        $comment->user_id = $currentUser->id;
        $comment->content = request()->comment;
        $comment->save();

        $last_insert_id = $comment->id;
        if ($last_insert_id > 0) {
            $totalComents = Comment::all();
            return response()->json(['status' => 1, 'message' => 'Posted', 'comment' => request()->comment,
                'total' => count($totalComents), 'userDetails' => $userDetails->toArray()]);
        } else {
            return response()->json(['status' => 0, 'message' => 'invalid', 'comment' => '', 'total' => 0, 'userDetails' => '']);
        }

    }
    public function getAllCommentPost()
    {

        $details = UserDetail::join('comments', 'user_details.user_id', '=', 'comments.user_id')
            ->select("user_details.username", "user_details.profile_photo", "comments.content")
            ->get();
        $totalComents = Comment::all();
        return response()->json(['status' => 1, 'message' => 'Comments Fetched', 'total' => count($totalComents), 'details' => $details->toArray()]);
    }

}
