<?php

namespace App\Http\Controllers;

use App\Modals\Country;
use App\Modals\Region;
use App\Modals\User;
use App\Modals\Report;
use App\Modals\Taxonomie;
use App\Modals\Competition;
use App\Modals\UserDetail;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;

class HomeController extends Controller{

    // private $country;
    // private $region;

    /**
     * HomeController constructor.
     * @param Country $country
     * @param Region $region
     */
    public function __construct(){
        // $this->country = $country;
        // $this->region = $region;
    }


    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index(){
        $countries = Country::orderBy('name','asc')->get();
        $profanity = Report::all();
        $regions = Region::all();
        $location = ip_info(get_client_ip());
        $artists = User::Role('user')->with('userDetail')->limit(8)->get();

        return view('frontend.index', compact('countries', 'regions','location','artists','profanity'));
    }
    public function sendVerifyCode(){
        if(Auth::check()){
           $rand = rand(1111,9999);

            sendVerificationMessage(Auth::user()->phone,'Your Verification Code is '.$rand);
            Auth::user()->verification_code = $rand;
            Auth::user()->save();
            return response()->json(['status' => 1]);
        }else{
            $rand = rand(1111,9999);
            $user = User::where('id',request()->id)->first();
            sendVerificationMessage($user->phone,'Your Verification Code is '.$rand);
            $user->verification_code = $rand;
            $user->save();
            return response()->json(['status' => 1]);
        }

    }
    public function VerifyCode(Request $request){

        $validator = Validator::make($request->all(), [
            'code'     => 'required',
        ]);
        $response = [];
        if ($validator->fails()) {
            $errors = [];
            $errors['status']   = 2;
            $errors['message']  = 'Validation Error';
            $errors['errors']   = $validator->messages();
            return json_encode($errors);
        }
       $code = request()->code;
       if(Auth::check()){
            $user = User::where('verification_code',$code)
                    ->where('id',Auth::user()->id)
                    ->first();
       }else{
            $user = User::where('verification_code',$code)
                        ->where('id',request()->id)
                        ->first();

       }
        if($user){
            $user->phone_status = 'verified';
            $user->save();
            $response['status'] = 1;
            return response()->json($response);
        }else{
            $response['status'] = 0;
            return response()->json($response);
        }

    }
    public function changeNumber(Request $request){
        if(Auth::check()){
            $validator = Validator::make($request->all(), [
                'number'     => 'required|max:30|unique:users,phone,id'.Auth::user()->id,
            ]);
        }else{
            $validator = Validator::make($request->all(), [
                'number'     => 'required|max:30|unique:users,phone,id'.request()->id,
            ]);
        }
        if ($validator->fails()) {
            $errors = [];
            $errors['status']   = 2;
            $errors['message']  = 'Validation Error';
            $errors['errors']   = $validator->messages();
            return json_encode($errors);
        }
        if(Auth::check()){
            $rand = rand(1111,9999);
            $number = request()->number;
            Auth::user()->phone = $number;
            sendVerificationMessage(Auth::user()->phone,'Your Verification Code is '.$rand);
            Auth::user()->verification_code = $rand;
            Auth::user()->save();
            return response()->json(['status' => 1]);
        }else{
            $rand = rand(1111,9999);
            $number = request()->number;
            $user = User::where('id',request()->id)->first();
            $user->phone = $number;
            sendVerificationMessage($user->phone,'Your Verification Code is '.$rand);
            $user->verification_code = $rand;
            $user->save();
            return response()->json(['status' => 1]);
        }
    }
    public function getUserDetails(){
        $userDetails = UserDetail::select("username")->get();

        return response()->json(['user_details' => $userDetails]);
    }
}
