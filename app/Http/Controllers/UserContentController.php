<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddUserContentRequest;
use App\Modals\User;
use App\Modals\UserContent;
use App\Modals\UserTier;
use App\Modals\Competition;
use App\Modals\CompetitionUser;
use App\Modals\CompetitionUserLike;
use App\Modals\CompetitionUserJudge;
use Illuminate\Support\Facades\Validator;
use App\Utils\AppUtils;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Utils\GoogleCloudRequest;
use Auth;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Validator;

class UserContentController extends Controller
{
    private $userContent;
    private $user;

    /**
     * UserContentController constructor.
     * @param UserContent $userContent
     * @param User $user
     */
    public function __construct(UserContent $userContent, User $user)
    {
        $this->userContent = $userContent;
        $this->user = $user;
    }


    public function addContent(Request $request){

//         dd(request()->all());
        set_time_limit(200);
        $validator = Validator::make($request->all(), [
            'user_id'   => 'required',
            'file_type' => 'required',
            // 'file_type' => 'required|'.Rule::in(AppUtils::FILE_TYPES),
            'file'      => 'required',
            'category_id'  => 'required',
            // 'category'  => 'required|'.Rule::in(AppUtils::CATEGORIES),
            'name'      => 'required',
            // 'genre_id'  => 'required',
        ]);
        if ($validator->fails()) {
            $errors = [];
            $errors['status']   = 2;
            $errors['message']  = 'Validation Error';
            $errors['errors']   = $validator->messages();
            return response()->json($errors,422);
        }

        $data = [];
        $file = $request->file('file');

        $bucketName = config('constant.GCS_BUCKET');


        // $bucketName = env('GCS_BUCKET', 'gong_bucket');
        $fileContent = file_get_contents($file->getRealPath());
        if(request()->file_type == 'video'){
            $image = $request->img_thum;  // your base64 encoded
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = rand(111452,15465460).'.'.'png';
            // \File::put(storage_path(). '/' . $imageName, base64_decode($image));
        }
        $cloudPath = 'uploads/'. $request->file_type . '/' . time() . $file->getClientOriginalName();

        $googleRequest = new GoogleCloudRequest();

        if(request()->file_type == 'video'){

            $img_path = 'uploads/'. 'img' . '/' . time() . $imageName;

            $googleRequest->uploadFile($bucketName, base64_decode($image), $img_path);

        }

        $isSucceed = $googleRequest->uploadFile($bucketName, $fileContent, $cloudPath);


        if ($isSucceed){
            $data['status'] = true;
            $data['message'] = 'content uploaded successfully';
            $fileInfo = $googleRequest->getFileInfo($bucketName, $cloudPath);
            // $request->merge(['file_gcs_self_link' => $fileInfo['selfLink']]);
            $user_tier = UserTier::where('user_id',Auth::user()->id)
                                    ->where('category_id',request()->category_id)
                                    ->first();
            // dd($user_tier);
            $userContent = new UserContent();
            $userContent->user_tier_id          = $user_tier->id;
            $userContent->file_type             = request()->file_type;
            $userContent->file_gcs_self_link    = $fileInfo['selfLink'];
            $userContent->file                  = $cloudPath;
            if(request()->file_type == 'video'){
                $userContent->thumbnail         = $img_path;
            }
            $userContent->category_id           = request()->category_id;
            $userContent->name                  = request()->name;
            $userContent->genre_id              = request()->genre_id;
            $userContent->save();
            // if(request()->file_type == 'video'){
            //     $this->dda($userContent);
            //     // VideoNudity::dispatch($userContent)->onConnection('database');
            // }
            // if(request()->file_type == 'image'){
            //     $val = detect_safe_search_gcs('gs://gong_bucket/'.$userContent->file);
            //     if ($val == 1) {
            //         Mail::to(Auth::user()->email)->send(new MediaRejectMail());
            //         UserContent::where('id',$userContent->id)->update(['status' => 2]);
            //     }else{
            //         UserContent::where('id',$userContent->id)->update(['status' => 1]);
            //     }
            // }

            $user_contenet = UserContent::where('category_id',request()->category_id)
                                        // ->where('genre_id',request()->genre_id)
                                        ->where('is_competition',0)
                                        // ->where('status',1)
                                        ->where('file_type',request()->file_type)
                                        ->whereHas('userTier',function($qry)use($user_tier){
                                            $qry->where('category_id',request()->category_id)
                                                ->where('is_competition',0)
                                                ->where('taxonomie_id',$user_tier->taxonomie_id);

                                        })
                                        ->orderBy('created_at','desc')
                                        ->limit(4)
                                        ->get();

//dd($user_contenet);
            if(count($user_contenet) >= 4) {

                $supplier_email = getVal('email', 'users', 'id', $input['user_id']);
                $subject = "Testing";
                $content = "<h2>invoice email testing</h2>";
                $html_body = html_entity_decode($content);
                $fromName = "ERP-POS System";
                sendMail($supplier_email, $fromName, $sender_email = 'erppos1122@gmail.com', $subject, $html_body);

                $u_tier = UserTier::where('user_id',Auth::user()->id)
                                    ->where('category_id',request()->category_id)
                                    ->first();
                $competititon = new Competition();
                $competititon->competition_type          = 'public';
                $competititon->competition_media         = request()->file_type;
                $competititon->competition_user_level_id = $u_tier->id;
                $competititon->category_id               = $u_tier->category_id;
                $competititon->start_at                  = date('Y-m-d H:i:s');
                $competititon->end_at                    = date('Y-m-d H:i:s',strtotime(' +10 minutes'));
                // $competititon->end_at                    = date('Y-m-d H:i:s',strtotime(' +4 day'));
                if(request()->file_type == 'video'){
                    $competititon->priority  = 3;
                }elseif(request()->file_type == 'audio'){
                    $competititon->priority  = 2;
                }elseif(request()->file_type == 'image'){
                    $competititon->priority  = 1;
                }
                $is_saved = $competititon->save();

                if($is_saved){
                    foreach ($user_contenet as $key => $value) {
                        UserTier::where('id',$value->userTier->id)->update(['is_competition'=>1]);
                        $competititon_user                  = new CompetitionUser();
                        $competititon_user->user_id         = Auth::user()->id;
                        $competititon_user->user_content_id = $value->id;
                        $competititon_user->competition_id  = $competititon->id;
                        $is_c_saved = $competititon_user->save();
                        if($is_c_saved){
                            $user_contenet = UserContent::where('id',$value->id)->update(['is_competition'=>1]);
                        }
                    }
                }
            }

            $data['data'] = [
                'file_link' => config('constant.GCS_BASE_URL') .config('constant.GCS_BUCKET') . '/' . $cloudPath,
                // 'file_link' => env('GCS_BASE_URL') .env('GCS_BUCKET') . '/' . $cloudPath,
                'content_id' => $userContent->id
            ];
        }else{
            $data['status'] = false;
            $data['message'] = 'There is problem uploading content';
            $data['data'] = [];
        }
        return json_encode($data);
    }
     public function dda($userContent_obj){
         $abc = [
                   "type" => "service_account",
                   "project_id" => "livegong",
                   "private_key_id" => "bb13b1b01b2391fa0637c6a61898c74188d308ae",
                   "private_key" => "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDdPrDVDtpYEhfs\ngUi5UCxbLltVA8cGPx8oP19RRDcc7U+xiYCM7hDQFjtsWMc2OPp+oHHyUwEH2hgX\ncLvspo5C5ig7zJVMaLPEDP8x/CdJ48MEJZGKaCcXWweHEygVnqsyR0LtPjEr8RBx\nGotMVLh39PzHJu9zuVtIQCJKkZbuEQHfXiHku5R0up3IeaSmCPT/12mXFLLdUHA/\n7gFLLUI6acxDx0iFlv/Olq5VafULJMg2q2jmMY4NAftrKu96aU9cmkxu93FIi8K7\nNfSqIaBOfhQ56m5f5UPT2Epxxa5361HwQKEG/yD8gd5Xu+roERvf3LyN/Qy7xV7U\nOWbrYFCpAgMBAAECggEACvlr5njNvJ/Su/jo7vhBGv/zqJ9+8c2TvniJdXIZLozy\nIdZP00ddYiHLYZZ0KAZitoE/sn6kU6VhaItymhlKf5sD1LBTMrt6BasdxXb1sL6X\n387IdCflnuaPB9Sb1ABPPmrQF3O5FxSADxXH++NMuym8Mb/tEYpyMrISGvmZyPkx\nwxwZKhalemyqbWR9TRo7WrIIR+KxUE0t9gJrQLayVQk5+3F//83TgaX9qZVcDnRH\n2f0CnP449xccnsl+EQcgLG8ZvqrKXBsMTCnQ+L/Ngj0VgKg954csxlx1AsMsmfqX\nuFkk7gjfyZErYLpwdqHPI3me3KT1iFLf9m2UO99h4QKBgQD4KtEqiS/nbeKP9vr9\nLD5Xt3mpzYK4ouskA8ro8JyB5QkMS/XGf1YTrBaWihP/uzgim3USDkOXuCd9iHS+\n6BF4q6OZ380i3zSYJD2HEgoBeQTS54n5QJnZOCkQa1p4r/SuTF5CSxn2CeanQR6K\nCeMgE+Y3WARN9ucNz0khqgWf5QKBgQDkOleFUChT5F8nM+Qv1vEqdJPSGGaiM7Xa\nnZFnLAMz5XY/RQCk6w0oyDK4ahwG10DKaJZFrP4Hzhc3BMkmAnvHq0GfLG8Ng5hW\n6MRNAph3ZIf8a5jMVQsBt+fpuu4shhj3Yy2mFT6UH2sadoPVJPBDAu7/MHEXr9Nt\nLBDqU595dQKBgGBHrFLGWrF87lNQTNwAWEeLxUXdl4OuoW+dkRGCmBXFGBePONFE\nAc8sCq9o7o1ew06Z5XeV1R99sMSBNloCixS38ob2Tw3ibvM/zCclFoCvxPFWJy9u\nosGlijj2jWkZSuJ8em1Z2b6c5MGrAAAqCdYeIkkCetkMm2O+Me8/mWMJAoGBAJhv\ns1dSo6zs4vUvPqEJYokl1jnEnICII1XCWoTbq8P05lb6AqUkbeSoWuA71z5mdVr8\nIiqlkgKqj2zrIGLn078YTrs/FWmkVJLv43VAX4+XMRDS2PrZboGWUSsW9M/+PRy3\nPJ0qoWKT3ZiOQh2SD3visLj3pQHaKVBI1397nQ+lAoGACu5DBLhWC5IEICfOpl4s\ns4yJm33aNxyKlmRk4uStmYU9puAnXqYIKO5BLRIdLk+3N2XGW7Uxxrj0Bq0AfUE2\nKCRPRkZQj/b9y1AZmdsbiVqCiWa1YKpu/8bGkqQzvxTAeO5yeJWBW2q5kNPXvcCe\n1sAasgbLEiVGrpsf0SFLakg=\n-----END PRIVATE KEY-----\n",
                   "client_email" => "livegong@appspot.gserviceaccount.com",
                   "client_id" => "110426010858303348973",
                   "auth_uri" => "https://accounts.google.com/o/oauth2/auth",
                   "token_uri" => "https://oauth2.googleapis.com/token",
                   "auth_provider_x509_cert_url" => "https://www.googleapis.com/oauth2/v1/certs",
                   "client_x509_cert_url" => "https://www.googleapis.com/robot/v1/metadata/x509/livegong%40appspot.gserviceaccount.com"
                 ];
             $e =  'gs://stagging_bucket/'.$userContent_obj->file;

        $a = 0;
        $video = new VideoIntelligenceServiceClient([
            'credentials' => $abc
        ]);
        $options = [];
        $operation = $video->annotateVideo([
            'inputUri' => $e,
            'features' => [Feature::EXPLICIT_CONTENT_DETECTION]
        ]);
        $operation->pollUntilComplete($options);
        if ($operation->operationSucceeded()) {
            $results = $operation->getResult()->getAnnotationResults()[0];
            $explicitAnnotation = $results->getExplicitAnnotation();
            foreach ($explicitAnnotation->getFrames() as $frame) {
                if(Likelihood::name($frame->getPornographyLikelihood()) == 'Likely' || Likelihood::name($frame->getPornographyLikelihood()) == 'VERY_LIKELY'){
                    $a = 1;
                }
            }
        } else {
            print_r($operation->getError());
        }
        if($a == 1){
            Mail::to(Auth::user()->email)->send(new MediaRejectMail());
            UserContent::where('id',$userContent_obj->id)->update(['status' => 2]); //status 2 means video blocked
        } else {
            UserContent::where('id',$userContent_obj->id)->update(['status' => 1]); // status 1 means video active and used for further processing
         }
    }
    public function getGenres(){
        $id = request()->id;
        $taxonomis = Taxonomie::where('parent_id',$id)
                                ->where('type','video_level')
                                ->get();
        if(count($taxonomis) > 0){
            return response()->json(['genres' => $taxonomis]);
        }else{
            return response()->json(['genres' => []]);
        }
        
    }
    public function addUserLikes(){
        $competition_user_id = request()->competition_user_id;
        $user_id              = request()->user_id;
        $is_like = 0;
        $already_like = CompetitionUserLike::where('user_like_id',Auth::user()->id)
                            ->where('competition_user_id',$competition_user_id)
                            ->count();
        if($already_like > 0){
            CompetitionUserLike::where('user_like_id',Auth::user()->id)
                            ->where('competition_user_id',$competition_user_id)->delete();
        }else{
           $competition_user_like                       = new CompetitionUserLike();
           $competition_user_like->competition_user_id  = $competition_user_id;
           $competition_user_like->user_like_id         = Auth::user()->id;
           $competition_user_like->expression           = 'like';
           $competition_user_like->save();
           $is_like = 1;
        }
        $total_like = CompetitionUserLike::where('competition_user_id',$competition_user_id)
                            ->count();
        return response()->json(['is_like' => $is_like,'total_like' => $total_like]);
    }
    public function competitionUserJudge(){
        // dd(request()->all());
        $crown = request()->crown_;
        $c_id  = request()->competition_user_id;
        $com = CompetitionUserJudge::where('user_judge_id',Auth::user()->id)
                                    ->where('competition_id',request()->competition_id)->first();
        if($com){
            $error  = 'Already you have give the voting';
            return redirect()->back()->with(compact('error'));
        }
        foreach ($crown as $key => $value) {
            if(empty($value)){
                $error  = 'All Field is required';
                return redirect()->back()->with(compact('error'));
                // ->with('error','All Field is required.');
            }
        }
        $com = CompetitionUserJudge::where('user_judge_id',Auth::user()->id)
                                    ->where('competition_id',request()->competition_id)->first();
        if($com){
            $error  = 'Already you have give the voting';
            return redirect()->back()->with(compact('error'));
        }else{
           foreach ($crown as $key => $value) {
                $competition_judge = new CompetitionUserJudge();
                $competition_judge->competition_user_id = request()->competition_user_id[$key];
                $competition_judge->competition_id      = request()->competition_id;
                $competition_judge->user_judge_id       = Auth::user()->id;
                $competition_judge->expression          = $value;
                $competition_judge->save();
            }
            $success  = 'You have vote successfully';
            return redirect()->back()->with(compact('success'));
        }
    }
    public function getUserCompetition(){
        // dd(Auth::user());
        $active = 0;
        $inactive = 0;
        $hold = 0;
        // dd(Auth::user()->id);
        $active_competiton = Competition::where('is_active',1)
                                        ->whereHas('singleCompetitionDetail',function($q){
                                            $q->where('user_id',Auth::user()->id);
                                        })
                                        ->with('singleCompetitionDetail.userContent')
                                        ->orderBy('created_at','desc')
                                        ->limit(3)
                                        ->get();


        $inactive_competiton = Competition::where('is_active',0)
                                        ->whereHas('singleCompetitionDetail',function($q){
                                            $q->where('user_id',Auth::user()->id);
                                        })
                                            ->with('singleCompetitionDetail.userContent')
                                            ->orderBy('created_at','desc')
                                            ->limit(3)
                                            ->get();
        $hold_competiton = Competition::where('is_active',5)
                                         ->whereHas('singleCompetitionDetail',function($q){
                                            $q->where('user_id',Auth::user()->id);
                                        })
                                        ->with('singleCompetitionDetail.userContent')
                                        ->orderBy('created_at','desc')
                                        ->limit(3)
                                        ->get();
        // dd($hold_competiton);

        if(count($active_competiton) > 0){
            $active = 1;
        }
        if(count($inactive_competiton) > 0){
            $inactive = 1;
        }
        if(count($hold_competiton) > 0){
            $hold = 1;
        }
        $arr  =  [
            'active_competiton_status'   => $active,
            'inactive_competiton_status' => $inactive,
            'hold_competition_status'    => $hold,
            'inactive_competiton'        => $inactive_competiton,
            'hold_competiton'            => $hold_competiton,
            'active_competiton'          => $active_competiton,
        ];
        return response()->json($arr);
    }
}
