<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginFormRequest;
use App\Http\Requests\UserDetailFormRequest;
use App\Modals\Competition;
use App\Modals\CompetitionUser;
use App\Modals\CompetitionUserJudge;
use App\Modals\Country;
use App\Modals\Region;
use App\Modals\Report;
use App\Modals\State;
use App\Modals\Taxonomie;
use App\Modals\User;
use App\Modals\UserDetail;
use App\Modals\UserTier;
use App\Modals\User_sharedUrlCount;
use App\Utils\GoogleCloudRequest;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\Datatables;

// use App\Modals\Taxonomie;
class UserController extends Controller
{

    private $user;

    /**
     * UserController constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function doLogin(LoginFormRequest $request)
    {
        // dd(request()->all());
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);
        if ($validator->fails()) {
            $errors = [];
            $errors['status'] = 2;
            $errors['message'] = 'Validation Error';
            $errors['errors'] = $validator->messages();
            return response()->json($errors, 422);
        }
        $data = [];
        $remember_me = $request->has('remember_me') ? true : false;
        if (auth()->attempt(['email' => $request->email, 'password' => $request->password], $remember_me)) {
            $data['status'] = 1;
            $data['message'] = "Login success";
        } else {
            $data['status'] = 0;
            $data['message'] = "Credential is incorrect";
        }
        return response()->json($data);
    }

    public function doRegister(Request $request)
    {
        // dd(request()->all());
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
            'full_number' => 'required|max:30|unique:users,phone',
            'password' => 'required|confirmed|min:6',
        ]);
        if ($validator->fails()) {
            $errors = [];
            $errors['status'] = 2;
            $errors['message'] = 'Validation Error';
            $errors['errors'] = $validator->messages();
            return json_encode($errors);
        }
        $data = [];
        $taxonomie = Taxonomie::where('type', 'user_level')
            ->where('name', 'Premier')
            ->first();
        $request->merge(['taxonomie_id' => $taxonomie->id, 'phone' => request()->full_number]);
        $user = User::create($request->all());
        // $user->assignRole('user');
        if ($user) {
            $category_media = Taxonomie::where('type', 'video_level')
                ->where('parent_id', 0)
                ->get();
            $taxno = Taxonomie::where('type', 'user_level')
                ->where('name_key', 'premier')
                ->where('parent_id', 0)
                ->first();
            foreach ($category_media as $key => $value) {
                $user_tier = new UserTier();
                $user_tier->category_id = $value->id;
                $user_tier->taxonomie_id = $taxno->id;
                $user_tier->user_id = $user->id;
                $user_tier->save();
            }
            $data['status'] = 1;
            $data['message'] = 'new account created';
            $data['data'] = [
                'user_id' => $user->id,
            ];
        } else {
            $data['status'] = 0;
            $data['message'] = 'Something went wrong';
            $data['data'] = [];
        }
        return response()->json($data);
    }

    public function verifyPhoneNumber(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required',
        ]);
        $response = [];
        if ($validator->fails()) {
            $errors = [];
            $errors['status'] = 2;
            $errors['message'] = 'Validation Error';
            $errors['errors'] = $validator->messages();
            return json_encode($errors);
        }
        $code = request()->code;
        $user = User::where('verification_code', $code)->first();
        if ($user) {
            $user->phone_status = 'verified';
            $user->save();
            $response['status'] = 1;
            return response()->json($response);
        } else {
            $response['status'] = 0;
            return response()->json($response);
        }
    }

    public function userRole()
    {

        $role = request()->role;
        $user_id = request()->user_id;

        if ($user_id) {

            $user = User::where('id', $user_id)->first();
            switch ($role) {
                case 'talent':
                    $user->assignRole('user');
                    break;
                case 'fan':
                    $user->assignRole('fan');
                    break;
                case 'agent':
                    $user->assignRole('agent');
                    break;
                case 'sponser':
                    $user->assignRole('sponsor');
                    break;
            }

            $rand = rand(1111, 9999);

            sendVerificationMessage($user->phone, 'Your Verification Code is ' . $rand);

            $user->verification_code = $rand;

            $user->save();

            return response()->json(['status' => 1]);
        } else {
            return response()->json(['status' => 0]);
        }

    }

    /**
     * @param UserDetailFormRequest $request
     * @return false|string
     */
    public function addUserDetail(Request $request)
    {
        // dd(request()->all());

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'first_name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'username' => 'required|unique:user_details,username|max:20',
            'date_of_birth' => ((request()->agent_role_hidden == 'agent') || (request()->spoonser_role_hidden == 'spoonser')) ? '' : 'required|date|before:-13 years',
            'gender' => 'required',
            'profile_photo' => 'required|mimes:jpeg,jpg,png',
            'region_id' => 'required',
            'country_id' => 'required',
        ],
            [
                'date_of_birth.before' => 'You must be 13 years or Older',
            ]);

        if ($validator->fails()) {
            $errors = [];
            $errors['status'] = 2;
            $errors['message'] = 'Validation Error';
            $errors['errors'] = $validator->messages();
            return response()->json($errors, 422);
        }
        $data = [];
        $cloudPath = '';
        if (request()->file('profile_photo')) {
            // if(request()->file_canvas){
            //     $img = str_replace('data:image/png;base64,', '', request()->file_canvas);
            //     $img = str_replace(' ', '+', $img);
            //     $img_decoded = base64_decode($img);
            //     $val = detect_safe_search_local_img($img_decoded,1);

            // }else{
            //     $val = detect_safe_search_local_img(request()->file('profile_photo')->getRealPath(),2);

            // }
            // dd($val);
            // if ($val == 1) {
            //     $data['status']   = 0;
            //     $data['message']  = 'Profile Picture content not valid. please upload another';
            //     $data['data'] = [];
            //     return json_encode($data);
            // }
            if (request()->file_canvas) {

                $img = str_replace('data:image/png;base64,', '', request()->file_canvas);
                $img = str_replace(' ', '+', $img);
                $img_decoded = base64_decode($img);
                $bucketName = config('constant.GCS_BUCKET');
                $bucketName = env('GCS_BUCKET', 'gong_bucket');
                $fileContent = file_get_contents(request()->file_canvas);
                $cloudPath = 'uploads/' . $request->file_type . '/' . time() . rand(101010100101, 545454547) . 'png';

                $googleRequest = new GoogleCloudRequest();
                // $isSucceed = $googleRequest->uploadFile($bucketName, $img_decoded, $cloudPath);

            } else {

                $file = $request->file('profile_photo');
                $bucketName = config('constant.GCS_BUCKET');
                $bucketName = env('GCS_BUCKET', 'gong_bucket');
                $fileContent = file_get_contents($file->getRealPath());
                $cloudPath = 'uploads/' . $request->file_type . '/' . time() . $file->getClientOriginalName();
                $googleRequest = new GoogleCloudRequest();
                $isSucceed = $googleRequest->uploadFile($bucketName, $fileContent, $cloudPath);

            }

            // $file = request()->file("profile_photo");
            // $file_unique_name = 'profile_photo' . '-' . time() . '-' . date("Ymdhis") . rand(0, 999) . '.' . $file->guessExtension();
            // $file->move(base_path('public/uploads/user/'), $file_unique_name);
        }
        $userDetail = new UserDetail();
        $userDetail->user_id = request()->user_id;

        $userDetail->first_name = request()->first_name;
        $userDetail->last_name = request()->last_name;
        $userDetail->username = request()->username;
        $userDetail->date_of_birth = ((request()->agent_role_hidden == 'agent') || (request()->spoonser_role_hidden == 'spoonser')) ? '1359-07-29' : request()->date_of_birth;
        $userDetail->gender = request()->gender;
        $userDetail->profile_photo = $cloudPath;

        $userDetail->region_id = request()->region_id;
        $userDetail->country_id = request()->country_id;
        $userDetail->notify_by_app = request()->app_notify;
        $userDetail->notify_by_text = request()->text_notify;
        $userDetail->save();

        $user = User::where('id', request()->user_id)->first();
        // if($user){
        //     $rand = rand(4565434356,598954653);
        //     sendVerificationMessage($user->phone,'Your Verification Code is '.$rand);
        //     $user->verification_code = $rand;
        //     $user->save();
        // }

        // sendVerificationMessage('+923066189896','Hi Mohsin How are you');
        if ($userDetail) {
            $data['status'] = 1;
            $data['message'] = 'new account created';
            $data['data'] = [
                'user' => $userDetail->user()->first(),
            ];
        } else {
            $data['status'] = 0;
            $data['message'] = 'new account created';
            $data['data'] = [];
        }
        if ($data['status'] == 1) {

            auth()->login($data['data']['user']);
            $data['role'] = (Auth::user()->hasRole('user')) ? 1 : 0;
        }
        return json_encode($data);
    }

    /**
     * @return RedirectResponse
     */
    public function doLogout()
    {
        if (auth()->check()) {
            auth()->logout();
        }
        return redirect()->route('home');
    }

    /**
     * @return Factory|RedirectResponse|View
     */
    public function dashboard()
    {
        $data = [];
        $user = auth()->user();
        $taxnomies = Taxonomie::where('type', 'video_level')->with('mediaGenre')->first();
        // dd($taxnomies);
        // $userDetail = $user->userDetail()->first();
        $data['message'] = 'dashboard data';
        $data['data'] = [
            'user' => [
                'created_at' => $user->created_at,
                'email' => $user->email,
                'phone' => $user->phone,
                'tier' => ($user->currentUserTier) ? $user->currentUserTier->tier->name : '',
                'name' => ($user->userDetail) ? $user->userDetail->first_name . ' ' . $user->userDetail->last_name : '-',
                'username' => ($user->userDetail) ? $user->userDetail->username : '-',
                'date_of_birth' => ($user->userDetail) ? $user->userDetail->date_of_birth : '-',
                'profile_photo' => ($user->userDetail) ? $user->userDetail->profile_photo : 'no-image.png',
                'gender' => ($user->userDetail) ? $user->userDetail->gender : '-',
                'region' => ($user->userDetail) ? (($user->userDetail->region) ? $user->userDetail->region->name : '') : '-',
                'country' => ($user->userDetail) ? (($user->userDetail->country) ? $user->userDetail->country->name : '') : '-',
                'mediaTypes' => Taxonomie::where('type', 'video_level')->where('parent_id', 0)->get(),
                'genres' => ($taxnomies) ? (($taxnomies->mediaGenre) ? $taxnomies->mediaGenre : '') : '',
            ],
        ];
        if (empty(Auth::user()->userDetail)) {
            $regions = Region::all();
            $countries = Country::orderBy('name', 'asc')->get();
            $location = ip_info(get_client_ip());
            $data['regions'] = $regions;
            $data['countries'] = $countries;
            $data['location'] = $location;
            // dd($data);
            return view('frontend.users.pages.dashboard', $data);
        }
        return view('frontend.users.pages.dashboard', $data);
    }

    public function ShowDataTable(Request $request)
    {

        $competition = Competition::where('competition_by', Auth::user()->id)->get();
//        echo '<pre>';print_r($competition);exit;
        return DataTables::of($competition)
            ->addColumn('action', function ($competition) {
                if ($competition->IsActive > 0) {
                    return '<button type="submit" style="background:#428bca;border-color:#428bca;color:white;border-radius:7px;" class="fa fa-toggle-on fa-2x" id="butt' . $competition->id . '" onclick="ajax(\'' . $competition->id . '\')"  name="Status"></button>';
                } else {
                    return '<button type="submit" style="background:#d9534f;border-color:#d9534f;color:white;border-radius:7px;" class="fa fa-toggle-off fa-2x" id="butt' . $competition->id . '" onclick="ajax(\'' . $competition->id . '\')"  name="Status"></button>';
                }
            })
            ->addColumn('date', function ($competition) {
                $data = new Carbon($competition->created_at);
                $date = $data->toDateString();
                return $date;
            })

            ->addColumn('delete', function ($competition) {

                $main_category = $competition->tier->name;
                $sub_category = $competition->genre->name;
                $tier = Taxonomie::select("name")->where("id", $competition->eligible_tier_id)->first();
                $tier = $tier->name;
                $total_wins = '';

                return "<button type=\"submit\" class=\"btn btn-primary rounded-pill \" data-toggle=\"modal\"
                                data-target=\"#competitionDetailsModal{$competition->id}\">View</button>
                        <div class=\"modal fade\" id=\"competitionDetailsModal{$competition->id}\" tabindex=\"-1\" role=\"dialog\"
                            aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">
                          <div class=\"modal-dialog\">
                            <div class=\"modal-content\">
                              <div class=\"modal-header\">
                                <h3 class=\"modal-title\" id=\"gridSystemModalLabel\">{$competition->competition_name}</h3>
                                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                                        <span aria-hidden=\"true\">&times;</span>
                                  </button>

                              </div>
                              <div class=\"modal-body\">
                                <div class=\"body-message\">
                                  <h5>Competition Details as below:</h5>
                                  <div class=\"row\">
                                  <div class=\"col-md-6\">
                                    <ul class=\"list-group\">
                                      <li class=\"list-group-item\">Time:<b>{$competition->duration}</b></li>
                                      <li class=\"list-group-item\">Number of competitors:<b>{$competition->no_of_competitiors}</b></li>
                                      <li class=\"list-group-item\">Category : <b>{$main_category}</b></li>
                                      <li class=\"list-group-item\">Genre: <b>{$sub_category}</b></li>
                                    </ul>

                                  </div>
                                  <div class=\"col-md-6\">
                                    <ul class=\"list-group\">
                                      <li class=\"list-group-item\">Tier:<b>{$tier}</b></li>
                                      <li class=\"list-group-item\">Competitor Name:<b>Talent</b></li>
                                      <li class=\"list-group-item\">Competitor Profile image:<img src=\"https://storage.googleapis.com/gong_bucket/uploads//159248986433151461929png\"
                                      style='max-width: 15%;border-radius: 50px;'></li>
                                      <li class=\"list-group-item\">Total Wins:<b>{$total_wins}</b></li>
                                    </ul>
                                  </div>
                                </div>
                              </div>
                            </div>
                              <div class=\"modal-footer\">
                                <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Close</button>
                              </div>
                            </div>
                          </div>
                    </div>";

            })
            ->rawColumns(['date', 'delete'])
            ->addIndexColumn()
            ->make(true);
    }

    public function getDetailCompetition()
    {
        $type = request()->q;
        if ($type == 'all') {
            $competition = Competition::where('is_active', 1)
                ->orderBy('priority', 'desc')
            // ->where('competition_media','video')
            // ->where('comopetition_by_type','auto')
            // ->whereHas('competitionDetail.user.userTier.tier',function($qry){
            //     $qry->where('name_key','premier');
            // })
                ->whereHas('competitionUserDetail.tier', function ($qry) {
                    $qry->where('name_key', 'premier');
                })
                ->with('activeCompetitionDetail.user.userDetail', 'activeCompetitionDetail.userContent', 'activeCompetitionDetail.userLikes')
                ->first();
            $winner_competition = Competition::where('is_active', 1)
                ->orderBy('priority', 'desc')
            // ->MAX('priority')
            // ->where('competition_media','audio')
            // ->where('comopetition_by_type','auto')
            // ->whereHas('competitionDetail.user.tier',function($qry){
            //     $qry->where('name_key','competition-winner');
            // })

            // ->whereHas('competitionDetail.user.userTier.tier',function($qry){
            //     $qry->where('name_key','competition-winner');
            // })
                ->whereHas('competitionUserDetail.tier', function ($qry) {
                    $qry->where('name_key', 'competition-winner');
                })
                ->with('activeCompetitionDetail.user.userDetail', 'activeCompetitionDetail.userContent', 'activeCompetitionDetail.userLikes')
                ->first();
            // dd($winner_competition);
            $popular_competition = Competition::where('is_active', 1)
                ->orderBy('priority', 'desc')
            // ->where('competition_media','video')
            // ->where('comopetition_by_type','auto')
            // ->whereHas('competitionDetail.user.tier',function($qry){
            //     $qry->where('name_key','popular');
            // })

            // ->whereHas('competitionDetail.user.userTier.tier',function($qry){
            //     $qry->where('name_key','popular');
            // })

                ->whereHas('competitionUserDetail.tier', function ($qry) {
                    $qry->where('name_key', 'popular');
                })
                ->with('activeCompetitionDetail.user.userDetail', 'activeCompetitionDetail.userContent', 'activeCompetitionDetail.userLikes')
                ->first();
            $star_competition = Competition::where('is_active', 1)
                ->orderBy('priority', 'desc')
            // ->where('competition_media','video')
            // ->where('comopetition_by_type','auto')
            // ->whereHas('competitionDetail.user.tier',function($qry){
            //     $qry->where('name_key','rising-start');
            // })

            // ->whereHas('competitionDetail.user.userTier.tier',function($qry){
            //     $qry->where('name_key','rising-start');
            // })
                ->whereHas('competitionUserDetail.tier', function ($qry) {
                    $qry->where('name_key', 'rising-start');
                })
                ->with('activeCompetitionDetail.user.userDetail', 'activeCompetitionDetail.userContent', 'activeCompetitionDetail.userLikes')
                ->first();

            $talent_competition = Competition::where('is_active', 1)
                ->orderBy('priority', 'desc')
            // ->where('competition_media','video')
            // ->where('comopetition_by_type','auto')
            // ->whereHas('competitionDetail.user.tier',function($qry){
            //     $qry->where('name_key','got-talent');
            // })

            // ->whereHas('competitionDetail.user.userTier.tier',function($qry){
            //     $qry->where('name_key','got-talent');
            // })
                ->whereHas('competitionUserDetail.tier', function ($qry) {
                    $qry->where('name_key', 'got-talent');
                })
                ->with('activeCompetitionDetail.user.userDetail', 'activeCompetitionDetail.userContent', 'activeCompetitionDetail.userLikes')
                ->first();

            $professional_competition = Competition::where('is_active', 1)
                ->orderBy('priority', 'desc')
            // ->where('competition_media','video')
            // ->where('comopetition_by_type','auto')
            // ->whereHas('competitionDetail.user.tier',function($qry){
            //     $qry->where('name_key','professional');
            // })

            // ->whereHas('competitionDetail.user.userTier.tier',function($qry){
            //     $qry->where('name_key','professional');
            // })

                ->whereHas('competitionUserDetail.tier', function ($qry) {
                    $qry->where('name_key', 'professional');
                })
                ->with('activeCompetitionDetail.user.userDetail', 'activeCompetitionDetail.userContent', 'activeCompetitionDetail.userLikes')
                ->first();

            $serious_competition = Competition::where('is_active', 1)
                ->orderBy('priority', 'desc')
            // ->where('competition_media','video')
            // ->where('comopetition_by_type','auto')
            // ->whereHas('competitionDetail.user.tier',function($qry){
            //     $qry->where('name_key','serious-talent');
            // })
            // ->whereHas('competitionDetail.user.userTier.tier',function($qry){
            //     $qry->where('name_key','serious-talent');
            // })
                ->whereHas('competitionUserDetail.tier', function ($qry) {
                    $qry->where('name_key', 'serious-talent');
                })
                ->with('activeCompetitionDetail.user.userDetail', 'activeCompetitionDetail.userContent', 'activeCompetitionDetail.userLikes')
                ->first();

            $potential_competition = Competition::where('is_active', 1)
                ->orderBy('priority', 'desc')
            // ->where('competition_media','video')
            // ->where('comopetition_by_type','auto')
            // ->whereHas('competitionDetail.user.tier',function($qry){
            //     $qry->where('name_key','potential-star');
            // })
            // ->whereHas('competitionDetail.user.userTier.tier',function($qry){
            //     $qry->where('name_key','potential-star');
            // })
                ->whereHas('competitionUserDetail.tier', function ($qry) {
                    $qry->where('name_key', 'potential-star');
                })
                ->with('activeCompetitionDetail.user.userDetail', 'activeCompetitionDetail.userContent', 'activeCompetitionDetail.userLikes')
                ->first();

            $star_a_competition = Competition::where('is_active', 1)
                ->orderBy('priority', 'desc')
            // ->where('competition_media','video')
            // ->where('comopetition_by_type','auto')
            // ->whereHas('competitionDetail.user.tier',function($qry){
            //     $qry->where('name_key','star');
            // })
            // ->whereHas('competitionDetail.user.userTier.tier',function($qry){
            //     $qry->where('name_key','star');
            // })
                ->whereHas('competitionUserDetail.tier', function ($qry) {
                    $qry->where('name_key', 'star');
                })
                ->with('activeCompetitionDetail.user.userDetail', 'activeCompetitionDetail.userContent', 'activeCompetitionDetail.userLikes')
                ->first();

            $super_star_competition = Competition::where('is_active', 1)
                ->orderBy('priority', 'desc')
            // ->where('competition_media','video')
            // ->where('comopetition_by_type','auto')
            // ->whereHas('competitionDetail.user.tier',function($qry){
            //     $qry->where('name_key','superstar');
            // })
            // ->whereHas('competitionDetail.user.userTier.tier',function($qry){
            //     $qry->where('name_key','superstar');
            // })
                ->whereHas('competitionUserDetail.tier', function ($qry) {
                    $qry->where('name_key', 'superstar');
                })
                ->with('activeCompetitionDetail.user.userDetail', 'activeCompetitionDetail.userContent', 'activeCompetitionDetail.userLikes')
                ->first();
            if ($competition || $winner_competition || $popular_competition || $star_competition || $talent_competition || $professional_competition || $serious_competition || $potential_competition || $star_a_competition || $super_star_competition) {
                return response()->json([
                    'competition' => $competition,
                    'winner_competition' => $winner_competition,
                    'popular_competition' => $popular_competition,
                    'star_competition' => $star_competition,
                    'talent_competition' => $talent_competition,
                    'professional_competition' => $professional_competition,
                    'serious_competition' => $serious_competition,
                    'potential_competition' => $potential_competition,
                    'star_a_competition' => $star_a_competition,
                    'super_star_competition' => $super_star_competition,

                    'status' => 1,
                ]);
            } else {
                return response()->json(['status' => 0]);
            }
        } else {
            return $this->categoryWiseGet($type);
        }

    }

    public function categoryWiseGet($type)
    {
        $competition = Competition::where('is_active', 1)
            ->orderBy('priority', 'desc')
        // ->where('comopetition_by_type','auto')
            ->whereHas('competitionUserDetail.tier', function ($qry) {
                $qry->where('name_key', 'premier');
            })
            ->whereHas('tier', function ($qry) use ($type) {
                $qry->where('name_key', $type);
            })
            ->with('activeCompetitionDetail.user.userDetail', 'activeCompetitionDetail.userContent', 'activeCompetitionDetail.userLikes')
            ->first();
        $winner_competition = Competition::where('is_active', 1)
            ->orderBy('priority', 'desc')
        // ->where('comopetition_by_type','auto')
            ->whereHas('competitionUserDetail.tier', function ($qry) {
                $qry->where('name_key', 'competition-winner');
            })
            ->whereHas('tier', function ($qry) use ($type) {
                $qry->where('name_key', $type);
            })
            ->with('activeCompetitionDetail.user.userDetail', 'activeCompetitionDetail.userContent', 'activeCompetitionDetail.userLikes')
            ->first();
        $popular_competition = Competition::where('is_active', 1)
            ->orderBy('priority', 'desc')
        // ->where('comopetition_by_type','auto')
            ->whereHas('competitionUserDetail.tier', function ($qry) {
                $qry->where('name_key', 'popular');
            })
            ->whereHas('tier', function ($qry) use ($type) {
                $qry->where('name_key', $type);
            })
            ->with('activeCompetitionDetail.user.userDetail', 'activeCompetitionDetail.userContent', 'activeCompetitionDetail.userLikes')
            ->first();
        $star_competition = Competition::where('is_active', 1)
            ->orderBy('priority', 'desc')
        // ->where('comopetition_by_type','auto')
            ->whereHas('competitionUserDetail.tier', function ($qry) {
                $qry->where('name_key', 'rising-start');
            })
            ->whereHas('tier', function ($qry) use ($type) {
                $qry->where('name_key', $type);
            })
            ->with('activeCompetitionDetail.user.userDetail', 'activeCompetitionDetail.userContent', 'activeCompetitionDetail.userLikes')
            ->first();

        $talent_competition = Competition::where('is_active', 1)
            ->orderBy('priority', 'desc')
        // ->where('comopetition_by_type','auto')
            ->whereHas('competitionUserDetail.tier', function ($qry) {
                $qry->where('name_key', 'got-talent');
            })
            ->whereHas('tier', function ($qry) use ($type) {
                $qry->where('name_key', $type);
            })
            ->with('activeCompetitionDetail.user.userDetail', 'activeCompetitionDetail.userContent', 'activeCompetitionDetail.userLikes')
            ->first();

        $professional_competition = Competition::where('is_active', 1)
            ->orderBy('priority', 'desc')
        // ->where('comopetition_by_type','auto')
            ->whereHas('competitionUserDetail.tier', function ($qry) {
                $qry->where('name_key', 'professional');
            })
            ->whereHas('tier', function ($qry) use ($type) {
                $qry->where('name_key', $type);
            })
            ->with('activeCompetitionDetail.user.userDetail', 'activeCompetitionDetail.userContent', 'activeCompetitionDetail.userLikes')
            ->first();

        $serious_competition = Competition::where('is_active', 1)
            ->orderBy('priority', 'desc')
        // ->where('comopetition_by_type','auto')

            ->whereHas('competitionUserDetail.tier', function ($qry) {
                $qry->where('name_key', 'serious-talent');
            })
            ->whereHas('tier', function ($qry) use ($type) {
                $qry->where('name_key', $type);
            })
            ->with('activeCompetitionDetail.user.userDetail', 'activeCompetitionDetail.userContent', 'activeCompetitionDetail.userLikes')
            ->first();

        $potential_competition = Competition::where('is_active', 1)
            ->orderBy('priority', 'desc')
        // ->where('comopetition_by_type','auto')
            ->whereHas('competitionUserDetail.tier', function ($qry) {
                $qry->where('name_key', 'potential-star');
            })
            ->whereHas('tier', function ($qry) use ($type) {
                $qry->where('name_key', $type);
            })
            ->with('activeCompetitionDetail.user.userDetail', 'activeCompetitionDetail.userContent', 'activeCompetitionDetail.userLikes')
            ->first();

        $star_a_competition = Competition::where('is_active', 1)
            ->orderBy('priority', 'desc')
        // ->where('comopetition_by_type','auto')
            ->whereHas('competitionUserDetail.tier', function ($qry) {
                $qry->where('name_key', 'star');
            })
            ->whereHas('tier', function ($qry) use ($type) {
                $qry->where('name_key', $type);
            })
            ->with('activeCompetitionDetail.user.userDetail', 'activeCompetitionDetail.userContent', 'activeCompetitionDetail.userLikes')
            ->first();

        $super_star_competition = Competition::where('is_active', 1)
            ->orderBy('priority', 'desc')
        // ->where('comopetition_by_type','auto')
            ->whereHas('competitionUserDetail.tier', function ($qry) {
                $qry->where('name_key', 'superstar');
            })
            ->whereHas('tier', function ($qry) use ($type) {
                $qry->where('name_key', $type);
            })
            ->with('activeCompetitionDetail.user.userDetail', 'activeCompetitionDetail.userContent', 'activeCompetitionDetail.userLikes')
            ->first();

        if ($competition || $winner_competition || $popular_competition || $star_competition || $talent_competition || $professional_competition || $serious_competition || $potential_competition || $star_a_competition || $super_star_competition) {
            return response()->json([
                'competition' => $competition,
                'winner_competition' => $winner_competition,
                'popular_competition' => $popular_competition,
                'star_competition' => $star_competition,
                'talent_competition' => $talent_competition,
                'professional_competition' => $professional_competition,
                'serious_competition' => $serious_competition,
                'potential_competition' => $potential_competition,
                'star_a_competition' => $star_a_competition,
                'super_star_competition' => $super_star_competition,

                'status' => 1,
            ]);
        } else {
            return response()->json(['status' => 0]);
        }
    }

    public function getStates()
    {
        $country_id = request()->id;
        $states = State::where('country_id', '=', $country_id)->orderBy('name', 'asc')->get();
        return response()->json(['states' => $states]);
    }

    public function votingMedia()
    {
        $key = 'bRuD5WYw5wd0rdHR9yLlM6wt2vteuiniQBqE70nAuhU=';
        $q = '';
        if (strlen(request()->q) > 50) {
            $q = decryptString(request()->q, $key);
        } else {
            $q = request()->q;
        }
        $countries = Country::orderBy('name', 'asc')->get();
        $regions = Region::all();
        $location = ip_info(get_client_ip());
        $competition = Competition::where('id', $q)
            ->with('activeCompetitionDetail.user', 'activeCompetitionDetail.userContent', 'activeCompetitionDetail.userLikes', 'tier', 'activeCompetitionDetail.userJudge')
            ->first();

        $competition_user_judge = CompetitionUserJudge::where('competition_id', $q)
        // ->groupBy('competition_id','user_judge_id')
            ->count();
        // dd($competition_user_judge);
        $competition_user_winner = CompetitionUserJudge::
            select(DB::raw('competition_user_id,count("competition_user_id") as winner'))
            ->where('competition_id', $q)
            ->where('expression', 1)
            ->groupBy('competition_user_id')
            ->with('competitionUser.user')
            ->get();

        $c_id = $competition_user_winner->max('winner');
        $competition_user = CompetitionUser::where('id', $c_id)->with('user.userDetail')->first();
        $profanity = Report::all();

        // dd($competition_user);

        if ($competition) {
            return view('frontend.users.pages.judge-media', compact('competition', 'countries', 'competition_user_judge', 'competition_user', 'location', 'profanity'));
        } else {
            return redirect()->back();
        }
    }
    public function getShareableLink()
    {
        $key = 'bRuD5WYw5wd0rdHR9yLlM6wt2vteuiniQBqE70nAuhU=';
        $url = URL::previous();
        $lastParam = explode("?q=", $url);
        $id = end($lastParam);
        $lastParam = encryptString($id, $key);
        $sharedUrl = url('/') . "/voting?q=" . $lastParam;

        return response()->json(['sharedUrl' => $sharedUrl]);
    }
    public function getShareableLinkCount()
    {

        $key = 'bRuD5WYw5wd0rdHR9yLlM6wt2vteuiniQBqE70nAuhU=';
        $competitionId = '';
        $userId = request()->user_id;
        if (strlen(request()->competition_id) > 50) {
            $competitionId = decryptString(request()->competition_id, $key);
        } else {
            $competitionId = request()->competition_id;
        }
        $counters = '';
        $isCountExist = User_sharedUrlCount::where(["user_id" => $userId, "competition_id" => $competitionId])->exists();
        if (!$isCountExist) {
            $counter = new User_sharedUrlCount();
            $counter->user_id = $userId;
            $counter->competition_id = $competitionId;
            $counter->value = 1;
            $counter->save();

            if ($counter->id > 0) {
                $counters = User_sharedUrlCount::all();
                return response()->json(['status' => 1, 'sharedCount' => count($counters)]);
            } else {
                return response()->json(['status' => 0, 'message' => "Something Went Wrong"]);
            }
        } else {
            $current_counter = User_sharedUrlCount::select("value")->where(["user_id" => $userId, "competition_id" => $competitionId])->first();

            $finalcounter = $current_counter->value + 1;
            $counter_update = User_sharedUrlCount::where(["user_id" => $userId, "competition_id" => $competitionId])
                ->update(['value' => $finalcounter]);
            if ($counter_update) {
                $counters = User_sharedUrlCount::all();
                return response()->json(['status' => 1, 'sharedCount' => count($counters)]);
            } else {
                return response()->json(['status' => 0, 'message' => "Something Went Wrong"]);
            }
        }
    }
    public function getShareableLinkCountAll()
    {
        $counters = User_sharedUrlCount::all();
        return response()->json(['status' => 1, 'sharedCount' => count($counters)]);
    }

}
