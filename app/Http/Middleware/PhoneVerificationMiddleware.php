<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class PhoneVerificationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->phone_status == 'verified'){
            return $next($request);
        }else{
            return redirect()->back();
        }
        
    }
}
