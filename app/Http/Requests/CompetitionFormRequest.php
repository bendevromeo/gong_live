<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompetitionFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'competition_name' => 'required|max:50',
            'create_competition_type' => 'required',
            'create_category_id' => 'required',
            'create_genre_id' => 'required',
            'file_type' => 'required',
            'competition_set' => 'required',
            'no_of_competitior' => 'required',
            // 'no_of_round' => 'required',
            'voting_time' => 'required',
            'competition_duration' => 'required',
            'access' => 'required',
            'eligible_tier' => 'required',
            'countries' => 'required',
            // 'region' => 'required',
        ];
    }
}
