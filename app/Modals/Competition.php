<?php

namespace App\Modals;

use Illuminate\Database\Eloquent\Model;
use Auth;
class Competition extends Model
{
	protected $table = 'competitions';
    public function comments()
    {
        return $this->hasMany(Comment::class, 'competition_id', 'id');
    }
    public function competitionDetail(){
    	return $this->hasMany(CompetitionUser::class,'competition_id','id');
    }
    public function activeCompetitionDetail(){
        return $this->hasMany(CompetitionUser::class,'competition_id','id')->where('is_active',1);
    }
    public function winnerNotFurtherUsedCompetitionDetail(){
        return $this->hasOne(CompetitionUser::class,'competition_id','id')
                    ->where('winner',1)
                    ->where('is_added',0);
    }
    // its used for get winner in the competition
    // public function singleWinnerNotFurtherUsedCompetitionDetail(){
    //     return $this->hasOne(CompetitionUser::class,'competition_id','id')
    //                 ->where('winner',1)
    //                 ->where('is_added',0);
    // }

    public function winnerCompetitionDetail(){
        return $this->hasOne(CompetitionUser::class,'competition_id','id')->where('winner',1);
    }

    public function competitionUserDetail(){
    	return $this->belongsTo(UserTier::class,'competition_user_level_id','id');
    }
    public function elegible(){
        return $this->belongsTo(UserTier::class,'eligible_tier_id','taxonomie_id')->where('is_competition',0);
    }
    public function tier(){
    	return $this->belongsTo(Taxonomie::class,'category_id','id');
    }

    public function judge(){
        return $this->hasMany(CompetitionUserJudge::class,'competition_id','id');
    }
    public function singleCompetitionDetail(){
        return $this->hasOne(CompetitionUser::class,'competition_id','id')
                    ->where('user_id',Auth::user()->id);
    }
    public function country(){
        return $this->belongsTo(Country::class,'country_id','id');
    }
    public function region(){
        return $this->belongsTo(State::class,'region_id','id');
    }
    public function genre(){
        return $this->belongsTo(Taxonomie::class,'genre_id','id');
    }
    public function parentCompetition(){
        return $this->belongsTo(Competition::class,'parent_id','id');
    }
    public function childCompetition(){
        return $this->hasMany(Competition::class,'parent_id','id');
    }
    public function activeChildCompetition(){
        return $this->hasMany(Competition::class,'parent_id','id')
                    ->where('is_active',1);
    }
    public function activeForChildCompetition(){
        return $this->hasMany(Competition::class,'parent_id','id')
                    ->where('is_active',1);
    }


    public function getRoundCompetitior(){
        return $this->belongsTo(CompetitorSet::class,'competitior_set_id','id');
    }


}
