<?php

namespace App\Modals;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    public function Album(){
        $this->belongsTo(Album::class);
    }
}
