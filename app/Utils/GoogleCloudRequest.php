<?php


namespace App\Utils;


use Exception;
use Google\Cloud\Storage\StorageClient;

class GoogleCloudRequest{

    /**
     * @var string
     * private key generated from google console
     */
    private $privateKeyFileContent;

    /**
     * GoogleCloudRequest constructor.
     */
    public function __construct(){
        $this->privateKeyFileContent = '{
            "type": "service_account",
            "project_id": "gong-2",
            "private_key_id": "9111046614af7beab8eb6c052308d01895b36e45",
            "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC4n1ytKZ3b2rzN\nyU3Aihh81TiMeos5FBJsmSE5dLm6k1myE1bJcjrrmRjPBJbNuLl2r320Sbd6vnO7\nsSmYG7ywfv5o45y9aGID7wT8DJ6uJJQw4vpNRXPYMppfzc+3WmD4YWN3qKddEBoq\n9PhUlavIR+2XO4O5Apy2ULGRKcgEjfK7YHxLeDDTmV7naDu6N2VeycQ0z0QvWZCj\nSaqC9EOiXyUrcpclawD3BPq/6vOBTiO4vno0MFQdsm4ZiuX/61hJEZdVtCe9NAdf\nFf0Jo/Gos6HanmykCWTiIyAJSLxq6B0uat6CbMRFq6fzrGBLosQEmkZxJmjZdlJ0\nxqr39LJ/AgMBAAECggEAUfptT/S9RCzuDiB3Vmnbcr7I49PcMyWfArrCHn2epOKd\n/u0Iad2/nPK3qeDbR5DhRXqqQh5Diwy7XhnOIt0aHXLea9u5sv8BGscM+xBte10B\nsm1vAFW5GURq+ORt+ff3PAMehDkOy8uFGxL4m7Jz+zQ5IyCJnmKzDF9rYU5gouhT\n2eWb/q+7t95OEyX2KQYtpWJ80akcr70GhrLTAjDA0exrhoiIGSdSM5KIBm60l/sz\niLlsQwd4hTQ6kKocM6CAIVBrx0P644XqdH/Z1Abnd/BQZGxhKeq1c99CogZzCmZG\nrBEQ4VCHWYplXWqgPyGK8DNZHVeFSuEzvTDrU3LdRQKBgQD6tAiZMlz3FBWZMpsa\ndQ8Xy6uTr/+lotJXMZjGLS/kRcn1E1ii/uiNZAs913jYpZX6XqCneu0N41g27a22\nkLEJDP4zWba/vCT+OYKJYd2ovhJ7fSDELiGgy9jq5XdRUWmnfBAd8eaUq+4njZJE\ncqAlmRfBVzgmEJPg2a2XOqIihQKBgQC8heu0qhSequPFaXZtbCAnyUNNfaD4O3cp\nt6I2T4gzQ78WwkfguGxinT+d6EbL65Y1gGAeQqmjDsu3vMyQeSPsS5ooK9Sjnz7Z\nIOksF4/ITeb7lBMQFVl4OX1WFkGvtaGD/AzT9XQ/SQCws3I4FqmDLFb51S4ga3I6\nF7q+42YqMwKBgGdtMILrGRlBqutpyj0u/8wEVUN3f1M5Cee46VsQt0pIVFoL4ixU\nV2JFXLHlm9Bc97OzqDmP6A4rtW/rvBeuN6sRNim/IDqXhvduiGDfBBAiYgOaCpOE\nqJDqqBG4GvJsUNcswoVoD4VULMsah7Y/R7xbHUbBEcP6YNeJVikmok71AoGAP3sI\n54jt8I+yquLcUfQ6z6rh0tIexfegJJgFMyyMvFZvngZFldIEDSDOLynrzPbDWbik\npQvle1acGn6kk3F3OzYp7qTzsTUKQKroWUSF0fkJcmVmFeBahDU3kyljbPtIK91P\npH/eaQD4DogeSey3n8fqca3rHleP2yfqZiTEXikCgYEAqM07JWuFmiU0QtQh4sqi\nmZIha8OBaZmk+UnI9vF+O4i5NijAHzPnah92mSnhhx9oAfg7bQiMKUXoQ5OAVodw\nVD865KeM2GZnbCGhZTRfb/m3OgLLESS/PpDK9Xo/4Xwgp9AZ2tk+SV8/OgZ4C4/h\nY0dnP03E6imaYia3sbuMCKg=\n-----END PRIVATE KEY-----\n",
            "client_email": "523214905658-compute@developer.gserviceaccount.com",
            "client_id": "118398233219130847683",
            "auth_uri": "https://accounts.google.com/o/oauth2/auth",
            "token_uri": "https://oauth2.googleapis.com/token",
            "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
            "client_x509_cert_url":"https://www.googleapis.com/robot/v1/metadata/x509/523214905658-compute%40developer.gserviceaccount.com"
        }';
    }

    /*
     * NOTE: if the server is a shared hosting by third party company then private key should not be stored as a file,
     * may be better to encrypt the private key value then store the 'encrypted private key' value as string in database,
     * so every time before use the private key we can get a user-input (from UI) to get password to decrypt it.
     */
    public function uploadFile($bucketName, $fileContent, $cloudPath)
    {
        // connect to Google Cloud Storage using private key as authentication
        try {
            $storage = new StorageClient([
                'keyFile' => json_decode($this->privateKeyFileContent, true)
            ]);

        } catch (Exception $e) {
            // maybe invalid private key ?
            print $e;
            return false;
        }

        // set which bucket to work in
        $bucket = $storage->bucket($bucketName);

        // upload/replace file
        $storageObject = $bucket->upload(
            $fileContent,
            ['name' => $cloudPath]
        // if $cloudPath is existed then will be overwrite without confirmation
        // NOTE:
        // a. do not put prefix '/', '/' is a separate folder name  !!
        // b. private key MUST have 'storage.objects.delete' permission if want to replace file !
        );

        // is it succeed ?

        return $storageObject != null;
    }

    /**
     * @param $bucketName
     * @param $cloudPath
     * @return array|bool
     * this (listFiles) method not used in this example but you may use according to your need
     */
    public function getFileInfo($bucketName, $cloudPath)
    {
        // connect to Google Cloud Storage using private key as authentication
        try {
            $storage = new StorageClient([
                'keyFile' => json_decode($this->privateKeyFileContent, true)
            ]);
        } catch (Exception $e) {
            // maybe invalid private key ?
            print $e;
            return false;
        }

        // set which bucket to work in
        $bucket = $storage->bucket($bucketName);
        $object = $bucket->object($cloudPath);
        return $object->info();
    }

    public function listFiles($bucket, $directory = null)
    {

        if ($directory == null) {
            // list all files
            $objects = $bucket->objects();
        } else {
            // list all files within a directory (sub-directory)
            $options = array('prefix' => $directory);
            $objects = $bucket->objects($options);
        }

        foreach ($objects as $object) {
            print $object->name() . PHP_EOL;
            // NOTE: if $object->name() ends with '/' then it is a 'folder'
        }
    }

}
