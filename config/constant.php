<?php

return [
    'GCS_BASE_URL' => env('GCS_BASE_URL'),
    'GCS_BUCKET' => env('GCS_BUCKET'),
    'BUCKET_URL' => ' https://storage.googleapis.com/stagging_bucket/',
    'date_format' => 'm-d-Y',
];
