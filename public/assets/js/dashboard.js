	$(document).on('click', '.button-wrapper.category-lead', function (e) {
        $('.button-wrapper.category-lead').removeClass('active');
        $(this).addClass('active');
        $('input[name="category"]').val($(this).attr('data-value'));
    });
    $(document).on('click', '.button-wrapper.file-type', function (e) {
        // alert(1);
         $('.audio-preview-container').css('width',0)
    	$('.audio-preview-container').css('height',0)
        $('.button-wrapper.file-type').removeClass('active');
        $(this).addClass('active');
        document.querySelector(".audio-preview").src = "";
        $(".audio-preview").hide();
        $(".audio-preview").removeAttr('src');

        document.querySelector(".video-preview").src = "";
        $(".video-preview").hide();
        $(".video-preview").removeAttr('src');

        document.querySelector(".img-preview").src   = "";
        $(".img-preview").hide();
        $(".img-preview").removeAttr('src');

        $('.placeholder_media').show();
        $('.placeholder_media').attr('src',"{{url('/')}}/assets/images/upload-placeholder.png");
        $('.placeholder_media').css('width',525);
        $('.placeholder_media').css('height',);
        if ($(this).attr('data-value') !== $('input[name="file_type"]').val() ){
            $('input[name="file"]').val('').attr('accept', $(this).attr('data-accept'));
            $('.file-preview-container').hide();
        }
        $('input[name="file_type"]').val($(this).attr('data-value'));
    });

    $('.btn_media_type').click(function(){
        $('.loader_video').show();
        $('.category_id').val($(this).data('id'));
        var id = $(this).data('id');
        axios({
            method: 'get',
            url: "{{route('user.content.get-genres')}}?id="+id,
            responseType: 'json'
        }).then(function (response) {
                $('.loader_video').hide();
                var selected = '';
                var data = [];
                $.each(response.data.genres,function(key,item){
                   selected += '<option  value="'+item.id+'">'+item.name+'</option>';
                })
                $('.genre_id').html(selected);
        }).catch(function (error) {
        }).finally(function () {
        });
    })

     $(document).on('change', 'input[name="file"]', function (e) {
            let fileType = $('input[name="file_type"]').val();
            let fileUrl = URL.createObjectURL(this.files[0]);
            $('.file-error-section').hide();
            $('.audio-preview-container').css('width',0)
            $('.audio-preview-container').css('height',0)
            // console.log(fileUrl);
            if (fileType !== "image"){
                if(fileType === "video"){

                    let videoTag = document.querySelector(".video-preview");
                    console.log(fileUrl);

                    videoTag.src = fileUrl;
                    console.log(videoTag);
                            // $('.video-preview-container').show();

                    $(".video-preview").show();
                    videoTag.onloadedmetadata = function() {
                        let videoDuration = videoTag.duration;
                        console.log(videoDuration);
                        if (videoDuration < MIN_MEDIA_DURATION || videoDuration > MAX_MEDIA_DURATION){
                            videoTag.src = '';
                            console.log(1122);
                            $('input[name="file"]').val('');
                            $('.file-preview-container').hide();
                            // alert('video length is not correct');
                            $('.error_msg').html('Video length is not correct');
                            $('.error_msg').show();
                            setTimeout(function(){ $('.error_msg').hide(); }, 3000);

                        }else{
                            console.log(1230);
                            $('.video-preview-container').show();
                            $('.file-error-section').hide();
                            document.querySelector(".video-preview").addEventListener('timeupdate', function() {
                            var _VIDEO = document.querySelector(".video-preview"),
                                _CANVAS = document.querySelector("#canvas-element"),
                                _CANVAS_CTX = _CANVAS.getContext("2d");

                            // Placing the current frame image of the video in the canvas
                            _CANVAS_CTX.drawImage(_VIDEO, 2, 2, _VIDEO.videoWidth, _VIDEO.videoHeight);
                            $('.img_thum').val(_CANVAS.toDataURL());

                            // Setting parameters of the download link
                            // document.querySelector("#download-link").setAttribute('href', _CANVAS.toDataURL());
                            // document.querySelector("#download-link").setAttribute('download', 'thumbnail.png');
                        });
                        }
                    };
                }else if (fileType === "audio"){
                    $(".audio-preview").show();
                    let audioTag = document.querySelector(".audio-preview");
                    audioTag.src = fileUrl;
                    audioTag.onloadedmetadata = function() {
                        let audioDuration = audioTag.duration;
                        // console.log(audioDuration);
                        if (audioDuration < MIN_MEDIA_DURATION || audioDuration > MAX_MEDIA_DURATION){
                            audioTag.src = '';
                            $('input[name="file"]').val('');
                            $('.file-preview-container').hide();
                            // alert('audio length is not correct');
                            $('.error_msg').html('Audio length is not correct');
                            $('.error_msg').show();
                             setTimeout(function(){ $('.error_msg').hide(); }, 3000);
                        }else{
                            $('.placeholder_media').attr('src',"{{url('/')}}/assets/images/backgroundimagebrowse.png");
                            // $('.audio-preview-img').attr('src',"{{url('/')}}/assets/images/backgroundimagebrowse.png");
                            $('.placeholder_media').css('width',525);
                            $('.placeholder_media').css('height',);
                            $('.audio-preview-container').css('width','100%');
                            $('.audio-preview-container').css('height','100%');
                            $('.audio-preview-container').show();
                        }
                    }
                }
            }else{
                $('.img-preview').show();
                $('.img-preview').attr('src',fileUrl);
                $('.file-preview-container').show();
            }

        });

        $(document).on('click', '#submitPublicCompetition', function (e) {

            if (isContentFormValidated()){
                $('#submitPublicCompetition').html(loader())
                $('#submitPublicCompetition').attr('disabled','true');
                $('.my_progress').show();
                let data = new FormData(document.getElementById('contentForm'));
                $.ajax({
                    url: '{{route('user.content.add')}}',
                    data: data,
                    processData: false,
                    contentType: false,
                    type: 'POST',

                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                if(percentComplete == 1){
                                    $('.loader_progess').show();
                                    $('#submitPublicCompetition').hide();
                                }
                                percentComplete = parseInt(percentComplete * 100);
                                $('.myprogress').text(percentComplete + '%');
                                $('.myprogress').css('width', percentComplete + '%');
                            }
                        }, false);
                        return xhr;
                    },
                    success: function ( response ) {
                        if (JSON.parse(response).status){
                            $('#submit-for-competition-2').hide();
                            $('#submit-for-competition-3').show();
                            $('.file_type_show').html($('input[name="file_type"]').val());
                            document.getElementById('contentForm').reset();
                            // setTimeout(function() {
                            //     window.location.href = "{{route('user.addUserDetail')}}";
                            // }, 2000);

                        }
                    },
                    error: function (errors) {
                        console.log(errors);
                    }
                });
            }
        });

        $(document).on('click', '#privateCompetition', function (e) {

            $.ajax({
                    url: "{{route('user.competition.get-public-competition')}}?category_id="+$('.category_id').val()+"&genre_id="+$('[name="genre_id"]').val()+"&type="+$('input[name="file_type"]').val(),
                    type: 'get',
                    dataType:'json',
                    success: function ( response ) {
                       if(response.status == 1){
                        $('.public_competition').html(response.html);
                       }
                    },
                    error: function (errors) {

                    }
                });


            $('.private-competition-1').show();
            $('#submit-for-competition-2').hide();
            $('#submit-for-competition-3').hide();
        })
        $(document).on('click', '.btn_private_code', function (e) {
            $('.btn_private_code').attr('disabled',true);
            $('.error_code_private').html('');
            $(this).html(loader());
            $.ajax({
                    url: "{{route('user.competition.verify_code')}}?code="+$('#code').val()+"&type="+$('input[name="file_type"]').val()+"&category_id="+$('.category_id').val()+"&genre_id="+$('[name="genre_id"]').val(),

                    processData: false,
                    contentType: false,
                    type: 'get',
                    dataType:'json',
                    success: function ( response ) {
                        $('.btn_private_code').html('Submit');
                        // $('.btn_signup_2').html('Complete Sign Up');
                        $('.btn_private_code').attr('disabled',false);
                        if (response.status == 1){
                            $('.competition_name').html(response.competition.competition_name);
                            $('.upload_type').html(response.competition.competition_media);
                            $('.number_of_competitiors').html(response.competition.no_of_competitiors+" Participants");
                            $('.competition_type').html(response.competition.competition_end_type);
                            $('.number_of_rounds').html(response.competition.no_of_rounds+" Rounds");
                            $('.competition_duration').html(response.competition.duration+" Days");
                            $('.genre').html(response.competition.genre.name);
                            $('.voting_time').html(response.competition.voting_interval+" Days");
                            $('.competitiors_set').html(response.competition.competition_set);
                            $('.region').html(response.competition.region.name);
                            $('.country').html(response.competition.country.name);
                            $('.btn_join_competition').data('type',$('input[name="file_type"]').val())
                            $('#join-list-modal').modal('show');
                        }
                        if (response.status == 0){
                            var ht = '<ul>'
                            ht += "<li class='alert alert-danger'>"+response.message+"</li>";
                            ht += '</ul>'
                            $('.error_code_private').html(ht);
                        }
                        if(response.status == 2){
                            var ht = '<ul>'
                            $.each(response.error, function (key, item){
                              ht += "<li class='alert alert-danger'>"+item+"</li>";
                            });
                            ht += '</ul>'
                            $('.error_code_private').html(ht);
                        }

                    },
                    error: function (errors) {
                        console.log(errors);
                        var ht = '<ul>'
                        $.each(errors.responseJSON.errors, function (key, item){
                          ht += "<li class='alert alert-danger'>"+item+"</li>";
                        });
                        ht += '</ul>'
                        $('.btn_private_code').attr('disabled',false);
                    }
                });
        })

        $(document).on('click', '.btn_join_competition', function (e) {
            $('.error_join').html('');
            $('.btn_join_competition').attr('disabled',true);
            $(this).html(loader());
             let data = new FormData(document.getElementById('contentForm'));
            $.ajax({
                    url: "{{route('user.competition.join_private_competition')}}?code="+$('#code').val()+"&type="+$(this).data('type'),
                    processData: false,
                    contentType: false,
                    type: 'post',
                    data:data,
                    dataType:'json',
                    success: function ( response ) {
                        $('.btn_join_competition').html('Join');
                        // $('.btn_signup_2').html('Complete Sign Up');
                        $('.btn_join_competition').attr('disabled',false);
                        if (response.status == 1){
                             $('.file_type_show').html($('input[name="file_type"]').val());
                            $(".private-competition-1").hide();
                            $('.competition_type_a').html('private competition');

                            $('#submit-for-competition-4').show();
                             $('#join-list-modal').modal('hide');
                        }
                        if (response.status == 0){
                            var ht = '<ul>'
                            ht += "<li class='alert alert-danger'>Your Uploaded media type is not correct.</li>";
                            ht += '</ul>'
                            $('.error_join').html(ht);
                        }
                        if(response.status == 2){
                            var ht = '<ul>'
                            $.each(response.error, function (key, item){
                              ht += "<li class='alert alert-danger'>"+item+"</li>";
                            });
                            ht += '</ul>'
                            $('.error_code_private').html(ht);
                        }

                    },
                    error: function (errors) {
                        console.log(errors);
                        var ht = '<ul>'
                        $.each(errors.responseJSON.errors, function (key, item){
                          ht += "<li class='alert alert-danger'>"+item+"</li>";
                        });
                        ht += '</ul>'
                        $('.btn_join_competition').attr('disabled',false);
                    }
                });
        });


        $(document).on('click', '#buttonProceed', function (e) {
            e.preventDefault();
            var file_type = $('input[name="file_type"]').val();
            var _validFileExtensions;
            if(file_type == 'image'){
                _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"];
            }else if(file_type == 'audio'){
                _validFileExtensions = [".mp3", ".wav", ".aiff", ".aac", ".ogg", ".wma"];
            }else if(file_type == 'video'){
                _validFileExtensions = ['.webm',".avi", ".mov", ".mpg", ".m4v", ".wma", ".3gp", ".mp4"];
            }
            var a = ValidateFile(_validFileExtensions);
            if(a == 0){
                return false;
            }
            // return false;

            $('video').trigger('pause');
            $('audio').trigger('pause');
            if (isContentFormValidated()){
                $('#submit-for-competition-1').addClass('d-none');
                $('#submit-for-competition-2').removeClass('d-none');
            }
        });


    function isContentFormValidated() {
            let isDataValid = true;
            if ($('[name="file"]').val() === ""){
                isDataValid = false;
                $('.file-error-section').show();
            }
            if ($('[name="name"]').val() === ""){
                isDataValid = false;
                $('.name-error-section').show();
            }
            if ($('[name="genre_id"]').val() === ""){
                isDataValid = false;
                $('.genre_id-error-section').show();
            }
            if(!$('.privacy-check').is(':checked')){
                isDataValid = false;
                $('.privacy-check').siblings('em').show();
            }
            if(!$('.terms-check').is(':checked')){
                isDataValid = false;
                $('.terms-check').siblings('em').show();
            }
            return isDataValid;
        }

        $(document).on('change', '#Name, #Genre', function (e) {
            // alert(2);
            if ($(this).val() !== ""){
                $("." + $(this).attr('name') + "-error-section").hide();
            }
        })
        $(document).on('click', '.terms-check, .privacy-check', function (e) {
            if($(this).is(':checked')){
                $(this).siblings('em').hide();
            }
        })


        function abc(){
            let fileType = $('input[name="file_type"]').val();
            let fileUrl = URL.createObjectURL(this.files[0]);
            // console.log(fileUrl);
            if (fileType !== "image"){
                if(fileType === "video"){
                    let videoTag = document.querySelector(".video-preview");
                    videoTag.src = fileUrl;
                    $(".video-preview").show();
                    videoTag.onloadedmetadata = function() {
                        let videoDuration = videoTag.duration;
                        if (videoDuration < MIN_MEDIA_DURATION || videoDuration > MAX_MEDIA_DURATION){
                            videoTag.src = '';
                            $('input[name="file"]').val('');
                            $('.file-preview-container').hide();
                            alert('video length is not correct');


                        }else{

                            $('.video-preview-container').show();
                            $('.file-error-section').hide();
                            document.querySelector(".video-preview").addEventListener('timeupdate', function() {
                            var _VIDEO = document.querySelector(".video-preview"),
                                _CANVAS = document.querySelector("#canvas-element"),
                                _CANVAS_CTX = _CANVAS.getContext("2d");

                            // Placing the current frame image of the video in the canvas
                            _CANVAS_CTX.drawImage(_VIDEO, 2, 2, _VIDEO.videoWidth, _VIDEO.videoHeight);
                            $('.img_thum').val(_CANVAS.toDataURL());

                            // Setting parameters of the download link
                            // document.querySelector("#download-link").setAttribute('href', _CANVAS.toDataURL());
                            // document.querySelector("#download-link").setAttribute('download', 'thumbnail.png');
                        });
                        }
                    };
                }else if (fileType === "audio"){
                    $(".audio-preview").show();
                    document.querySelector(".audio-preview").src = fileUrl;
                    $('.placeholder_media').attr('src',"{{url('/')}}/assets/images/audio-pre.jpg");
                    $('.placeholder_media').css('width',525);
                    $('.placeholder_media').css('height',);
                    $('.audio-preview-container').show();
                }
            }else{
                $('.img-preview').show();
                $('.img-preview').attr('src',fileUrl);
                $('.file-preview-container').show();
            }
        }
        function changeIMG(e,a){
            $('.media').removeClass('active');
            $('.user_role').val(a);
            $(e).addClass('active');
        }
        function loadPage(){
            location.reload();
        }
        $(document).ready(function(){

             $(document).on('click', '.pagination a', function(event){
              event.preventDefault();
              var page = $(this).attr('href').split('page=')[1];
              fetch_data(page);
             });

            function fetch_data(page){
              $.ajax({
               url:"{{route('user.competition.get-public-competition')}}?page="+page+"&category_id="+$('.category_id').val()+"&genre_id="+$('[name="genre_id"]').val()+"&type="+$('input[name="file_type"]').val(),
               success:function(response)
               {
                if(response.status == 1){
                    $('.public_competition').html(response.html);
                }

               }
              });
            }

            $(document).on('click', '.btn_competition_show', function(event){
                $.ajax({
                    url: "{{route('user.competition.get-public-competition-detail')}}?id="+$(this).data('id'),

                    processData: false,
                    contentType: false,
                    type: 'get',
                    dataType:'json',
                    success: function ( response ) {
                        if (response.status == 1){
                            $('.public_competition_name').html(response.competition.competition_name);
                            $('.public_upload_type').html(response.competition.competition_media);
                            $('.public_number_of_competitiors').html(response.competition.no_of_competitiors+" Participants");
                            $('.public_competition_type').html(response.competition.competition_end_type);
                            $('.public_number_of_rounds').html(response.competition.no_of_rounds+" Rounds");
                            $('.public_competition_duration').html(response.competition.duration+" Days");
                            $('.public_genre').html(response.competition.genre.name);
                            $('.public_voting_time').html(response.competition.voting_interval+" Days");
                            $('.public_competitiors_set').html(response.competition.competition_set);
                            $('.public_region').html(response.competition.region.name);
                            $('.public_country').html(response.competition.country.name);
                            $('.btn_public_competition_join').data('val',response.competition.id);
                            $('#join-list-modal-2').modal('show');
                        }
                        if (response.status == 0){
                            var ht = '<ul>'
                            ht += "<li class='alert alert-danger'>"+response.message+"</li>";
                            ht += '</ul>'
                            // $('.error_code_private').html(ht);
                        }

                    },
                    error: function (errors) {
                    }
                });
            })
            $('.btn_public_competition_join').click(function(){
                // $('.error_join').html('');
                $('.btn_public_competition_join').attr('disabled',true);
                $(this).html(loader());
                 let data = new FormData(document.getElementById('contentForm'));
                $.ajax({
                        url: "{{route('user.competition.join_public_competition')}}?id="+$(this).data('val')+"&type="+$(this).data('type'),
                        processData: false,
                        contentType: false,
                        type: 'post',
                        data:data,
                        dataType:'json',
                        success: function ( response ) {
                            $('.btn_public_competition_join').html('Join');
                            // $('.btn_signup_2').html('Complete Sign Up');
                            $('.btn_public_competition_join').attr('disabled',false);
                            if (response.status == 1){
                                $('.file_type_show').html($('input[name="file_type"]').val());
                                $(".private-competition-1").hide();
                                $('#submit-for-competition-4').show();
                                $('.competition_type_a').html('public competition');
                                $('#join-list-modal-2').modal('hide');
                            }
                            if (response.status == 0){
                                var ht = '<ul>'
                                ht += "<li class='alert alert-danger'>Your Uploaded media type is not correct.</li>";
                                ht += '</ul>'
                                $('.public_error_join').html(ht);
                            }
                            if(response.status == 2){
                                var ht = '<ul>'
                                $.each(response.error, function (key, item){
                                  ht += "<li class='alert alert-danger'>"+item+"</li>";
                                });
                                ht += '</ul>'
                                $('.error_code_private').html(ht);
                            }

                        },
                        error: function (errors) {
                            console.log(errors);
                            var ht = '<ul>'
                            $.each(errors.responseJSON.errors, function (key, item){
                              ht += "<li class='alert alert-danger'>"+item+"</li>";
                            });
                            ht += '</ul>'
                            $('.btn_public_competition_join').attr('disabled',false);
                        }
                    });
            })

        });

    function ValidateFile(_validFileExtensions) {
        // var arrInputs = oForm.getElementsByTagName("input");
        // for (var i = 0; i < arrInputs.length; i++) {
            // var oInput = arrInputs[i];
            var oInput = document.getElementById("file");

            if (oInput.type == "file") {
                var sFileName = oInput.value;
                if (sFileName.length > 0) {
                    var blnValid = false;
                    for (var j = 0; j < _validFileExtensions.length; j++) {
                        var sCurExtension = _validFileExtensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            break;
                        }
                    }

                    if (!blnValid) {
                        $('.error_msg').html("Your upload file is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                        $('.error_msg').show();
                        setTimeout(function(){ $('.error_msg').hide(); }, 5000);
                        return 0;
                    }
                }
            }
        // }

        return 1;
    }
    function refreshMedia(){
       var elem = document.getElementById('file');
       if(elem && document.createEvent) {
          var evt = document.createEvent("MouseEvents");
          evt.initEvent("click", true, false);
          elem.dispatchEvent(evt);
       }
    }

     var wavesurfer = [];
    function getUserCompetition(){
        $('.avc').html('');
        $('video').trigger('pause');
        $('audio').trigger('pause');
            $('.active_competition_loader').show();
        $('.inactive_competition_loader').show();
        $('.onhold_competition_loader').show();
        $('.active_no_competiton').addClass('hide-display');
        $('.inactive_no_competiton').addClass('hide-display');
        $('.onhold_no_competiton').addClass('hide-display');
     axios({
            method: 'get',
            url: "{{route('user.content.get-user-competition')}}",
            responseType: 'json'
        }).then(function (response) {
            $('.active_competition_loader').hide();
          $('.inactive_competition_loader').hide();
            $('.onhold_competition_loader').hide();
                if(response.data.active_competiton_status == 1){
                    var active_html = '';
                    $.each(response.data.active_competiton,function(key,item){
                       if(item.competition_media == 'video'){
                        if(item.single_competition_detail){
                            active_html += videoHtml(item);
                        }
                        // $('.active_img_detail_page').append(active_html);
                       }
                       if(item.competition_media == 'audio'){
                        if(item.single_competition_detail){
                            active_html += audioHtml(item,key);
                            // wavesurfer[key] = renderWaveForm('{{config("constant.BUCKET_URL")}}'+item.single_competition_detail.user_content.file, '#waveform_'+key);
                        }
                        // $('.active_img_detail_page').append(active_html);
                       }
                       if(item.competition_media == 'image'){
                        if(item.single_competition_detail){
                            active_html += imgHtml(item);
                        }
                        // $('.active_img_detail_page').append(active_html);
                       }
                    });
                }else{
                    $('.active_no_competiton').removeClass('hide-display');
                }
                $('.active_img_detail_page').html(active_html);
                if(response.data.inactive_competiton_status == 1){
                    var inactive_html = '';
                    $.each(response.data.inactive_competiton,function(key,item){
                       if(item.competition_media == 'video'){
                      if(item.single_competition_detail){
                        inactive_html += videoHtml(item);
                     }
                       }
                       if(item.competition_media == 'audio'){
                      if(item.single_competition_detail){
                        inactive_html += audioHtml(item,key);
                       }
                       }
                       if(item.competition_media == 'image'){
                      if(item.single_competition_detail){
                        inactive_html += imgHtml(item);
                       }
                       }
                    });
                }else{
                    $('.inactive_no_competiton').removeClass('hide-display');
                }
            if(response.data.hold_competition_status == 1){
              var hold_html = '';
              $.each(response.data.hold_competiton,function(key,item){
                     if(item.competition_media == 'video'){
                      if(item.single_competition_detail){
                      hold_html += videoHtml(item);
                     }
                     }
                     if(item.competition_media == 'audio'){
                      if(item.single_competition_detail){
                        hold_html += audioHtml(item,key);
                       }
                     }
                     if(item.competition_media == 'image'){
                      if(item.single_competition_detail){
                        hold_html += imgHtml(item);
                       }
                     }
                  });
                $('.onhold_img_detail_page').html(hold_html);
            }else{
              console.log(123);
              $('.onhold_no_competiton').removeClass('hide-display');
            }

              $('.inactive_img_detail_page').html(inactive_html);

                $('.video_class_detail').mediaelementplayer({
                    // Do not forget to put a final slash (/)
                    pluginPath: 'https://cdnjs.com/libraries/mediaelement/',
                    // this will allow the CDN to use Flash without restrictions
                    // (by default, this is set as `sameDomain`)
                    shimScriptAccess: 'always'
                    // more configuration
                });

        }).catch(function (error) {
        }).finally(function () {
        });
    }
    function videoHtml(item){
        var a = `<div class="col-md-4" style="float:left">
                            <div class="post-video-grid border-0 mb-0 pb-0">
                <div class="video-slider">
                  <div class="video-posted mb-0 pb-0 border-bottom-0">
                            <div class="video-card">
                            <a href="{{route('voting')}}?q=`+item.single_competition_detail.competition_id+`" >
                                <div class="player">
                                  <p class="event-name"></p>
                                  <p class="event-category"></p>
                                  <video id="player1" class="video_class_detail" width="640" height="360" preload="none" style="width: 100%; max-width: 100%; height: 280px" controls poster="{{config('constant.BUCKET_URL')}}`+item.single_competition_detail.user_content.thumbnail+`" playsinline webkit-playsinline>
                                     <source src="{{config('constant.BUCKET_URL')}}`+item.single_competition_detail.user_content.file+`" type="video/mp4">
                                  </video>
                                </div>
                                </a>
                                    </div></div>
                            </div></div></div>`;
        return a;
    }
    function imgHtml(item){
        var a = `<div class="col-md-4" style="float:left">

                    <div class="post-img-grid border-0 pb-0 mb-0">
                              <div class="img-posted pb-0 border-0">
                                <div class="item dfbkdjsf">
                                  <a href="{{route('voting')}}?q=`+item.single_competition_detail.competition_id+`" >
                                  <div class="img-card-parent">
                                    <div class="abc"></div>
                                     <p class="event-name"></p>
                                              <p class="event-category"></p>
                                    <div class="img-card">
                                            <img src="{{config('constant.BUCKET_URL')}}`+item.single_competition_detail.user_content.file+`" class="img-responsive " />
                                    </div>
                                  </div>
                                  </a>
                                </div>
                              </div>
                            </div></div>`;
        return a;
    }
    function audioHtml(item,key){
        var a = `<div class="col-md-4" style="float:left"><div class="post-audio-grid border-0 pb-0 mb-0">
                  <div class="audio-posted pb-0 border-0">
                      <div class="audio-owl-slider  owl-theme ">
                        <a href="{{route('voting')}}?q=`+item.single_competition_detail.competition_id+`" >
                          <div id="waveform_`+key+`" class="audio-waveform wave" style="height:200px">
                              <p class="event-name"></p>
                              <p class="event-category"></p>

                              <img src="https://storage.googleapis.com/gong_bucket/uploads/image/1585216412audio.PNG" class="img-responsive img_`+key+`" style="position: absolute;overflow: hidden; margin-top: 54px;margin-left: 19px;" />
                          </div>
                          </a>
                          <div class="">

                          </div>
                      </div>
                  </div>
              </div></div>`;
        return a;
    }

    function renderWaveForm(url, parentSelector) {
      var domEl = document.createElement('div')
      document.querySelector(parentSelector).appendChild(domEl)

      var wavesurferr = WaveSurfer.create({
        container: domEl,
        waveColor: 'red',
        progressColor: 'purple',
        hideScrollbar: true,
        xhr: {
        cache: "default",
        mode: "cors",
        method: "GET",
        // credentials: "gong_bucket",
        headers: [
          { key: "cache-control", value: "no-cache" },
          { key: "Access-Control-Allow-Origin", value: "*" },
          { key: "Origin", value: "http://localhost:8000" },
          { key: "Origin", value: "*" },
          { key: "pragma", value: "no-cache" }
        ]
      }
      });
      wavesurferr.load(url);
      return wavesurferr;
    }