<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Polse</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- App favicon -->
  <!-- App css -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="{{asset('instructor/style/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('instructor/style/icons.min.css')}}">
  <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="_token" content="{{ csrf_token() }}">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
        rel="stylesheet">
  <link href="{{asset('instructor/style/theme-style.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('instructor/style/custom-style.css')}}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" integrity="sha256-ENFZrbVzylNbgnXx0n3I1g//2WeO47XxoPe0vkp3NC8=" crossorigin="anonymous" />
</head>
<body style="padding-bottom: 0; min-height: 100%">
  <div class="register-page ">
    <div class="row hv-100">
      <div class="col-12 col-sm-6 login-bg">
        <div class="d-flex justify-content-center align-items-center h-100">
          <img class="polis-logo" src="{{asset('instructor/images/register-logo.png')}}" alt="polis-logo">
        </div>
      </div>
      <div class="col-12 col-sm-6">
        <div class="d-flex justify-content-center align-items-center h-100">
          <form action="" method="post">
            @csrf
           
            <div class="form-group mb-2">
              <label>Email </label>
              <input type="email" name="email" required="" class="form-control" placeholder="email">
            </div>
            
            <div class="form-group mb-2">
              <label>Password</label>
              <input type="password" name="password" required="" autocomplete="off" class="form-control" placeholder="*********">
            </div>
           
            <div class="custom-control custom-checkbox mb-3">
              <input type="checkbox" class="custom-control-input" id="customCheck1">
              <label class="custom-control-label" for="customCheck1">Remember Me</label>
            </div>
            <div class="form-group text-center">
              <button type="submit" class="btn btn-secondary waves-effect px-4">Login</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
 

<!-- END wrapper --><!-- END wrapper --><!-- END wrapper --><!-- END wrapper -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

<!-- Vendor js -->
<script src="{{asset('instructor/javascript/jquer.js')}}"></script>
<!-- App js-->
<script src="{{asset('instructor/javascript/javascript.js')}}"></script>
<script src="{{asset('js/toastr.js')}}" ></script>
<script type="text/javascript">
  toastr.options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-bottom-left",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    };
    @if(request()->session()->has('flash'))  
    
      toastr.error("{{request()->session()->get('flash')}}", 'Error');
      @php request()->session()->forget('flash')@endphp
    @endif
    @if(request()->session()->has('flash_success')) 
     
      toastr.success("{{request()->session()->get('flash_success')}}", 'Success');
      @php request()->session()->forget('flash_success')@endphp
    @endif

    var KTAppOptions = {
    "colors": {
      "state": {
        "brand": "#5d78ff",
        "dark": "#282a3c",
        "light": "#ffffff",
        "primary": "#5867dd",
        "success": "#34bfa3",
        "info": "#36a3f7",
        "warning": "#ffb822",
        "danger": "#fd3995"
      },
      "base": {
        "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
        "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
      }
    }
  };
</script>
</body>
</html>
