@section('css')
@parent 
	<link href="{{asset('admin/assets/vendors/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
  
@endsection
@section('script')
@parent
	<script src="{{asset('admin/assets/vendors/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
	<script src="{{asset('admin/assets/js/demo1/pages/crud/datatables/basic/basic.js')}}" type="text/javascript"></script>
@endsection