@if(count($competition) > 0)
	@foreach($competition as $key=>$item)
		<div class="col-md-6 mb-4">
		  <div class="join-list btn_competition_show" data-id="{{$item->id}}" >
		    <div class="d-flex justify-content-between align-items-center">
		      <h4>{{$item->competition_name}}</h4>
		      <i class="fa fa-microphone"></i>
		    </div>
		    <table class="table">
		      <tr>
		        <td>
		          <i class="fa fa-list-ol"></i>
		          <span>{{$item->no_of_rounds}} Round</span>
		        </td>
		        <td>
		          <i class="fa fa-user"></i>
		          <span>{{$item->no_of_competitiors}} Participants</span>
		        </td>
		        <td>
		          <i class="fa fa-clock-o"></i>
		          <span>{{$item->voting_interval}} hours</span>
		        </td>
		      </tr>
		      <tr>
		        <td>
		          <i class="fa fa-user-plus"></i>
		          <span>Solo</span>
		        </td>
		        <td>
		          <i class="fa fa-user-times"></i>
		          <span>Elimination</span>
		        </td>
		        <td>
		          <i class="fa fa-clock-o"></i>
		          <span>{{$item->duration
		          }} days</span>
		        </td>
		      </tr>
		    </table>
		  </div>
		</div>
	@endforeach
@else
	<div class="col-12 my-5 text-center">
		<h1 class="coming-soon-title">
		  No Competition
		</h1>
	</div>
@endif
{!! $competition->links() !!}

 
 