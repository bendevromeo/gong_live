@extends('frontend.users.layout.dashboard-master')

@section('css')
    @parent
    <link rel="stylesheet" href="{{asset('assets/css/dropzone.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/video/mediaelementplayer.min.css')}}">
@endsection

@section('dash-content')
     <div class="competition-from">
        <nav class="form-steps-nav">
            <div class="nav nav-tabs" id="competitor-nav-tab" role="tablist">
                <a class="nav-item nav-link" href="{{route('user.dashboard')}}" >Competition</a>
                <!-- <a class="nav-item nav-link" href="{{route('user.dashboard')}}" >Competitions</a> -->
                 <a class="nav-item nav-link" href="{{route('user.dashboard')}}" >Social</a>
                 <!-- <a class="nav-item nav-link" href="{{route('user.dashboard')}}" >Competition Detail</a>  -->

                <!-- <a class="nav-item nav-link" id="nav-About-tab" data-toggle="tab" href="#nav-About" role="tab" aria-selected="false">About</a> -->
            </div>
        </nav>
          <div class="tab-content form-steps-content" id="competitor-nav-tabContent">
            <div class="tab-pane fade show active" id="nav-Social" role="tabpanel" aria-labelledby="nav-Social-tab">
              <div class="active-competition-videos">
                <div class="competition-title align-items-end mb-4 d-flex justify-content-between">
                  <h2>
                    Create Competition
                  </h2>
                </div>
                  <div class="alert alert-danger print-error-msg" style="display:none">
                      <ul></ul>
                  </div>
                  <div class="alert alert-success success_message" style="display:none">
                      <ul></ul>
                  </div>
                 <form action="{{route('user.competition.save')}}"  id="create_competition">
                    @csrf
                    <div class="row pb-3 border-bottom mb-4">
                      <div class="col-md-6">
                        <div class="input-field">
                          <label for="Name1" class="text-dark">Competition Name</label>
                          <div class="input-group mb-4">
                            <div class="input-pre-icon input-border-dark">
                              <img src="{{asset('/assets/images/cometition-icon.png')}}" alt="cometition-icon">
                            </div>
                            <input type="text" name="competition_name" class="form-control input-border-dark" id="competition_name">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-field">
                          <label for="Type" class="text-dark">Competition Type</label>
                          <div class="input-group mb-4">
                            <div class="input-pre-icon input-border-dark">
                              <img src="{{asset('/assets/images/elemeneate-icon.png')}}" alt="elemeneate-icon">
                            </div>
                            <select name="create_competition_type" id="create_competition_type" class="form-control text-dark input-border-dark">
                              <option value="elimination">Elimination</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <label for="Name" style="color: #940000 !important; font-size: 14px;font-weight: 500;">Choose a Category</label>

                        <div class="create_category-btn mb-4">

                            @if($data['user']['mediaTypes'])

                                @foreach($data['user']['mediaTypes'] as $key => $item)
                                    @if($key == 0)
                                        <input type="hidden" value="{{$item->id}}" name="create_category_id" class="create_category_id">
                                    @endif
                                    <button type="button" class="button-wrapper create_category-lead create_btn_media_type   {{ ($key == 0) ?  'active' : ''}}" data-value="{{$item->id}}" data-id="{{$item->id}}">
                                     <span class="label">
                                        {!! $item->avatar !!}
                                     </span>
                                        <label for="">{{$item->name}}</label>
                                    </button>
                                @endforeach
                            @endif
                        </div>
                      </div>

                      <div class="col-md-6">
                        <center class="create_loader_video ">
                            <div class="spinner-border text-warning" role="status">
                              <span class="sr-only">Loading...</span>
                            </div>
                        </center>
                          <div class="input-field">
                              <label for="Genre" style="color: #940000 !important;">Choose a Genre</label>
                              <div class="input-group mb-4">
                                  <div class="input-pre-icon input-border-dark">
                                      <img src="{{asset('/assets/images/genre-icon.png')}}" alt="genre-icon" class="input-icon">
                                  </div>
                                  <select class="form-control create_genre_id form-control text-dark input-border-dark" name="create_genre_id" id="create_genre_id">
                                      <option value="" selected="">Please choose genre</option>
                                      @if($data['user']['genres'])
                                          @foreach($data['user']['genres'] as $key => $item)
                                              <option value="{{$item->id}}">{{$item->name}}</option>
                                          @endforeach
                                      @endif
                                  </select>
                                  <em class="genre_id-error-section" style="display: none;">Please select genre</em>
                              </div>
                          </div>

                      </div>
                      <div class="col-md-6">
                        <div class="input-field">
                          <label for="Type" class="text-dark">Upload Type</label>
                          <input type="hidden" name="file_type" id="file_type" value="video">
                          <div class="file-upload mini-icon">
                            <div class="button-wrapper create_file-type active" data-val="video">
                               <span class="label" >
                                  <i class="fa fa-video-camera"></i>
                               </span>
                            </div>
                            <div class="button-wrapper create_file-type" data-val="audio">
                               <span class="label" >
                                  <i class="fa fa-music"></i>
                               </span>
                            </div>
                            <div class="button-wrapper create_file-type"data-val="image" >
                              <span class="label" >
                                <i class="fa fa-image"></i>
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row pb-3 border-bottom mb-4">
                      <div class="col-md-6">
                        <div class="input-field">
                          <label for="Round1" class="text-dark">Competitior Set</label>
                          <div class="input-group mb-4">
                            <div class="input-pre-icon input-border-dark">
                             <img src="{{asset('/assets/images/competitior-set-icon.png')}}" alt="competitior-set-icon">
                            </div>
                            <select name="competition_set" id="competition_set" class="form-control text-dark input-border-dark">
                              <option value="solo">Solo</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-field chuspic-2">
                          <label for="NumberRounds" class="text-dark">Number of Competitiors</label>
                          <div class="input-group mb-4">
                            <div class="input-pre-icon input-border-dark">
                              <img src="{{asset('/assets/images/round-icon.png')}}" alt="round-icon">
                            </div>

                            <select name="no_of_competitior" id="no_of_competitior" class="form-control text-dark input-border-dark">

                              <option selected="" value="">Please Choose Competitiors</option>
                              @foreach($data['user']['competitors'] as $key=>$value)
                                <option data-loop_val = "{{$value->no_of_rounds}}" value="{{$value->id}}">{{$value->number_of_competitiors}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-field">
                          <label for="NumberRounds" class="text-dark">Number of Rounds</label>
                          <div class="input-group mb-4">
                            <div class="input-pre-icon input-border-dark">
                              <img src="{{asset('/assets/images/round-icon.png')}}" alt="round-icon">
                            </div>
                            <input type="text" disabled="" class="form-control text-dark input-border-dark" name="no_of_round" id="no_of_round">
                            <!-- <select name="no_of_round" id="no_of_round" class="form-control text-dark input-border-dark">
                              <option selected="">Please choose a Rounds</option> -->
                              <!-- @for($i=1; $i<12; $i++)
                                <option value="{{$i}}">{{$i}} Rounds</option>
                              @endfor -->
                            <!-- </select> -->
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-field chuspic-2">
                          <label for="Round1" class="text-dark">Voting Time / Round</label>
                          <div class="input-group mb-4">
                            <div class="input-pre-icon input-border-dark">
                              <img src="{{asset('/assets/images/voting-icon.png')}}" alt="voting-icon">
                            </div>
                            <select name="voting_time" id="voting_time" class="form-control text-dark input-border-dark">
                              @for($i=1; $i<25; $i++)
                                <option value="{{$i}}">{{$i}} Hours</option>
                              @endfor
                            </select>
                          </div>
                        </div>
                      </div>


                      <div class="col-md-6">
                        <div class="input-field">
                          <label for="Duration" class="text-dark">Competition Duration</label>
                          <div class="input-group mb-4">
                            <div class="input-pre-icon input-border-dark">
                              <img src="{{asset('/assets/images/competion-during.png')}}" alt="competion-during">
                            </div>
                            <input readonly="" name="competition_duration" type="text" class="form-control input-border-dark" id="competition_duration">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-field">
                          <label for="Competitors" class="text-dark">Join Access</label>
                          <div class="d-flex mb-4">
                            <div class="checkbox checkbox-primary checkbox-inline mr-3">
                              <input type="checkbox" class="checkbox_a" name="access" id="aaa" value="private" checked="">
                              <label for="aaa"> <a href="javascript:;"> <strong>Private</strong> </a> </label>
                            </div>
                            <div class="checkbox checkbox-primary checkbox-inline">
                              <input type="checkbox" class="checkbox_a" name="access" id="bbb" value="public">
                              <label for="bbb"> <a href="javascript:;"> <strong> Public</strong> </a> </label>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-field chuspic-2">
                          <label for="Set" class="text-dark">Eligible Tier</label>
                          <div class="input-group mb-4">
                            <div class="input-pre-icon input-border-dark">
                              <img src="{{asset('/assets/images/competitior-set-icon.png')}}" alt="competitior-set-icon">
                            </div>

                            <select name="eligible_tier" id="eligible_tier" class="form-control text-dark input-border-dark">

                              @foreach($data['user']['tiers'] as $key => $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-field chuspic-2">
                          <label for="Competitors" class="text-dark">Country</label>
                          <div class="input-group mb-4">
                            <div class="input-pre-icon input-border-dark">
                              <img src="{{asset('/assets/images/competitior-number-icon.png')}}" alt="competitior-number-icon">
                            </div>
                            <select name="countries" id="countries" class="form-control text-dark input-border-dark">
                              <!-- <option value="" selected="">Please choose country</option> -->
                              @foreach($data['countries'] as $key => $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-field chuspic-2">
                           <center class="region_loader_video ">
                            <div class="spinner-border text-warning" role="status">
                              <span class="sr-only">Loading...</span>
                            </div>
                        </center>
                          <label for="Competitors" class="text-dark">Region</label>
                          <div class="input-group mb-4">
                            <div class="input-pre-icon input-border-dark">
                              <img src="{{asset('/assets/images/competitior-number-icon.png')}}" alt="competitior-number-icon">
                            </div>
                            <select name="region" id="region" class="form-control text-dark input-border-dark">

                            </select>
                          </div>
                        </div>
                      </div>


                    </div>

                    <div class="theme-form ">
                        <div class="container">
                            <div class="row ">
                                <div class="col-12 d-flex justify-content-center">
                                    <!-- <div class="mb-5">
                                        <div class="checkbox checkbox-primary checkbox-inline">
                                            <input type="checkbox" id="create_inlineCheckbox2" value="option1" checked="" name="create_inlineCheckbox2" class="privacy-check" required="">
                                            <label for="create_inlineCheckbox2"> I agree to the <a href=""> <strong>Privacy Policy</strong> </a> </label>
                                        </div>
                                        <div class="checkbox checkbox-primary checkbox-inline">
                                            <input type="checkbox" id="create_inlineCheckbox1" name="create_inlineCheckbox1" required="" value="option1" class="terms-check">
                                            <label for="create_inlineCheckbox1"> I agree to the <a href=""> <strong>Terms & Conditions</strong> </a> </label>

                                        </div>
                                    </div> -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 mb-5 text-center">
                                    <button  class="btn btn-primary rounded-pill" id="create_buttonProceed">Proceed</button>
                                </div>
                            </div>
                        </div>
                    </div>


                </form>

              </div>
               <div id="submit-for-competition-3" style="display: none;">
                    <div class="competition-title align-items-end mb-2 d-flex justify-content-between">
                        <h2>
                           Competition Created
                        </h2>
                    </div>
                    <p class="title-lead">
                        Your Competition sent for approval by the admin. Once approved, it shall be available.
                        <span class="private_competition"></span>
                    </p>
                </div>
            </div>
          </div>
      </div>

@endsection
@include('frontend.users.js-css-blades.select2')
@include('frontend.users.js-css-blades.form-validation')
@section('script')
    @parent
     <script type="text/javascript">
     $('.create_loader_video').hide();
     $('.region_loader_video').hide();
      $('.region_loader_video').show();
     axios({
            method: 'get',
            url: "{{route('get-states')}}?id="+$('#countries').val(),
            responseType: 'json'
          }).then(function (response) {
                var selected = '';
                var data = [];
                $.each(response.data.states,function(key,item){
                   selected += '<option selected="selected" value="'+item.id+'">'+item.name+'</option>';
                })
          $('.region_loader_video').hide();

                $('#region').html(selected);
            })
            .catch(function (error) {
            })
          .finally(function () {
        });
          //mohsin
      $(document).on('click', '.button-wrapper.create_category-lead', function (e) {
                $('.button-wrapper.create_category-lead').removeClass('active');
                $(this).addClass('active');
                // console.log(12);
                $('input[name="create_category_id"]').val($(this).attr('data-value'));
            });
      $(document).on('click', '.button-wrapper.create_file-type', function (e) {
                console.log($(this).data('val'));
                $('#file_type').val($(this).data('val'));
                $('.button-wrapper.create_file-type').removeClass('active');
                $(this).addClass('active');

            });
      $('.create_btn_media_type').click(function(){
                $('.create_loader_video').show();
                $('.create_category_id').val($(this).data('id'));
                var id = $(this).data('id');
                axios({
                    method: 'get',
                    url: "{{route('user.content.get-genres')}}?id="+id,
                    responseType: 'json'
                }).then(function (response) {
                        $('.create_loader_video').hide();
                        var selected = '';
                        var data = [];
                        $.each(response.data.genres,function(key,item){
                           selected += '<option  value="'+item.id+'">'+item.name+'</option>';
                        })
                        $('.create_genre_id').html(selected);
                }).catch(function (error) {
                }).finally(function () {
                });
            })
      $(document).ready(function(){
        $('#create_competition').validate({
                    rules: {
                        competition_name: {
                            required: true,
                        },
                        create_competition_type: {
                            required: true,
                        },
                        create_genre_id: {
                            required: true,
                        },
                        competition_set: {
                            required: true,
                            // accept:"image/png"
                        },
                        no_of_competitior: {
                            required: true,
                        },
                        no_of_round: {
                            required: true,
                        },
                        voting_time: {
                            required: true,
                        },
                        eligible_tier: {
                            required: true,
                        },
                        countries: {
                            required: true,
                        },
                        region : {
                          // required : true,
                        },
                        // create_inlineCheckbox2: {
                        //     required: true,
                        // },
                        // create_inlineCheckbox1: {
                        //     required: true,
                        // },

                    },
                    messages: {
                        // create_inlineCheckbox2: {
                        //     required: "Please provide name",
                        //     minlength: "Name must be minimum 5 characters long"
                        // },
                        // create_inlineCheckbox1: {
                        //     required: "Please provide username",
                        //     minlength: "Username must be minimum 5 characters long"
                        // },
                        // date_of_birth: "Please provide date of birth",
                        // profile_photo: {
                        //     required: "Please provide profile photo",
                        //     // accept: "Only images of type jpg,png and jpeg are allowed"
                        // },
                        // region: "Please provide region",
                        // country: "Please select country",
                        // gender: "Please select gender"
                    },
                    errorElement: "em",
                    errorPlacement: function ( error, element ) {
                        // Add the `help-block` class to the error element
                        error.addClass( "error-create-competition" );

                        // Add `has-feedback` class to the parent div.form-group
                        // in order to add icons to inputs
                        element.parents( ".col-sm-5" ).addClass( "has-feedback" );

                        if ( element.prop( "type" ) === "checkbox" ) {
                            error.insertAfter( element.parent( "label" ) );
                        } else {
                            error.insertAfter( element );
                        }

                        // Add the span element, if doesn't exists, and apply the icon classes to it.
                        if ( !element.next( "span" )[ 0 ] ) {
                            $( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
                        }
                    },
                    success: function ( label, element ) {
                        // Add the span element, if doesn't exists, and apply the icon classes to it.
                        if ( !$( element ).next( "span" )[ 0 ] ) {
                            $( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
                        }
                    },
                    function ( element, errorClass, validClass ) {
                        $( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
                        $( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
                    },
                    unhighlight: function ( element, errorClass, validClass ) {
                        $( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
                        $( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
                    },
                    submitHandler: function (form) {
                      // $(form).submit();
                        $('#create_buttonProceed').attr('disabled',true);
                        let url = $(form).attr('action');
                        let data = new FormData(document.getElementById('create_competition'));
                        $.ajax({
                            url: url,
                            data: data,
                            processData: false,
                            contentType: false,
                            type: 'POST',
                            dataType:'json',
                            success: function ( response ) {
                              $('#create_buttonProceed').attr('disabled',false);
                              if(response.status == 2){
                                printErrorMsg(response.error);
                              }
                              if(response.status == 1){
                                $('.private_competition').html('');
                                if(response.joining_code != ""){
                                  $('.private_competition').html('Your invitation Code is <b>'+response.joining_code+'</b>')
                                }
                                $('#submit-for-competition-3').show();

                                $('.active-competition-videos').hide();
                                // printSucessMsg('Competition Created Successfully');

                              }

                            },
                            error: function (errors) {
                              $('#create_buttonProceed').attr('disabled',false);
                                printErrorMsg(errors.error);
                            }
                        });
                    }
                });
        function printErrorMsg (msg) {

            $(".print-error-msg").find("ul").html('');

            $(".print-error-msg").css('display','block');

            $.each( msg, function( key, value ) {

                $(".print-error-msg").find("ul").append('<li>'+value+'</li>');

            });

        }
        function printSucessMsg (msg) {
            $(".success_message").find("ul").html('');
            $(".success_message").css('display','block');
            $(".success_message").find("ul").append('<li>'+msg+'</li>');

        }
      $('#countries').select2({
        // allowClear: true,
        //   placeholder: 'Select an Country',
        });
      $('#voting_time').select2({
        // allowClear: true,
        //   placeholder: 'Select an Country',
        });
      $('#no_of_competitior').select2({
        // allowClear: true,
        //   placeholder: 'Select an Country',
        });
      $('#eligible_tier').select2({
        // allowClear: true,
        //   placeholder: 'Select an Country',
        });
      $('#region').select2({
        // allowClear: true,
        //   placeholder: 'Select an Country',
        });
      $('#countries').change(function(){
    $('.region_loader_video').show();

        axios({
            method: 'get',
            url: "{{route('get-states')}}?id="+$(this).val(),
            responseType: 'json'
          }).then(function (response) {
                var selected = '';
                var data = [];
                $.each(response.data.states,function(key,item){
                   selected += '<option selected="selected" value="'+item.id+'">'+item.name+'</option>';
                })
          $('.region_loader_video').hide();

                $('#region').html(selected);
            })
            .catch(function (error) {
            })
          .finally(function () {
            // $('.add-instructor').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').prop('disabled', false);
        });
    });
      })
      $('#no_of_competitior').change(function(){
        var val = $(this).val();
        var loo = $(this).find(':selected').attr('data-loop_val');
        console.log(loo);
        // var ht =  `<option value="">Please Choose a rounds</option>`;
        //   for(var i = 1; i<= loo; i++){
        //     ht += `<option value="`+i+`">`+i+` Rounds</option>`;
        //   }

        $('#competition_duration').val(1);
        $('#no_of_round').val(loo);
        $('#competition_duration').val(loo);
      })
      $('#no_of_round').change(function(){
        var val = $(this).val();
         $('#competition_duration').val(val);
      })
      $('.checkbox_a').click(function(){
        $('.checkbox_a').prop("checked", false);
        $(this).prop("checked", true);
         // $('#competition_duration').val(val);
      })
      // function numberOfRound(a,b=0){ // 212
      //   b++;
      //   console.log(a);
      //   if(a < 11){
      //     var abc = a/4;
      //     if(abc == 1){
      //       a = 4;
      //     }
      //     checkRoundDivide(a,b);
      //     // if(t == 0){
      //       // for (var i = a - 1; i >= 1; i--) {
      //       //   if(i%4==0){
      //       //       t = checkRoundDivide(i,b);
      //       //       console.log(t);
      //       //       if(t==5 || t == 3 || t == 2 || t == 6 || t == 4 || t == 1){
      //       //          console.log(':)');
      //       //         return false;
      //       //       }else{
      //       //         numberOfRound(t,b);
      //       //       }
      //       //     }
      //       //     else if(i%2==0){
      //       //       t = checkRoundDivide(i,b);

      //       //       if(t==5 || t == 3 || t == 2 || t == 6 || t == 4 || t == 1){
      //       //         return false;
      //       //       }else{
      //       //         numberOfRound(t,b);
      //       //       }
      //       //     }
      //       //     else if(i%3==0){
      //       //       t = checkRoundDivide(i,b);
      //       //       if(t==5 || t == 3 || t == 2 || t == 6 || t == 4 || t == 1){
      //       //         return false;
      //       //       }else{
      //       //         numberOfRound(t,b);
      //       //       }
      //       //     }
      //       //     else if(i%5==0){
      //       //       t = checkRoundDivide(i,b);
      //       //       if(t==5 || t == 3 || t == 2 || t == 6 || t == 4 || t == 1){
      //       //         return false;
      //       //       }else{
      //       //         numberOfRound(t,b);
      //       //       }
      //       //     }
      //       // }
      //     // }
      //   }else{
      //     var d = a/4;

      //     if(isInt(d)){
      //       $('#no_of_round').append('<option>'+b+'</option>');
      //     }else{

      //         for (var i = a - 1; i >= 1; i--) {
      //           console.log(i);
      //          if(i%4==0){
      //             t = checkRoundDivide(i,b);
      //             console.log(t);
      //             if(t==5 || t == 3 || t == 2 || t == 6 || t == 4 || t ==1){
      //               console.log(':)');
      //               return false;
      //             }else{

      //               numberOfRound(t,b);
      //             }
      //           }
      //           else if(i%2==0){
      //             t = checkRoundDivide(i,b);
      //             if(t==5 || t == 3 || t == 2 || t == 6 || t == 4 || t == 1){
      //               return false;
      //             }else{
      //               numberOfRound(t,b);
      //             }
      //           }
      //           else if(i%3==0){
      //             t = checkRoundDivide(i,b);
      //             if(t==5 || t == 3 || t == 2 || t == 6 || t == 4 || t ==1){
      //               return false;
      //             }else{
      //               numberOfRound(t,b);
      //             }
      //           }
      //           else if(i%5==0){
      //             t = checkRoundDivide(i,b);
      //             if(t==5 || t == 3 || t == 2 || t == 6 || t == 4 || t==1){
      //               return false;
      //             }else{
      //               numberOfRound(t,b);
      //             }
      //           }
      //         }
      //     }
      //     if(d != 1){
      //       if(isInt(d)){
      //         numberOfRound(d,b);
      //       }else{
      //         numberOfRound(a,b);
      //       }
      //     }
      //   }
      // }
      // function isInt(n){
      //   return Number(n) === n && n % 1 === 0;
      // }
      // function isFloat(n){
      //   return Number(n) === n && n % 1 !== 0;
      // }
      // function checkRoundDivide(a,b){
      //   // var t = 0;
      //   if(checkRemainder(a,4)){
      //       $('#no_of_round').append('<option>'+b+'</option>');
      //   }
      //   else if(checkRemainder(a,2)){
      //     $('#no_of_round').append('<option>'+b+'</option>');
      //   }
      //   else if(checkRemainder(a,3)){
      //     $('#no_of_round').append('<option>'+b+'</option>');
      //   }
      //   else if(checkRemainder(a,5)){
      //     $('#no_of_round').append('<option>'+b+'</option>');
      //   }

      //     // return t;

      // }
      // function checkRemainder(a,b){
      //   if(a % b == 0){
      //     // console.log(23);
      //     return true;
      //   }else{
      //     return false;
      //   }
      // }
  </script>
@endsection
