@extends('frontend.users.layout.dashboard-master')

@section('css')
    @parent
    <link rel="stylesheet" href="{{asset('assets/css/dropzone.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/video/mediaelementplayer.min.css')}}">
@endsection

@section('dash-content')
    <div class="competition-from">
        <nav class="form-steps-nav">
            <div class="nav nav-tabs" id="competitor-nav-tab" role="tablist">
                <a class="nav-item nav-link nav_check_class active" onclick="getUserCompetition()"  id="nav-Competitions-tab"
                   data-toggle="tab" href="#nav-Competitions" role="tab" aria-selected="false">Competition</a>
                <!-- <a class="nav-item nav-link active" onclick="getUserCompetition()" id="nav-Competitions-tab" data-toggle="tab" href="#nav-Competitions" role="tab" aria-selected="false">Competition</a> -->

                <a class="nav-item nav-link nav_check_class " id="nav-Social-tab" data-toggle="tab" href="#nav-Social" role="tab" aria-selected="true" >Social</a>


                {{-- <a class="nav-item nav-link" id="nav-Shop-tab" data-toggle="tab" href="#nav-Shop" role="tab" aria-selected="false">Shop</a> --}}
                <!-- <a class="nav-item nav-link" id="nav-About-tab" data-toggle="tab" href="#nav-About" role="tab" aria-selected="false">About</a> -->
            </div>
        </nav>
        <div class="tab-content form-steps-content" id="competitor-nav-tabContent">
            <div class="tab-pane fade " id="nav-Social" role="tabpanel" aria-labelledby="nav-Social-tab">
                <div class="col-12 my-5 text-center">
                    <h1 class="coming-soon-title">
                      Coming Soon
                    </h1>
                  </div>
            </div>
            <div class="tab-pane fade show active competition_detail_show_hide" id="nav-Competitions" role="tabpanel" aria-labelledby="nav-Competitions-tab">
                <!-- <div class="col-12 my-5 text-center">
                    <h1 class="coming-soon-title">
                      Coming Soon
                    </h1>
                  </div> -->
                  <!-- When you remove the comment please remove script below last script tag i have add my name in starting of script --->
               @include('frontend.users.pages.dashboard-tabs.competition-detail')
            </div>
            <div class="competition_tab_submit_for" style="display: none">
                @include('frontend.users.pages.dashboard-tabs.submit-competition')
            </div>

            <div class="tab-pane fade" id="nav-Shop" role="tabpanel" aria-labelledby="nav-Shop-tab">Shop</div>
            <div class="tab-pane fade" id="nav-About" role="tabpanel" aria-labelledby="nav-About-tab">Shop</div>
        </div>
    </div>
    <div class="modal fade" id="join-list-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="max-width: 525px; width: 100%; margin: 10.75rem auto;" role="document">
        <div class="modal-content join-list modal-list">
          <div class="modal-body p-1">
            <div class="table-responsive">
              <table class="table ">
                <tr>
                  <td>
                    <p>Competition Name</p>
                    <label for="" class="competition_name">--</label>
                  </td>
                  <td>
                    <p>Upload Type</p>
                    <label for="" class="upload_type">--</label>
                  </td>
                  <td>
                    <p>Number of Competitors</p>
                    <label for="" class="number_of_competitiors">--</label>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p>Competition Type</p>
                    <label for="" class="competition_type">--</label>
                  </td>
                  <td>
                    <p>Number of Rounds</p>
                    <label for="" class="number_of_rounds">--</label>
                  </td>
                  <td>
                    <p>Competition Duration</p>
                    <label for="" class="competition_duration">--</label>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p>Genre</p>
                    <label for="" class="genre">--</label>
                  </td>
                  <td>
                    <p>Voting Time/Round</p>
                    <label for="" class="voting_time">--</label>
                  </td>
                  <td>
                    <p>Competitors Set</p>
                    <label for="" class="competitiors_set">--</label>
                  </td>
                </tr>
                 <tr>
                  <td>
                    <p>Region</p>
                    <label for="" class="region">--</label>
                  </td>
                  <td>
                    <p>Country</p>
                    <label for="" class="country">--</label>
                  </td>

                </tr>
              </table>
            </div>
          </div>
          <div class="error_join">

          </div>
          <div class="modal-footer justify-content-center border-0 p-0">
            <button type="button" class="btn btn-primary rounded-pill btn_join_competition ">Join</button>
            <button type="button" class="btn btn-secondary rounded-pill" data-dismiss="modal">cancel</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="join-list-modal-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="max-width: 525px; width: 100%; margin: 10.75rem auto;" role="document">
        <div class="modal-content join-list modal-list">
          <div class="modal-body p-1">
            <div class="table-responsive">
              <table class="table ">
                <tr>
                  <td>
                    <p>Competition Name</p>
                    <label for="" class="public_competition_name">--</label>
                  </td>
                  <td>
                    <p>Upload Type</p>
                    <label for="" class="public_upload_type">--</label>
                  </td>
                  <td>
                    <p>Number of Competitors</p>
                    <label for="" class="public_number_of_competitiors">--</label>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p>Competition Type</p>
                    <label for="" class="public_competition_type">--</label>
                  </td>
                  <td>
                    <p>Number of Rounds</p>
                    <label for="" class="public_number_of_rounds">--</label>
                  </td>
                  <td>
                    <p>Competition Duration</p>
                    <label for="" class="public_competition_duration">--</label>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p>Genre</p>
                    <label for="" class="public_genre">--</label>
                  </td>
                  <td>
                    <p>Voting Time/Round</p>
                    <label for="" class="public_voting_time">--</label>
                  </td>
                  <td>
                    <p>Competitors Set</p>
                    <label for="" class="public_competitiors_set">--</label>
                  </td>
                </tr>
                 <tr>
                  <td>
                    <p>Region</p>
                    <label for="" class="public_region">--</label>
                  </td>
                  <td>
                    <p>Country</p>
                    <label for="" class="public_country">--</label>
                  </td>

                </tr>
              </table>
            </div>
          </div>
           <div class="public_error_join">

          </div>
          <div class="modal-footer justify-content-center border-0 p-0">
            <button type="button" class="btn btn-primary rounded-pill btn_public_competition_join ">Join</button>
            <button type="button" class="btn btn-secondary rounded-pill" data-dismiss="modal">cancel</button>
          </div>
        </div>
      </div>
    </div>
    @if(empty(Auth::user()->userDetail))
    <!--    Sign Up Modal 2 Start-->
        <div class="modal fade" id="signUpModal2" tabindex="-1" role="dialog" aria-labelledby="signUpModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content theme-modal sign-up-modal" style="background: url({{asset('assets/images/modal-bg.png')}}) no-repeat center;background-size: cover">
                    <div class="modal-body">
                        {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"> --}}
                            {{-- <span aria-hidden="true">&times;</span> --}}
                        {{-- </button> --}}
                        <span class="modal-divider">
                &
              </span>
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-md-6 col-sm-6">
                                    <div class="already-account mb-0 mb-sm-4">
                                        {{-- <p>Already have an account?</p> --}}
                                        {{-- <a href="">Log In</a> --}}
                                    </div>

                                </div>
                                <div class="col-md-6 col-sm-6 text-center bg-red py-5">


                                    <form action="{{route('user.addUserDetail')}}" method="post" id="signUpForm2" enctype="multipart/form-data">
                                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}" id="user_id">
                                    <input type="hidden" name="gender" value="male" id="gender">
                                    <div class="upload-user-img">
                                        <img src="{{asset('assets/images/user-placeholder.png')}}" alt="" id="userImagePreview">
                                        <span>
                                              <i class="material-icons">
                                              photo_camera
                                            </i>
                                            </span>
                                        <input type="file" id="profile_photo" accept="image/*" name="profile_photo">
                                    </div>
                                    <div class="error_signup_2">
                                    </div>
                                    <!-- div class="input-field">
                                        <label for="Name">Name</label>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" name="name" id="name">
                                        </div> -->
                                        <input type="hidden" name="file_canvas" value="" id="file_canvas">
                                    <!-- </div> -->
                                    <div class="input-field">

                                        <label for="Name">First Name</label>

                                        <div class="input-group mb-3">

                                            <div class="input-pre-icon">

                                                <i class="fa fa-user"></i>

                                            </div>

                                            <input type="text" class="form-control" name="first_name" id="first_name">

                                        </div>

                                    </div>
                                    <div class="input-field">

                                        <label for="Name">Last Name</label>

                                        <div class="input-group mb-3">

                                            <div class="input-pre-icon">

                                                <i class="fa fa-user"></i>

                                            </div>

                                            <input type="text" class="form-control" name="last_name" id="last_name">

                                        </div>

                                    </div>
                                    <div class="input-field">
                                        <label for="Username">Username</label>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" name="username" id="username">
                                        </div>
                                    </div>
                                    <div class="input-field">
                                        <label for="Date of Birth">Date of Birth</label>
                                        <div class="input-group mb-3">
                                            <input type="date" class="form-control" name="date_of_birth" id="date_of_birth">
                                        </div>
                                    </div>

                                     <div class="user-gender mb-3">
                                        <label for=""> I'm a</label>
                                        <div class="mb-4 mb-sm-0">
                                            <button class="btn btn-primary rounded-pill mr-2 gender-selector" data-value="male">
                                                <img src="{{asset('assets/images/male-icon.png')}}" alt="male"> Male
                                            </button>
                                            <button class="btn btn-light rounded-pill gender-selector" data-value="female">
                                                <img src="{{asset('assets/images/female-icon.png')}}" alt="female"> Female
                                            </button>
                                        </div>
                                    </div>

                                    <div class="input-field">
                                            <label for="Country">Country</label>
                                            <div class="input-group mb-3">
                                            <select name="country_id" class="form-control white-icon country_sign_up"
                                                    id="Country">
                                                @if(@$countries)
                                                    @if($location == null)
                                                        @php
                                                            $location['phonecode'] = '';
                                                        @endphp
                                                    @endif()
                                                    @foreach($countries as $country)

                                                        <option value="{{$country->id}}"
                                                            {{($country->iso2 == $location['phonecode']) ? 'selected' : ''}}>
                                                            {{ucFirst($country->name)}}
                                                        </option>
                                                    @endforeach
                                                @endif

                                            </select>
                                            </div>
                                        </div>
                                    <div class="input-field">
                                            <label for="region">Region</label>
                                            <div class="input-group mb-3">
                                                <select name="region_id" class="form-control states_sign_up" id="region">
                                                </select>
                                            </div>
                                        </div>
                                    <button class="btn btn-primary rounded-pill mt-3 btn_signup_2">Complete Sign Up</button>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="signUpModal_3" tabindex="-1" role="dialog" aria-labelledby="signUpModal-3Label" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content theme-modal sign-up-modal" style="background: url({{asset('assets/images/modal-bg.png')}}) no-repeat center;background-size: cover">
                <div class="modal-body">
                 <!--  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button> -->
                  <!-- <span class="modal-divider">
                            OR
                          </span> -->
                  <div class="container">
                    <div class="row align-items-center">

                      <div class="col-md-6 col-sm-6">

                        <div class="already-account">

                          <p>Please provide further details to be a part of great community.</p>

                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 text-center bg-red py-5">
                        <!-- <form action="" method="post" id="signupForm"> -->
                          <h2 class="modal-title">Sign Up</h2>
                          <p class="title-description">
                            Hi, Which of the following best describes you?
                          </p>
                              <input type="hidden" name="role" class="user_role" value="talent">

                          <div class="sign-up-selection">
                            <div class="media active" onclick="changeIMG(this,'talent')">
                              <img class="on-active mr-2" src="../assets/images/Talent-form-icon.png" alt="Talent">
                              <img class="de-active mr-2" src="../assets/images/Talent-form-icon-1.png" alt="Talent">
                              <div class="media-body">
                                <h5 class="mt-0">Talent</h5>
                                <p>
                                  (I want to showcase my talents, find an agent, compete, build a fan base, and be able to collect tips and sell my content and materials.)
                                </p>
                              </div>
                            </div>
                            <div class="media" onclick="changeIMG(this,'fan')">
                              <img class="on-active mr-2" src="../assets/images/fan-form-icon.png" alt="fan">
                              <img class="de-active mr-2" src="../assets/images/fan-form-icon-1.png" alt="fan">
                              <div class="media-body">
                                <h5 class="mt-0">Fan</h5>
                                <p>
                                  (I want to watch to watch funny or incredible content, support my friends, and be entertained.)
                                </p>
                              </div>
                            </div>
                            <div class="media" onclick="changeIMG(this,'agent')">
                              <img class="on-active mr-2" src="../assets/images/agent-form-icon.png" alt="agent">
                              <img class="de-active mr-2" src="../assets/images/agent-form-icon-1.png" alt="agent">
                              <div class="media-body">
                                <h5 class="mt-0">Agent</h5>
                                <p>
                                  (I want to find fresh talent and rising stars and promote my existing clients.)
                                </p>
                              </div>
                            </div>
                            <div class="media" onclick="changeIMG(this,'sponser')">
                              <img class="on-active mr-2" src="../assets/images/Sponsor-form-icon.png" alt="Sponsor">
                              <img class="de-active mr-2" src="../assets/images/Sponsor-form-icon-1.png" alt="Sponsor">
                              <div class="media-body">
                                <h5 class="mt-0">Sponsor</h5>
                                <p>
                                  (I want to promote my business, create special private competitions for talent and connect with everyone.)
                                </p>
                              </div>
                            </div>
                          </div>

                          <button class="btn btn-primary rounded-pill mt-3 btn_signup_role_3">Sign Up</button>
                        <!-- </form> -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>

      <div class="modal fade" id="verification-phone-1" tabindex="-1" role="dialog" aria-labelledby="verification-phone-1Label" aria-hidden="true">
          <div class="modal-dialog verify-wrapper" role="document">
              <div class="modal-content dialog">
                  <div class="">
                      <div class="error_sc2_ss"></div>
                      <!-- <button type="" class="close" data-dismiss="modal" aria-label="Close" >×</button> -->
                      <h3>Please enter the 4-digit verification code we sent via SMS:</h3>
                      <span>We want to make sure its you</span>
                      <div id="form">
                          <input type="text" name="code_name_1" class="code_name_1" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
                                          <input type="text" name="code_name_2" class="code_name_2" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" /><input type="text" name="code_name_3" class="code_name_3" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" /><input type="text" name="code_name_4" class="code_name_4" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
                          <button class="btn btn-primary btn-embossed btn_code_v_ddd">Verify</button>
                      </div>

                      <div>
                          Didn't receive the code?<br />
                          <a href="javascript:;" class="code_again">Send code again</a><br />
                          <a href="javascript:;" id="chang_ph_no">Change phone number</a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
       <div class="modal fade" id="verification-phone-2" tabindex="-1" role="dialog" aria-labelledby="verification-phone-2Label" aria-hidden="true">
        <div class="modal-dialog verify-wrapper" role="document">
            <div class="modal-content dialog">
              <div class="aaffs_dfdf"></div>
                <div class="">
                    <!-- <button class="close" data-dismiss="modal">×</button> -->
                    <h3>Change Phone Number</h3>
                    <div id="form2" class="text-center chuspic">
                        <input class="form-control mb-3 phone_no_a telephone tel_new_number_p" name="phone_no_a" type="tel" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
                        <button class="btn btn-primary btn-embossed btn_phone_chan_ddd mt-4">Confirm</button>
                    </div>

                </div>
            </div>
        </div>
    </div>


         <!--    Sign Up Modal Start-->
          <div class="modal fade" id="croppingModal" tabindex="-1" role="dialog" aria-labelledby="modalExpample-5Label" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content sign-up-modal"  style="background: #940000;">
                <div class="modal-body">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="text-white">&times;</span>
                  </button>
                  <div class="container">
                    <div class="row align-items-center">
                      <div class="col-12 pt-5 text-center">
                        <form action="" style="max-width: 720px;">
                          <div class="text-center mb-5">
                            <div class="upload-user-img preview rounded-circle">

                            </div>
                          </div>
                          <div >
                            <img  style="width: 500px;height: 500px" id="image" src="../assets/images/upload-placeholder.png" alt="">
                          </div>

                          <div class="mt-3 mb-3">
                            <!-- <button type="button" class="btn btn-primary">
                              Back
                            </button> -->
                            <button type="button" class="btn btn-primary save_crop">
                              Save
                            </button>
                            <!-- <button type="button" class="btn btn-primary">
                              Next
                            </button> -->
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!--    Sign Up Modal End-->

    @endif

    <!--    Sign Up Modal 2 End-->
@endsection
@include('frontend.users.js-css-blades.select2')
@include('frontend.users.js-css-blades.form-validation')
@if(!Auth::user()->hasRole([1,3,4,5]))
    @include('frontend.users.js-css-blades.sweetalert')

@endif
@if(Auth::user()->phone_status == 'unverified')
@endif
 @if(empty(Auth::user()->userDetail))
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css" integrity="sha256-jKV9n9bkk/CTP8zbtEtnKaKf+ehRovOYeKoyfthwbC8=" crossorigin="anonymous" />

        <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js" integrity="sha256-CgvH7sz3tHhkiVKh05kSUgG97YtzYNnWt6OXcmYzqHY=" crossorigin="anonymous"></script>
@endif
@section('script')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.2.15/mediaelement-and-player.min.js"></script>
    <script type="text/javascript" src="{{asset('assets/js/dropzone.js')}}"></script>
    <script>

        const MIN_MEDIA_DURATION = 1; // 15 seconds
        const MAX_MEDIA_DURATION = 4*60; // 4 minutes
        $( document ).ready(function() {
            // loadPage();
            getUserCompetition();
            var country_sign = $('.country_sign_up').val();
                    axios({
                        method: 'get',
                        url: "{{route('get-states')}}?id="+country_sign,
                        responseType: 'json'
                      }).then(function (response) {
                            var selected = '';
                            var data = [];
                            $.each(response.data.states,function(key,item){
                               selected += '<option selected="selected" value="'+item.id+'">'+item.name+'</option>';
                            })
                            $('.states_sign_up').html(selected).trigger('change');
                        })
                        .catch(function (error) {
                        })
                      .finally(function () {
                        // $('.add-instructor').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').prop('disabled', false);
                    });
            $('.loader_video').hide();
            $('.my_progress').hide();
            $('.loader_progess').hide();
            @if(!Auth::user()->hasRole([1,3,4,5]))
                $('#signUpModal_3').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                $("#signUpModal_3").on('show.bs.modal', function(){
                    // $('#signUpModal2').find('.help-block').html('');
                    $("#signUpModal").modal('hide');
                    $('#signUpForm2')[0].reset();
                });
                //  $('.btn_signup_role_3').click(function(e){

                // e.preventDefault();
                // swal({
                //   title: "Are you sure that you want to continue as a "+$('.user_role').val(),
                //   // text: "Once deleted, you will not be able to recover this imaginary file!",
                //   icon: "warning",
                //   buttons: true,
                //   dangerMode: true,
                // })
                // .then((willDelete) => {
                //   if (willDelete) {
                //     $(this).html(loader());
                //     axios({
                //         method: 'get',
                //         url: "{{route('user.role')}}?role="+$('.user_role').val()+"&user_id="+$('#user_id').val(),
                //         responseType: 'json'
                //     }).then(function (response) {
                //         $('.btn_signup_role_3').html('SignUp');
                //         if(response.data.status == 1){
                //             $("#signUpModal2").modal({
                //                 backdrop: 'static',
                //                 keyboard: false
                //             });
                //             var country_val = $('.country_sign_up').val();
                //                 axios({
                //                     method: 'get',
                //                     url: "{{route('get-states')}}?id="+country_val,
                //                     responseType: 'json'
                //                   }).then(function (response) {
                //                         var selected = '';
                //                         var data = [];
                //                         $.each(response.data.states,function(key,item){
                //                            selected += '<option selected="selected" value="'+item.id+'">'+item.name+'</option>';
                //                         })
                //                         $('.states_sign_up').html(selected).trigger('change');
                //                     })
                //                     .catch(function (error) {
                //                     })
                //                   .finally(function () {
                //                     // $('.add-instructor').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').prop('disabled', false);
                //                 });
                //             $("#signUpModal_3").modal("hide");
                //         }
                //     })
                //     .catch(function (error) {
                //         $('.btn_signup_role_3').html('SignUp');
                //     })
                //     .finally(function () {
                //         $('.btn_signup_role_3').html('SignUp');
                //     });
                //   } else {
                //     $('.btn_signup_role_3').html('SignUp');
                //   }
                // });

                // });

                $('.btn_signup_role_3').click(function(e){

                e.preventDefault();
                swal({
                  title: "Are you sure that you want to continue as a "+$('.user_role').val(),
                  // text: "Once deleted, you will not be able to recover this imaginary file!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {
                    $(this).html(loader());
                    axios({
                        method: 'get',
                        url: "{{route('user.role')}}?role="+$('.user_role').val()+"&user_id="+$('#user_id').val(),
                        responseType: 'json'
                    }).then(function (response) {
                        $('.btn_signup_role_3').html('SignUp');
                        if(response.data.status == 1){
                            $("#verification-phone-1").modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                            var country_val = $('.country_sign_up').val();
                                axios({
                                    method: 'get',
                                    url: "{{route('get-states')}}?id="+country_val,
                                    responseType: 'json'
                                  }).then(function (response) {
                                        var selected = '';
                                        var data = [];
                                        $.each(response.data.states,function(key,item){
                                           selected += '<option selected="selected" value="'+item.id+'">'+item.name+'</option>';
                                        })
                                        $('.states_sign_up').html(selected).trigger('change');
                                    })
                                    .catch(function (error) {
                                    })
                                  .finally(function () {
                                    // $('.add-instructor').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').prop('disabled', false);
                                });
                            $("#signUpModal_3").modal("hide");
                        }
                    })
                    .catch(function (error) {
                        $('.btn_signup_role_3').html('SignUp');
                    })
                    .finally(function () {
                        $('.btn_signup_role_3').html('SignUp');
                    });
                  } else {
                    $('.btn_signup_role_3').html('SignUp');
                  }
                });

            });

            $('.code_again').click(function(){
               $(this).html(loader());
                $.ajax({
                    url: "{{route('user.phone.send-verify')}}",
                    type: 'post',
                    data : {'id':$('#user_id').val()},
                    dataType:'json',
                    success: function ( response ) {
                       if(response.status == 1){
                        $('.error_sc2_ss').html('<ul><li class="alert alert-success">Code Send in your phone number</li></ul>');
                        setTimeout(function(){ $(".error_sc2_ss").html(''); }, 3000);
                         $('.code_again').html('Send code again');
                       }
                    },
                    error: function (errors) {
                        $('.error_sc2_ss').html('<ul><li class="alert alert-danger">'+errors.responseJSON.message+'</li></ul>')

                        setTimeout(function(){ $(".error_sc2_ss").html(''); }, 3000);
                        $('.code_again').html('Send code again');
                        // $('.send_code_again').html('Verify');
                    }
                });
            });

            $('.btn_phone_chan_ddd').click(function(){
                if($('.tel_new_number_p').val() ==  ""){
                    $('.err_em').removeClass('hide-display');
                    return false;
                }
                var n = $('#verification-phone-2 .iti__selected-dial-code').html();
                n+= $('.tel_new_number_p').val();
                $('.btn_phone_chan_ddd').html(loader());
                $.ajax({
                    url: "{{route('user.phone.change_number')}}",
                    type: 'post',
                    data : {'number' : n,'id':$('#user_id').val()},
                    dataType:'json',
                    success: function ( response ) {
                       if(response.status == 1){
                        $("#verification-phone-1").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                         $("#verification-phone-2").modal('hide');
                        $('.btn_phone_chan_ddd').html('Confirm');
                       }
                       if(response.status == 2){
                                var ht = '<ul>'
                                $.each(response.errors, function (key, item){
                                  ht += "<li class='alert alert-danger'>"+item+"</li>";
                                });
                                setTimeout(function(){ $(".aaffs_dfdf").html(''); }, 3000);

                                $(".aaffs_dfdf").html(ht);
                                $('.btn_phone_chan_ddd').html('Confirm');
                       }
                    },
                    error: function (errors) {

                        $('.btn_phone_chan_ddd').html('Confirm');
                    }
                });
            });
            $('#chang_ph_no').click(function(){
                $("#verification-phone-2").modal({
                    backdrop: 'static',
                    keyboard: false
                });
                 $("#verification-phone-1").modal('hide');
            });
            $('.btn_code_v_ddd').click(function(){
                $(this).html(loader());
                var a = $('.code_name_1').val() + $('.code_name_2').val() + $('.code_name_3').val() + $('.code_name_4').val();
                $.ajax({
                    url: "{{route('user.phone.verify')}}",
                    type: 'post',
                    data:{'code':a,'id':$("#user_id").val()},
                    dataType:'json',
                    success: function ( response ) {
                       if(response.status == 1){
                        $("#signUpModal2").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $("#verification-phone-1").modal('hide');
                        $("#verification-phone-2").modal('hide');
                       }
                       if(response.status == 0){
                        $('.error_sc2_ss').html('<ul><li class="alert alert-danger">Your code is Invalid</li></ul>');
                        setTimeout(function(){ $(".error_sc2_ss").html(''); }, 3000);
                        $('.btn_code_v_ddd').html('Verify');
                       }
                    },
                    error: function (errors) {
                        $('.error_sc2_ss').html('<ul><li class="alert alert-danger">'+errors.responseJSON.message+'</li></ul>')

                        setTimeout(function(){ $(".error_sc2_ss").html(''); }, 3000);
                        $('.btn_code_v_ddd').html('Verify');
                    }
                });
            })

                $(document).on('click', '.gender-selector', function (e) {
                    let selectedValue = $(this).attr('data-value');
                    $('.gender-selector').removeClass('btn-primary').addClass('btn-light');
                    $(this).addClass('btn-primary');
                    $('#gender').val(selectedValue);
                });
                document.querySelector('#profile_photo').addEventListener('change', function (e) {
                    let imagePreview = document.getElementById('userImagePreview');
                    if (this.value === ""){
                        imagePreview.src = "{{asset('assets/images/user-placeholder.png')}}";
                    }else{
                        imagePreview.src = window.URL.createObjectURL(this.files[0]);
                    }
                });
                $('.country_sign_up').select2({
                  placeholder: 'Select an Country',
                  dropdownParent: $("#signUpModal2")
                });
                var states_select = $('.states_sign_up').select2({
                  placeholder: 'Select an Region',
                  dropdownParent: $("#signUpModal2")
                });

                $('.country_sign_up').change(function(){
                    axios({
                        method: 'get',
                        url: "{{route('get-states')}}?id="+$(this).val(),
                        responseType: 'json'
                      }).then(function (response) {
                            var selected = '';
                            var data = [];
                            $.each(response.data.states,function(key,item){
                               selected += '<option selected="selected" value="'+item.id+'">'+item.name+'</option>';
                            })
                            $('.states_sign_up').html(selected).trigger('change');
                        })
                        .catch(function (error) {
                        })
                      .finally(function () {
                        // $('.add-instructor').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').prop('disabled', false);
                    });
                });
                $('#signUpForm2').validate({
                    rules: {
                        first_name: {
                            required: true,
                            // minlength: 5
                        },
                        last_name: {
                            required: true,
                            // minlength: 5
                        },
                        username: {
                            required: true,
                            minlength: 5
                        },
                        date_of_birth: {
                            required: true,
                        },
                        profile_photo: {
                            required: true,
                            // accept:"image/png"
                        },
                        region: {
                            required: true,
                        },
                        country: {
                            required: true,
                        },
                    },
                    messages: {
                        first_name: {
                            required: "Please provide first name",
                            // minlength: "Name must be minimum 5 characters long"
                        },
                        last_name: {
                            required: "Please provide last name",
                            // minlength: "Name must be minimum 5 characters long"
                        },
                        username: {
                            required: "Please provide username",
                            minlength: "Username must be minimum 5 characters long"
                        },
                        date_of_birth: "Please provide date of birth",
                        profile_photo: {
                            required: "Please provide profile photo",
                            // accept: "Only images of type jpg,png and jpeg are allowed"
                        },
                        region: "Please provide region",
                        country: "Please select country",
                        gender: "Please select gender"
                    },
                    errorElement: "em",
                    errorPlacement: function ( error, element ) {
                        // Add the `help-block` class to the error element
                        error.addClass( "help-block" );

                        // Add `has-feedback` class to the parent div.form-group
                        // in order to add icons to inputs
                        element.parents( ".col-sm-5" ).addClass( "has-feedback" );

                        if ( element.prop( "type" ) === "checkbox" ) {
                            error.insertAfter( element.parent( "label" ) );
                        } else {
                            error.insertAfter( element );
                        }

                        // Add the span element, if doesn't exists, and apply the icon classes to it.
                        if ( !element.next( "span" )[ 0 ] ) {
                            $( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
                        }
                    },
                    success: function ( label, element ) {
                        // Add the span element, if doesn't exists, and apply the icon classes to it.
                        if ( !$( element ).next( "span" )[ 0 ] ) {
                            $( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
                        }
                    },
                    function ( element, errorClass, validClass ) {
                        $( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
                        $( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
                    },
                    unhighlight: function ( element, errorClass, validClass ) {
                        $( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
                        $( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
                    },
                    submitHandler: function (form) {
                        $('.btn_signup_2').html(loader());
                        $('.btn_signup_2').attr('disabled',true);
                        let url = $(form).attr('action');
                        let data = new FormData(document.getElementById('signUpForm2'));
                        $.ajax({
                            url: url,
                            data: data,
                            processData: false,
                            contentType: false,
                            type: 'POST',
                            dataType:'json',
                            success: function ( response ) {
                                $('.btn_signup_2').html('Complete Sign Up');
                                $('.btn_signup_2').attr('disabled',false);
                                console.log(response);
                                if (response.status == 1){
                                    location.reload();
                                }
                                if(response.status == 0){
                                    console.log(response);
                                    var ht = '<ul>';
                                    ht += "<li class='alert alert-danger'>"+response.message+"</li>";
                                    ht += '</ul>'
                                    $(".error_signup_2").html(ht);
                                    setTimeout(function(){ $(".error_signup_2").html(''); }, 3000);
                                }
                            },
                            error: function (errors) {
                                console.log(errors);
                                var ht = '<ul>'
                                $.each(errors.responseJSON.errors, function (key, item){
                                  ht += "<li class='alert alert-danger'>"+item+"</li>";
                                });
                                ht += '</ul>'
                                $(".error_signup_2").html(ht);
                                $('.btn_signup_2').html('Complete Sign Up');
                                $('.btn_signup_2').attr('disabled',false);
                            }
                        });
                    }
                });

                var $modal = $('#croppingModal');

                var image = document.getElementById('image');

                var cropper;



                $(document).on("change", "#profile_photo", function(e){

                    var files = e.target.files;
                    // console.log(files);
                    var done = function (url) {

                      image.src = url;
                      $modal.modal({
                            backdrop: 'static',
                            keyboard: false
                        });

                    };
                    var reader;

                    var file;

                    var url;


                    if (files && files.length > 0) {

                      file = files[0];


                      if (URL) {

                        done(URL.createObjectURL(file));

                      } else if (FileReader) {

                        reader = new FileReader();

                        reader.onload = function (e) {

                          done(reader.result);

                        };

                        reader.readAsDataURL(file);

                      }

                    }

                });

                $modal.on('shown.bs.modal', function () {
                    console.log(image);
                    cropper = new Cropper(image, {
                      aspectRatio: 1,

                      viewMode: 0,
                      autoCrop: false,
                      preview: '.preview',
                      zoom: function(e) {
                        // console.log(e.type, e.detail.ratio);
                        // if (e.target.id === 'ZoomInBtn') {
                        //   cropper.zoom(0.1);
                        // }
                        // if (e.target.id === 'ZoomOutBtn') {
                          // cropper.zoom(-0.1);
                        // }
                      }

                    });

                }).on('hidden.bs.modal', function () {
                   cropper.destroy();
                   cropper = null;
                });
                $('.save_crop').click(function(){
                    $modal.modal('hide');
                    canvas = cropper.getCroppedCanvas({
                        maxWidth: 4096,
                        maxHeight: 4096,
                        aspectRatio : 'free'
                      });
                    $('#userImagePreview').attr('src',canvas.toDataURL());
                    $('#file_canvas').val(canvas.toDataURL());
                })

            @elseif(Auth::user()->phone_status == 'unverified')

                            $("#verification-phone-1").modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                            var country_val = $('.country_sign_up').val();
                                axios({
                                    method: 'get',
                                    url: "{{route('get-states')}}?id="+country_val,
                                    responseType: 'json'
                                  }).then(function (response) {
                                        var selected = '';
                                        var data = [];
                                        $.each(response.data.states,function(key,item){
                                           selected += '<option selected="selected" value="'+item.id+'">'+item.name+'</option>';
                                        })
                                        $('.states_sign_up').html(selected).trigger('change');
                                    })
                                    .catch(function (error) {
                                    })
                                  .finally(function () {
                                    // $('.add-instructor').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').prop('disabled', false);
                                });
                            $("#signUpModal_3").modal("hide");
            $('.code_again').click(function(){
               $(this).html(loader());
                $.ajax({
                    url: "{{route('user.phone.send-verify')}}",
                    type: 'post',
                    data : {'id':$('#user_id').val()},
                    dataType:'json',
                    success: function ( response ) {
                       if(response.status == 1){
                        $('.error_sc2_ss').html('<ul><li class="alert alert-success">Code Send in your phone number</li></ul>');
                        setTimeout(function(){ $(".error_sc2_ss").html(''); }, 3000);
                         $('.code_again').html('Send code again');
                       }
                    },
                    error: function (errors) {
                        $('.error_sc2_ss').html('<ul><li class="alert alert-danger">'+errors.responseJSON.message+'</li></ul>')

                        setTimeout(function(){ $(".error_sc2_ss").html(''); }, 3000);
                        $('.code_again').html('Send code again');
                        // $('.send_code_again').html('Verify');
                    }
                });
            });

            $('.btn_phone_chan_ddd').click(function(){
                if($('.tel_new_number_p').val() ==  ""){
                    $('.err_em').removeClass('hide-display');
                    return false;
                }
                var n = $('#verification-phone-2 .iti__selected-dial-code').html();
                n+= $('.tel_new_number_p').val();
                $('.btn_phone_chan_ddd').html(loader());
                $.ajax({
                    url: "{{route('user.phone.change_number')}}",
                    type: 'post',
                    data : {'number' : n,'id':$('#user_id').val()},
                    dataType:'json',
                    success: function ( response ) {
                       if(response.status == 1){
                        $("#verification-phone-1").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                         $("#verification-phone-2").modal('hide');
                        $('.btn_phone_chan_ddd').html('Confirm');
                       }
                       if(response.status == 2){
                                var ht = '<ul>'
                                $.each(response.errors, function (key, item){
                                  ht += "<li class='alert alert-danger'>"+item+"</li>";
                                });
                                setTimeout(function(){ $(".aaffs_dfdf").html(''); }, 3000);

                                $(".aaffs_dfdf").html(ht);
                                $('.btn_phone_chan_ddd').html('Confirm');
                       }
                    },
                    error: function (errors) {

                        $('.btn_phone_chan_ddd').html('Confirm');
                    }
                });
            });
            $('#chang_ph_no').click(function(){
                $("#verification-phone-2").modal({
                    backdrop: 'static',
                    keyboard: false
                });
                 $("#verification-phone-1").modal('hide');
            });
            $('.btn_code_v_ddd').click(function(){
                $(this).html(loader());
                var a = $('.code_name_1').val() + $('.code_name_2').val() + $('.code_name_3').val() + $('.code_name_4').val();
                $.ajax({
                    url: "{{route('user.phone.verify')}}",
                    type: 'post',
                    data:{'code':a,'id':$("#user_id").val()},
                    dataType:'json',
                    success: function ( response ) {
                       if(response.status == 1){
                        $("#signUpModal2").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $("#verification-phone-1").modal('hide');
                        $("#verification-phone-2").modal('hide');
                       }
                       if(response.status == 0){
                        $('.error_sc2_ss').html('<ul><li class="alert alert-danger">Your code is Invalid</li></ul>');
                        setTimeout(function(){ $(".error_sc2_ss").html(''); }, 3000);
                        $('.btn_code_v_ddd').html('Verify');
                       }
                    },
                    error: function (errors) {
                        $('.error_sc2_ss').html('<ul><li class="alert alert-danger">'+errors.responseJSON.message+'</li></ul>')

                        setTimeout(function(){ $(".error_sc2_ss").html(''); }, 3000);
                        $('.btn_code_v_ddd').html('Verify');
                    }
                });
            })
            @elseif(empty(Auth::user()->userDetail))
                $('#signUpModal2').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $(document).on('click', '.gender-selector', function (e) {
                    let selectedValue = $(this).attr('data-value');
                    $('.gender-selector').removeClass('btn-primary').addClass('btn-light');
                    $(this).addClass('btn-primary');
                    $('#gender').val(selectedValue);
                });
                document.querySelector('#profile_photo').addEventListener('change', function (e) {
                    let imagePreview = document.getElementById('userImagePreview');
                    if (this.value === ""){
                        imagePreview.src = "{{asset('assets/images/user-placeholder.png')}}";
                    }else{
                        imagePreview.src = window.URL.createObjectURL(this.files[0]);
                    }
                });
                $('.country_sign_up').select2({
                  placeholder: 'Select an Country',
                  dropdownParent: $("#signUpModal2")
                });
                var states_select = $('.states_sign_up').select2({
                  placeholder: 'Select an Region',
                  dropdownParent: $("#signUpModal2")
                });
                $('.country_sign_up').change(function(){
                    axios({
                        method: 'get',
                        url: "{{route('get-states')}}?id="+$(this).val(),
                        responseType: 'json'
                      }).then(function (response) {
                            var selected = '';
                            var data = [];
                            $.each(response.data.states,function(key,item){
                               selected += '<option selected="selected" value="'+item.id+'">'+item.name+'</option>';
                            })
                            $('.states_sign_up').html(selected).trigger('change');
                        })
                        .catch(function (error) {
                        })
                      .finally(function () {
                        // $('.add-instructor').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').prop('disabled', false);
                    });
                });
                $('#signUpForm2').validate({
                    rules: {
                        first_name: {
                            required: true,
                            // minlength: 5
                        },
                        last_name: {
                            required: true,
                            // minlength: 5
                        },
                        username: {
                            required: true,
                            minlength: 5
                        },
                        date_of_birth: {
                            required: true,
                        },
                        profile_photo: {
                            required: true,
                            // accept:"image/png"
                        },
                        region: {
                            required: true,
                        },
                        country: {
                            required: true,
                        },
                    },
                    messages: {
                        first_name: {
                            required: "Please provide first name",
                            // minlength: "Name must be minimum 5 characters long"
                        },
                        last_name: {
                            required: "Please provide last name",
                            // minlength: "Name must be minimum 5 characters long"
                        },
                        username: {
                            required: "Please provide username",
                            minlength: "Username must be minimum 5 characters long"
                        },
                        date_of_birth: "Please provide date of birth",
                        profile_photo: {
                            required: "Please provide profile photo",
                            // accept: "Only images of type jpg,png and jpeg are allowed"
                        },
                        region: "Please provide region",
                        country: "Please select country",
                        gender: "Please select gender"
                    },
                    errorElement: "em",
                    errorPlacement: function ( error, element ) {
                        // Add the `help-block` class to the error element
                        error.addClass( "help-block" );

                        // Add `has-feedback` class to the parent div.form-group
                        // in order to add icons to inputs
                        element.parents( ".col-sm-5" ).addClass( "has-feedback" );

                        if ( element.prop( "type" ) === "checkbox" ) {
                            error.insertAfter( element.parent( "label" ) );
                        } else {
                            error.insertAfter( element );
                        }

                        // Add the span element, if doesn't exists, and apply the icon classes to it.
                        if ( !element.next( "span" )[ 0 ] ) {
                            $( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
                        }
                    },
                    success: function ( label, element ) {
                        // Add the span element, if doesn't exists, and apply the icon classes to it.
                        if ( !$( element ).next( "span" )[ 0 ] ) {
                            $( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
                        }
                    },
                    function ( element, errorClass, validClass ) {
                        $( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
                        $( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
                    },
                    unhighlight: function ( element, errorClass, validClass ) {
                        $( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
                        $( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
                    },
                    submitHandler: function (form) {
                        $('.btn_signup_2').html(loader());
                        $('.btn_signup_2').attr('disabled',true);
                        let url = $(form).attr('action');
                        let data = new FormData(document.getElementById('signUpForm2'));
                        $.ajax({
                            url: url,
                            data: data,
                            processData: false,
                            contentType: false,
                            type: 'POST',
                            dataType:'json',
                            success: function ( response ) {
                                $('.btn_signup_2').html('Complete Sign Up');
                                $('.btn_signup_2').attr('disabled',false);
                                if (response.status == 1){
                                    location.reload();
                                }
                                if(response.status == 0){
                                    console.log(response);
                                    var ht = '<ul>';
                                    ht += "<li class='alert alert-danger'>"+response.message+"</li>";
                                    ht += '</ul>'
                                    $(".error_signup_2").html(ht);
                                     setTimeout(function(){ $(".error_signup_2").html(''); }, 3000);
                                }
                            },
                            error: function (errors) {
                                console.log(errors);
                                var ht = '<ul>'
                                $.each(errors.responseJSON.errors, function (key, item){
                                  ht += "<li class='alert alert-danger'>"+item+"</li>";
                                });
                                ht += '</ul>'
                                $(".error_signup_2").html(ht);
                                $('.btn_signup_2').html('Complete Sign Up');
                                $('.btn_signup_2').attr('disabled',false);
                            }
                        });
                    }
                });

                 var $modal = $('#croppingModal');

                var image = document.getElementById('image');

                var cropper;



                $(document).on("change", "#profile_photo", function(e){

                    var files = e.target.files;
                    // console.log(files);
                    var done = function (url) {

                      image.src = url;
                      $modal.modal({
                            backdrop: 'static',
                            keyboard: false
                        });

                    };
                    var reader;

                    var file;

                    var url;


                    if (files && files.length > 0) {

                      file = files[0];


                      if (URL) {

                        done(URL.createObjectURL(file));

                      } else if (FileReader) {

                        reader = new FileReader();

                        reader.onload = function (e) {

                          done(reader.result);

                        };

                        reader.readAsDataURL(file);

                      }

                    }

                });

                $modal.on('shown.bs.modal', function () {

                    cropper = new Cropper(image, {

                      aspectRatio: 1,

                      viewMode: 0,
                      autoCrop: false,
                       // dragCrop: true,

                      preview: '.preview'

                    });

                }).on('hidden.bs.modal', function () {
                   cropper.destroy();
                   cropper = null;
                });
                $('.save_crop').click(function(){
                    $modal.modal('hide');
                    canvas = cropper.getCroppedCanvas({
                        maxWidth: 4096,
                        maxHeight: 4096,
                      });
                    $('#userImagePreview').attr('src',canvas.toDataURL());
                    $('#file_canvas').val(canvas.toDataURL());
                })

            @endif

            $(document).on('click', '.button-wrapper.category-lead', function (e) {
                $('.button-wrapper.category-lead').removeClass('active');
                $(this).addClass('active');

                $('input[name="category"]').val($(this).attr('data-valuee'));
                $('[name="file"]').val('');
            });
            $(document).on('click', '.button-wrapper.file-type', function (e) {
                // alert(1);
                 $('.audio-preview-container').css('width',0)
            $('.audio-preview-container').css('height',0)
                $('.button-wrapper.file-type').removeClass('active');
                $(this).addClass('active');
                document.querySelector(".audio-preview").src = "";
                $(".audio-preview").hide();
                $(".audio-preview").removeAttr('src');

                document.querySelector(".video-preview").src = "";
                $(".video-preview").hide();
                $(".video-preview").removeAttr('src');

                document.querySelector(".img-preview").src   = "";
                $(".img-preview").hide();
                $(".img-preview").removeAttr('src');

                $('.placeholder_media').show();
                $('.placeholder_media').attr('src',"{{url('/')}}/assets/images/upload-placeholder.png");
                $('.placeholder_media').css('width',525);
                $('.placeholder_media').css('height',);
                if ($(this).attr('data-value') !== $('input[name="file_type"]').val() ){
                    $('input[name="file"]').val('').attr('accept', $(this).attr('data-accept'));
                    $('.file-preview-container').hide();
                }
                $('input[name="file_type"]').val($(this).attr('data-value'));
            });

            $('.btn_media_type').click(function(){
                $('.loader_video').show();
                $('.category_id').val($(this).data('id'));
                var id = $(this).data('id');
                $('.category_selecred').val($(this).find('label').html());
                axios({
                    method: 'get',
                    url: "{{route('user.content.get-genres')}}?id="+id,
                    responseType: 'json'
                }).then(function (response) {
                        $('.loader_video').hide();
                        var selected = '';
                        var data = [];
                        $.each(response.data.genres,function(key,item){
                           selected += '<option  value="'+item.id+'">'+item.name+'</option>';
                        })
                        $('.genre_id').html(selected);
                }).catch(function (error) {
                }).finally(function () {
                });
            })
        });
        @if(request()->is_login == 1 || request()->is_compete == 1)
            // $('.btn_submit_competition_sidebar')[0].click();
            $('.nav_check_class').removeClass('active');
            $("#nav-Social").hide();
            $("#nav-Competitions").hide();
            $('.competition_tab_submit_for').show();
        @endif
        $(document).on('change', 'input[name="file"]', function (e) {
            let fileType = $('input[name="file_type"]').val();
            let fileUrl = URL.createObjectURL(this.files[0]);
            $('.file-error-section').hide();
            $('.audio-preview-container').css('width',0)
            $('.audio-preview-container').css('height',0)
            // console.log(fileUrl);
            if (fileType !== "image"){
                if(fileType === "video"){

                    let videoTag = document.querySelector(".video-preview");
                    console.log(fileUrl);

                    videoTag.src = fileUrl;
                    console.log(videoTag);
                            // $('.video-preview-container').show();

                    $(".video-preview").show();
                    videoTag.onloadedmetadata = function() {
                        let videoDuration = videoTag.duration;
                        console.log(videoDuration);
                        if (videoDuration < MIN_MEDIA_DURATION || videoDuration > MAX_MEDIA_DURATION){
                            videoTag.src = '';
                            console.log(1122);
                            $('input[name="file"]').val('');
                            $('.file-preview-container').hide();
                            // alert('video length is not correct');
                            $('.error_msg').html('Video length is not correct');
                            $('.error_msg').show();
                            setTimeout(function(){ $('.error_msg').hide(); }, 3000);

                        }else{
                            console.log(1230);
                            $('.video-preview-container').show();
                            $('.file-error-section').hide();
                            document.querySelector(".video-preview").addEventListener('timeupdate', function() {
                            var _VIDEO = document.querySelector(".video-preview"),
                                _CANVAS = document.querySelector("#canvas-element"),
                                _CANVAS_CTX = _CANVAS.getContext("2d");

                            // Placing the current frame image of the video in the canvas
                            _CANVAS_CTX.drawImage(_VIDEO, 2, 2, _VIDEO.videoWidth, _VIDEO.videoHeight);
                            $('.img_thum').val(_CANVAS.toDataURL());

                            // Setting parameters of the download link
                            // document.querySelector("#download-link").setAttribute('href', _CANVAS.toDataURL());
                            // document.querySelector("#download-link").setAttribute('download', 'thumbnail.png');
                        });
                        }
                    };
                }else if (fileType === "audio"){
                    $(".audio-preview").show();
                    let audioTag = document.querySelector(".audio-preview");
                    audioTag.src = fileUrl;
                    audioTag.onloadedmetadata = function() {
                        let audioDuration = audioTag.duration;
                        // console.log(audioDuration);
                        if (audioDuration < MIN_MEDIA_DURATION || audioDuration > MAX_MEDIA_DURATION){
                            audioTag.src = '';
                            $('input[name="file"]').val('');
                            $('.file-preview-container').hide();
                            // alert('audio length is not correct');
                            $('.error_msg').html('Audio length is not correct');
                            $('.error_msg').show();
                             setTimeout(function(){ $('.error_msg').hide(); }, 3000);
                        }else{
                            $('.placeholder_media').attr('src',"{{url('/')}}/assets/images/backgroundimagebrowse.png");
                            // $('.audio-preview-img').attr('src',"{{url('/')}}/assets/images/backgroundimagebrowse.png");
                            $('.placeholder_media').css('width',525);
                            $('.placeholder_media').css('height',);
                            $('.audio-preview-container').css('width','100%');
                            $('.audio-preview-container').css('height','');
                            $('.audio-preview-container').show();
                        }
                    }
                }
            }else{
                $('.img-preview').show();
                $('.img-preview').attr('src',fileUrl);
                $('.file-preview-container').show();
            }

        });

        $(document).on('click', '#submitPublicCompetition', function (e) {

            if (isContentFormValidated()){
                $('#submitPublicCompetition').html(loader())
                $('#submitPublicCompetition').attr('disabled','true');
                $('.my_progress').show();
                let data = new FormData(document.getElementById('contentForm'));
                $.ajax({
                    url: '{{route('user.content.add')}}',
                    data: data,
                    processData: false,
                    contentType: false,
                    type: 'POST',

                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                if(percentComplete == 1){
                                    $('.loader_progess').show();
                                    $('#submitPublicCompetition').hide();
                                }
                                percentComplete = parseInt(percentComplete * 100);
                                $('.myprogress').text(percentComplete + '%');
                                $('.myprogress').css('width', percentComplete + '%');
                            }
                        }, false);
                        return xhr;
                    },
                    success: function ( response ) {
                        if (JSON.parse(response).status){
                            $('#submit-for-competition-2').hide();
                            $('#submit-for-competition-3').show();
                            $('.file_type_show').html($('input[name="file_type"]').val());
                            $('.file_sub_com').html($('.category_selecred').val());
                            document.getElementById('contentForm').reset();
                            // setTimeout(function() {
                            //     window.location.href = "{{route('user.addUserDetail')}}";
                            // }, 2000);

                        }
                    },
                    error: function (errors) {
                        console.log(errors);
                    }
                });
            }
        });

        $(document).on('click', '#privateCompetition', function (e) {

            $.ajax({
                    url: "{{route('user.competition.get-public-competition')}}?category_id="+$('.category_id').val()+"&genre_id="+$('[name="genre_id"]').val()+"&type="+$('input[name="file_type"]').val(),
                    type: 'get',
                    dataType:'json',
                    success: function ( response ) {
                       if(response.status == 1){
                        $('.public_competition').html(response.html);
                       }
                    },
                    error: function (errors) {

                    }
                });


            $('.private-competition-1').show();
            $('#submit-for-competition-2').hide();
            $('#submit-for-competition-3').hide();
        })
        $(document).on('click', '.btn_private_code', function (e) {
            $('.btn_private_code').attr('disabled',true);
            $('.error_code_private').html('');
            $(this).html(loader());
            $.ajax({
                    url: "{{route('user.competition.verify_code')}}?code="+$('#code').val()+"&type="+$('input[name="file_type"]').val()+"&category_id="+$('.category_id').val()+"&genre_id="+$('[name="genre_id"]').val(),

                    processData: false,
                    contentType: false,
                    type: 'get',
                    dataType:'json',
                    success: function ( response ) {
                        $('.btn_private_code').html('Submit');
                        // $('.btn_signup_2').html('Complete Sign Up');
                        $('.btn_private_code').attr('disabled',false);
                        if (response.status == 1){
                            $('.competition_name').html(response.competition.competition_name);
                            $('.upload_type').html(response.competition.competition_media);
                            $('.number_of_competitiors').html(response.competition.no_of_competitiors+" Participants");
                            $('.competition_type').html(response.competition.competition_end_type);
                            $('.number_of_rounds').html(response.competition.no_of_rounds+" Rounds");
                            $('.competition_duration').html(response.competition.duration+" Days");
                            $('.genre').html(response.competition.genre.name);
                            $('.voting_time').html(response.competition.voting_interval+" Days");
                            $('.competitiors_set').html(response.competition.competition_set);
                            $('.region').html(response.competition.region.name);
                            $('.country').html(response.competition.country.name);
                            $('.btn_join_competition').data('type',$('input[name="file_type"]').val())
                            $('#join-list-modal').modal('show');
                        }
                        if (response.status == 0){
                            var ht = '<ul>'
                            ht += "<li class='alert alert-danger'>"+response.message+"</li>";
                            ht += '</ul>'
                            $('.error_code_private').html(ht);
                        }
                        if(response.status == 2){
                            var ht = '<ul>'
                            $.each(response.error, function (key, item){
                              ht += "<li class='alert alert-danger'>"+item+"</li>";
                            });
                            ht += '</ul>'
                            $('.error_code_private').html(ht);
                        }

                    },
                    error: function (errors) {
                        console.log(errors);
                        var ht = '<ul>'
                        $.each(errors.responseJSON.errors, function (key, item){
                          ht += "<li class='alert alert-danger'>"+item+"</li>";
                        });
                        ht += '</ul>'
                        $('.btn_private_code').attr('disabled',false);
                    }
                });
        })

        $(document).on('click', '.btn_join_competition', function (e) {
            $('.error_join').html('');
            $('.btn_join_competition').attr('disabled',true);
            $(this).html(loader());
             let data = new FormData(document.getElementById('contentForm'));
            $.ajax({
                    url: "{{route('user.competition.join_private_competition')}}?code="+$('#code').val()+"&type="+$(this).data('type'),
                    processData: false,
                    contentType: false,
                    type: 'post',
                    data:data,
                    dataType:'json',
                    success: function ( response ) {
                        $('.btn_join_competition').html('Join');
                        // $('.btn_signup_2').html('Complete Sign Up');
                        $('.btn_join_competition').attr('disabled',false);
                        if (response.status == 1){
                             $('.file_type_show').html($('input[name="file_type"]').val());
                            $(".private-competition-1").hide();
                            $('.competition_type_a').html('private competition');

                            $('#submit-for-competition-4').show();
                             $('#join-list-modal').modal('hide');
                        }
                        if (response.status == 0){
                            var ht = '<ul>'
                            ht += "<li class='alert alert-danger'>Your Uploaded media type is not correct.</li>";
                            ht += '</ul>'
                            $('.error_join').html(ht);
                        }
                        if(response.status == 2){
                            var ht = '<ul>'
                            $.each(response.error, function (key, item){
                              ht += "<li class='alert alert-danger'>"+item+"</li>";
                            });
                            ht += '</ul>'
                            $('.error_code_private').html(ht);
                        }

                    },
                    error: function (errors) {
                        console.log(errors);
                        var ht = '<ul>'
                        $.each(errors.responseJSON.errors, function (key, item){
                          ht += "<li class='alert alert-danger'>"+item+"</li>";
                        });
                        ht += '</ul>'
                        $('.btn_join_competition').attr('disabled',false);
                    }
                });
        });


        $(document).on('click', '#buttonProceed', function (e) {
            e.preventDefault();
            var file_type = $('input[name="file_type"]').val();
            var _validFileExtensions;
            if(file_type == 'image'){
                _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"];
            }else if(file_type == 'audio'){
                _validFileExtensions = [".mp3", ".wav", ".aiff", ".aac", ".ogg", ".wma"];
            }else if(file_type == 'video'){
                _validFileExtensions = ['.webm',".avi", ".mov", ".mpg", ".m4v", ".wma", ".3gp", ".mp4"];
            }
            var a = ValidateFile(_validFileExtensions);
            if(a == 0){
                return false;
            }
            // return false;

            $('video').trigger('pause');
            $('audio').trigger('pause');
            if (isContentFormValidated()){
                $('#submit-for-competition-1').addClass('d-none');
                $('#submit-for-competition-2').removeClass('d-none');
                // $('#submitPublicCompetition').trigger('click');
            }
        });

        /**
         * content form validation
         * @returns {boolean}
         */
        function isContentFormValidated() {
            let isDataValid = true;
            if ($('[name="file"]').val() === ""){
                isDataValid = false;
                $('.file-error-section').show();
            }
            if ($('[name="name"]').val() === ""){
                isDataValid = false;
                $('.name-error-section').show();
            }
            if ($('[name="genre_id"]').val() === ""){
                isDataValid = false;
                $('.genre_id-error-section').show();
            }
            if(!$('.privacy-check').is(':checked')){
                isDataValid = false;
                $('.privacy-check').siblings('em').show();
            }
            if(!$('.terms-check').is(':checked')){
                isDataValid = false;
                $('.terms-check').siblings('em').show();
            }
            return isDataValid;
        }

        $(document).on('change', '#Name, #Genre', function (e) {
            // alert(2);
            if ($(this).val() !== ""){
                $("." + $(this).attr('name') + "-error-section").hide();
            }
        })
        $(document).on('click', '.terms-check, .privacy-check', function (e) {
            if($(this).is(':checked')){
                $(this).siblings('em').hide();
            }
        })


        function abc(){
            let fileType = $('input[name="file_type"]').val();
            let fileUrl = URL.createObjectURL(this.files[0]);
            // console.log(fileUrl);
            if (fileType !== "image"){
                if(fileType === "video"){
                    let videoTag = document.querySelector(".video-preview");
                    videoTag.src = fileUrl;
                    $(".video-preview").show();
                    videoTag.onloadedmetadata = function() {
                        let videoDuration = videoTag.duration;
                        if (videoDuration < MIN_MEDIA_DURATION || videoDuration > MAX_MEDIA_DURATION){
                            videoTag.src = '';
                            $('input[name="file"]').val('');
                            $('.file-preview-container').hide();
                            alert('video length is not correct');


                        }else{

                            $('.video-preview-container').show();
                            $('.file-error-section').hide();
                            document.querySelector(".video-preview").addEventListener('timeupdate', function() {
                            var _VIDEO = document.querySelector(".video-preview"),
                                _CANVAS = document.querySelector("#canvas-element"),
                                _CANVAS_CTX = _CANVAS.getContext("2d");

                            // Placing the current frame image of the video in the canvas
                            _CANVAS_CTX.drawImage(_VIDEO, 2, 2, _VIDEO.videoWidth, _VIDEO.videoHeight);
                            $('.img_thum').val(_CANVAS.toDataURL());

                            // Setting parameters of the download link
                            // document.querySelector("#download-link").setAttribute('href', _CANVAS.toDataURL());
                            // document.querySelector("#download-link").setAttribute('download', 'thumbnail.png');
                        });
                        }
                    };
                }else if (fileType === "audio"){
                    $(".audio-preview").show();
                    document.querySelector(".audio-preview").src = fileUrl;
                    $('.placeholder_media').attr('src',"{{url('/')}}/assets/images/audio-pre.jpg");
                    $('.placeholder_media').css('width',525);
                    $('.placeholder_media').css('height',);
                    $('.audio-preview-container').show();
                }
            }else{
                $('.img-preview').show();
                $('.img-preview').attr('src',fileUrl);
                $('.file-preview-container').show();
            }
        }
        function changeIMG(e,a){
            $('.media').removeClass('active');
            $('.user_role').val(a);
            $(e).addClass('active');
        }
        function loadPage(){
            location.reload();
        }
        $(document).ready(function(){

             $(document).on('click', '.pagination a', function(event){
              event.preventDefault();
              var page = $(this).attr('href').split('page=')[1];
              fetch_data(page);
             });

            function fetch_data(page){
              $.ajax({
               url:"{{route('user.competition.get-public-competition')}}?page="+page+"&category_id="+$('.category_id').val()+"&genre_id="+$('[name="genre_id"]').val()+"&type="+$('input[name="file_type"]').val(),
               success:function(response)
               {
                if(response.status == 1){
                    $('.public_competition').html(response.html);
                }

               }
              });
            }

            $(document).on('click', '.btn_competition_show', function(event){
                $.ajax({
                    url: "{{route('user.competition.get-public-competition-detail')}}?id="+$(this).data('id'),

                    processData: false,
                    contentType: false,
                    type: 'get',
                    dataType:'json',
                    success: function ( response ) {
                        if (response.status == 1){
                            $('.public_competition_name').html(response.competition.competition_name);
                            $('.public_upload_type').html(response.competition.competition_media);
                            $('.public_number_of_competitiors').html(response.competition.no_of_competitiors+" Participants");
                            $('.public_competition_type').html(response.competition.competition_end_type);
                            $('.public_number_of_rounds').html(response.competition.no_of_rounds+" Rounds");
                            $('.public_competition_duration').html(response.competition.duration+" Days");
                            $('.public_genre').html(response.competition.genre.name);
                            $('.public_voting_time').html(response.competition.voting_interval+" Days");
                            $('.public_competitiors_set').html(response.competition.competition_set);
                            $('.public_region').html(response.competition.region.name);
                            $('.public_country').html(response.competition.country.name);
                            $('.btn_public_competition_join').data('val',response.competition.id);
                            $('#join-list-modal-2').modal('show');
                        }
                        if (response.status == 0){
                            var ht = '<ul>'
                            ht += "<li class='alert alert-danger'>"+response.message+"</li>";
                            ht += '</ul>'
                            // $('.error_code_private').html(ht);
                        }

                    },
                    error: function (errors) {
                    }
                });
            })
            $('.btn_public_competition_join').click(function(){
                // $('.error_join').html('');
                $('.btn_public_competition_join').attr('disabled',true);
                $(this).html(loader());
                 let data = new FormData(document.getElementById('contentForm'));
                $.ajax({
                        url: "{{route('user.competition.join_public_competition')}}?id="+$(this).data('val')+"&type="+$(this).data('type'),
                        processData: false,
                        contentType: false,
                        type: 'post',
                        data:data,
                        dataType:'json',
                        success: function ( response ) {
                            $('.btn_public_competition_join').html('Join');
                            // $('.btn_signup_2').html('Complete Sign Up');
                            $('.btn_public_competition_join').attr('disabled',false);
                            if (response.status == 1){
                                $('.file_type_show').html($('input[name="file_type"]').val());
                                $(".private-competition-1").hide();
                                $('#submit-for-competition-4').show();
                                $('.competition_type_a').html('public competition');
                                $('#join-list-modal-2').modal('hide');
                            }
                            if (response.status == 0){
                                var ht = '<ul>'
                                ht += "<li class='alert alert-danger'>Your Uploaded media type is not correct.</li>";
                                ht += '</ul>'
                                $('.public_error_join').html(ht);
                            }
                            if(response.status == 2){
                                var ht = '<ul>'
                                $.each(response.error, function (key, item){
                                  ht += "<li class='alert alert-danger'>"+item+"</li>";
                                });
                                ht += '</ul>'
                                $('.error_code_private').html(ht);
                            }

                        },
                        error: function (errors) {
                            console.log(errors);
                            var ht = '<ul>'
                            $.each(errors.responseJSON.errors, function (key, item){
                              ht += "<li class='alert alert-danger'>"+item+"</li>";
                            });
                            ht += '</ul>'
                            $('.btn_public_competition_join').attr('disabled',false);
                        }
                    });
            })

        });

    function ValidateFile(_validFileExtensions) {
        // var arrInputs = oForm.getElementsByTagName("input");
        // for (var i = 0; i < arrInputs.length; i++) {
            // var oInput = arrInputs[i];
            var oInput = document.getElementById("file");

            if (oInput.type == "file") {
                var sFileName = oInput.value;
                if (sFileName.length > 0) {
                    var blnValid = false;
                    for (var j = 0; j < _validFileExtensions.length; j++) {
                        var sCurExtension = _validFileExtensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            break;
                        }
                    }

                    if (!blnValid) {
                        $('.error_msg').html("Your upload file is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                        $('.error_msg').show();
                        setTimeout(function(){ $('.error_msg').hide(); }, 5000);
                        return 0;
                    }
                }
            }
        // }

        return 1;
    }
    function refreshMedia(){
       var elem = document.getElementById('file');
       if(elem && document.createEvent) {
          var evt = document.createEvent("MouseEvents");
          evt.initEvent("click", true, false);
          elem.dispatchEvent(evt);
       }
    }
    </script>
   <!--  <script type="text/javascript">
        // Mohsin Baig
        var wavesurfer = [];
        function getUserCompetition(){
            $('.avc').html('');
            $('video').trigger('pause');
            $('audio').trigger('pause');
                $('.active_competition_loader').show();
            $('.inactive_competition_loader').show();
            $('.onhold_competition_loader').show();
            $('.active_no_competiton').addClass('hide-display');
            $('.inactive_no_competiton').addClass('hide-display');
            $('.onhold_no_competiton').addClass('hide-display');
         axios({
                method: 'get',
                url: "{{route('user.content.get-user-competition')}}",
                responseType: 'json'
            }).then(function (response) {
                $('.active_competition_loader').hide();
              $('.inactive_competition_loader').hide();
                $('.onhold_competition_loader').hide();
                    if(response.data.active_competiton_status == 1){
                        var active_html = '';
                        $.each(response.data.active_competiton,function(key,item){
                           if(item.competition_media == 'video'){
                            if(item.single_competition_detail){
                                active_html += videoHtml(item);
                            }
                            // $('.active_img_detail_page').append(active_html);
                           }
                           if(item.competition_media == 'audio'){
                            if(item.single_competition_detail){
                                active_html += audioHtml(item,key);
                                // wavesurfer[key] = renderWaveForm('{{config("constant.BUCKET_URL")}}'+item.single_competition_detail.user_content.file, '#waveform_'+key);
                            }
                            // $('.active_img_detail_page').append(active_html);
                           }
                           if(item.competition_media == 'image'){
                            if(item.single_competition_detail){
                                active_html += imgHtml(item);
                            }
                            // $('.active_img_detail_page').append(active_html);
                           }
                        });
                    }else{
                        $('.active_no_competiton').removeClass('hide-display');
                    }
                    $('.active_img_detail_page').html(active_html);
                    if(response.data.inactive_competiton_status == 1){
                        var inactive_html = '';
                        $.each(response.data.inactive_competiton,function(key,item){
                           if(item.competition_media == 'video'){
                          if(item.single_competition_detail){
                            inactive_html += videoHtml(item);
                         }
                           }
                           if(item.competition_media == 'audio'){
                          if(item.single_competition_detail){
                            inactive_html += audioHtml(item,key);
                           }
                           }
                           if(item.competition_media == 'image'){
                          if(item.single_competition_detail){
                            inactive_html += imgHtml(item);
                           }
                           }
                        });
                    }else{
                        $('.inactive_no_competiton').removeClass('hide-display');
                    }
                if(response.data.hold_competition_status == 1){
                  var hold_html = '';
                  $.each(response.data.hold_competiton,function(key,item){
                         if(item.competition_media == 'video'){
                          if(item.single_competition_detail){
                          hold_html += videoHtml(item);
                         }
                         }
                         if(item.competition_media == 'audio'){
                          if(item.single_competition_detail){
                            hold_html += audioHtml(item,key);
                           }
                         }
                         if(item.competition_media == 'image'){
                          if(item.single_competition_detail){
                            hold_html += imgHtml(item);
                           }
                         }
                      });
                    $('.onhold_img_detail_page').html(hold_html);
                }else{
                  console.log(123);
                  $('.onhold_no_competiton').removeClass('hide-display');
                }

                  $('.inactive_img_detail_page').html(inactive_html);

                    $('.video_class_detail').mediaelementplayer({
                        // Do not forget to put a final slash (/)
                        pluginPath: 'https://cdnjs.com/libraries/mediaelement/',
                        // this will allow the CDN to use Flash without restrictions
                        // (by default, this is set as `sameDomain`)
                        shimScriptAccess: 'always'
                        // more configuration
                    });

            }).catch(function (error) {
            }).finally(function () {
            });
        }
        function videoHtml(item){
            var a = `<div class="col-md-4" style="float:left">
                                <div class="post-video-grid border-0 mb-0 pb-0">
                    <div class="video-slider">
                      <div class="video-posted mb-0 pb-0 border-bottom-0">
                                <div class="video-card">
                                <a href="{{route('voting')}}?q=`+item.single_competition_detail.competition_id+`" >
                                    <div class="player">
                                      <p class="event-name"></p>
                                      <p class="event-category"></p>
                                      <video id="player1" class="video_class_detail" width="640" height="360" preload="none" style="width: 100%; max-width: 100%; height: 280px" controls poster="{{config('constant.BUCKET_URL')}}`+item.single_competition_detail.user_content.thumbnail+`" playsinline webkit-playsinline>
                                         <source src="{{config('constant.BUCKET_URL')}}`+item.single_competition_detail.user_content.file+`" type="video/mp4">
                                      </video>
                                    </div>
                                    </a>
                                        </div></div>
                                </div></div></div>`;
            return a;
        }
        function imgHtml(item){
            var a = `<div class="col-md-4" style="float:left">

                        <div class="post-img-grid border-0 pb-0 mb-0">
                                  <div class="img-posted pb-0 border-0">
                                    <div class="item dfbkdjsf">
                                      <a href="{{route('voting')}}?q=`+item.single_competition_detail.competition_id+`" >
                                      <div class="img-card-parent">
                                        <div class="abc"></div>
                                         <p class="event-name"></p>
                                                  <p class="event-category"></p>
                                        <div class="img-card">
                                                <img src="{{config('constant.BUCKET_URL')}}`+item.single_competition_detail.user_content.file+`" class="img-responsive " />
                                        </div>
                                      </div>
                                      </a>
                                    </div>
                                  </div>
                                </div></div>`;
            return a;
        }
        function audioHtml(item,key){
            var a = `<div class="col-md-4" style="float:left"><div class="post-audio-grid border-0 pb-0 mb-0">
                      <div class="audio-posted pb-0 border-0">
                          <div class="audio-owl-slider  owl-theme ">
                            <a href="{{route('voting')}}?q=`+item.single_competition_detail.competition_id+`" >
                              <div id="waveform_`+key+`" class="audio-waveform wave" style="height:200px">
                                  <p class="event-name"></p>
                                  <p class="event-category"></p>

                                  <img src="https://storage.googleapis.com/gong_bucket/uploads/image/1585216412audio.PNG" class="img-responsive img_`+key+`" style="position: absolute;overflow: hidden; margin-top: 54px;margin-left: 19px;" />
                              </div>
                              </a>
                              <div class="">

                              </div>
                          </div>
                      </div>
                  </div></div>`;
            return a;
        }

        function renderWaveForm(url, parentSelector) {
          var domEl = document.createElement('div')
          document.querySelector(parentSelector).appendChild(domEl)

          var wavesurferr = WaveSurfer.create({
            container: domEl,
            waveColor: 'red',
            progressColor: 'purple',
            hideScrollbar: true,
            xhr: {
            cache: "default",
            mode: "cors",
            method: "GET",
            // credentials: "gong_bucket",
            headers: [
              { key: "cache-control", value: "no-cache" },
              { key: "Access-Control-Allow-Origin", value: "*" },
              { key: "Origin", value: "http://localhost:8000" },
              { key: "Origin", value: "*" },
              { key: "pragma", value: "no-cache" }
            ]
          }
          });
          wavesurferr.load(url);
          return wavesurferr;
        }
    </script> -->
@endsection
