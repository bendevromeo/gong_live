@extends('layouts.master')

@section('content')
<div class="content vocal-compition-page mt-5 mb-5 pb-5">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <form method="post" action="{{route('user.content.judge')}}">
            @csrf

          <div class="post-video-grid">
            <div class="row">
              <div class="col-12">
                <div class="competition-title align-items-sm-start align-item-start mb-4 d-flex justify-content-sm-between flex-sm-row flex-column">
                  <h2 class="mb-4 mb-sm-0">
                    {{$competition->tier->name}}
                  </h2>
                  <div>
                    <button type="button" class="btn btn-secondary mini-btn mt-sm-2 mt-0 mb-sm-0 mb-2">{{$competition->competitionUserDetail->tier->name}} </button>
                    <!-- <button type="button" class="btn btn-danger mini-btn mt-sm-2 mt-0 mb-sm-0 mb-2">SOPRANO </button> -->
                    @if(Auth::check())
                      @php $arr = $competition->activeCompetitionDetail->pluck('user_id')->toArray(); @endphp
{{--                      @if(!in_array(Auth::user()->id, $arr))--}}
                        <button type="button" class="btn btn-primary mini-btn mt-sm-2 mt-0 mb-sm-0 mb-2 btn_submit">
                            Submit
                          </button>
{{--                      @endif--}}
                    @else
                      <button type="button" class="btn btn-primary mini-btn mt-sm-2 mt-0 mb-sm-0 mb-2 btn_submit">
                        Submit
                      </button>
                    @endif

                  </div>
                </div>
              </div>
            </div>
              <!-- <div class="row"> -->
                <div class="col-md-12 alert alert-danger error" style="@if(Session::get('error'))@else display: none @endif" role="alert">
                  {{Session::get('error')}}
                </div>
                <div class="col-md-12 alert alert-success" style="@if(Session::get('success'))@else display: none @endif" role="alert">
                  {{Session::get('success')}}
                </div>
                <!-- </div> -->
            <div class="row grid-divider">
              @if($competition)
              @php $a = 0; @endphp
                @foreach($competition->activeCompetitionDetail as $key => $response)

                  <div class="col-lg-6 col-md-12">
                    <div class="video-posted">
                      <div class="container">
                        <div class="row align-items-center ">
                          <div class="col-md-7">
                            @if($competition->competition_media == 'video')
                            <div class="video-card">
                                <!-- <div class="abc"></div> -->
                                <div class="player">
                                  <div class="abc"></div>
                                  <p class="event-name">{{$response->user->userDetail->name}}</p>
                                              <p class="event-category">{{$response->userContent->name}}</p>
                                  <button type="button" class="like-btn" onclick="likeMedia('{{$key}}','{{$response->id}}',`{{$response->user_id}}`)">
                                    <i class="fa fa-spinner fa-spin like_spinner like_spinner_{{$key}}"></i>
                                    <i class="fa fa-thumbs-up"></i> <span class="total_like_{{$key}}">
                                      @php
                                        echo \App\Modals\CompetitionUserLike::where('competition_user_id',$response->id)->count();
                                      @endphp

                                    </span>
                                  </button>
                                  <button type="button" class="btn_flag mar-right" onclick="report('{{$key}}','{{$response->id}}',`{{$response->user_id}}`)">
                                      <i class="fa fa-flag"></i>
                                  </button>
                                  <video id="player1" width="640" height="360" preload="none" style="width: 100%; max-width: 100%; height: 280px" controls poster="{{config('constant.BUCKET_URL').$response->userContent->thumbnail}}" playsinline webkit-playsinline>
                                    <source src="{{config('constant.BUCKET_URL').$response->userContent->file}}" type="video/mp4">
                                  </video>
                                </div>

                            </div>
                            @elseif($competition->competition_media == 'audio')
                              <div class="post-audio-grid border-0 pb-0 mb-0">
                                  <div class="audio-posted pb-0 border-0">
                                      <div class="audio-owl-slider  owl-theme">

                                          <div id="waveform_{{$key}}" class="audio-waveform">
                                            <div class="abc"></div>
                                              <button type="button" class="like-btn" onclick="likeMedia('{{$key}}','{{$response->id}}',`{{$response->user_id}}`)">
                                                  <i class="fa fa-spinner fa-spin like_spinner like_spinner_{{$key}}"></i>
                                                  <i class="fa fa-thumbs-up"></i> <span class="total_like_{{$key}}">
                                                    @php
                                                      echo \App\Modals\CompetitionUserLike::where('competition_user_id',$response->id)->count();
                                                    @endphp

                                                  </span>
                                              </button>
                                              <button type="button" class="btn_flag mar-right" onclick="report('{{$key}}','{{$response->id}}',`{{$response->user_id}}`)">
                                                  <i class="fa fa-flag"></i>
                                              </button>
                                              <p class="event-name">{{$response->user->userDetail->name}}</p>
                                              <p class="event-category">{{$response->userContent->name}}</p>

                                              <button type="button" class="play-btn" type="button" name="playbtn" onclick="wavesurfer[{{$key}}].playPause()">
                                                  <i class="fa fa-play"></i>
                                              </button>
                                          </div>
                                          <div class="">

                                          </div>
                                      </div>
                                  </div>
                              </div>
                            @elseif($competition->competition_media == 'image')

                            <div class="post-img-grid border-0 pb-0 mb-0">
                              <div class="img-posted pb-0 border-0">
                                <div class="item dfbkdjsf">

                                  <div class="img-card-parent">
                                    <div class="abc"></div>
                                    <button type="button" class="like-btn" onclick="likeMedia('{{$key}}','{{$response->id}}',`{{$response->user_id}}`)">
                                    <i class="fa fa-spinner fa-spin like_spinner like_spinner_{{$key}}"></i>
                                    <i class="fa fa-thumbs-up"></i> <span class="total_like_{{$key}}">
                                      @php
                                        echo \App\Modals\CompetitionUserLike::where('competition_user_id',$response->id)->count();
                                      @endphp

                                    </span>
                                  </button>
                                  <button type="button" class="btn_flag mar-right" onclick="report('{{$key}}','{{$response->id}}',`{{$response->user_id}}`)">
                                      <i class="fa fa-flag"></i>
                                  </button>
                                     <p class="event-name">{{$response->user->userDetail->name}}</p>
                                              <p class="event-category">{{$response->userContent->name}}</p>
                                    <div class="img-card">
                                            <img src="{{config('constant.BUCKET_URL').$response->userContent->file}}" class="img-responsive " />
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            @endif
                          </div>
                          @php $b =0; @endphp
                          <div class="col-md-5">
                            <div class="video-btn-group">
                              <button type="button" class="btn btn-secondary abc_{{$key+1}} py-2 px-4   mini-btn rounded-pill btn_first btn_judge @if(Auth::check())

            @if((@$response->userJudge->competition_user_id == $response->id &&  @$response->userJudge->expression == 1) && @$response->userJudge->user_judge_id == Auth::user()->id)
               active
              @php $b = 1; @endphp
            @else
            @if($b== 1)  @else   @if($key == 0 && Auth::check()) active @endif @endif
            @endif
        @else
          @if($key == 0) active @endif
            @endif" data-pr="{{($key==0) ? 1 : ''}}" data-val="{{$a+1}}" onclick="activeCrown('{{$key+1}}',$(this))">
                                  <img src="../assets/images/crown.png" alt=""> 1st
                              </button>
                              <button type="button" class="btn btn-secondary abc_{{$key+1}} py-2 px-4 mini-btn rounded-pill btn_judge @if(Auth::check())
            @if((@$response->userJudge->competition_user_id == $response->id &&  @$response->userJudge->expression == 2) && @$response->userJudge->user_judge_id == Auth::user()->id)
                active

            @endif
        @endif " data-pr="" data-val="{{$a+2}}" onclick="activeCrown('{{$key+1}}',$(this))">
                                2nd
                              </button>
                            </div>
                            <div class="video-btn-group">
                              <button type="button" class="btn btn-secondary abc_{{$key+1}} py-2 px-4 mini-btn rounded-pill btn_judge  @if(Auth::check())

            @if((@$response->userJudge->competition_user_id == $response->id &&  @$response->userJudge->expression == 3) && @$response->userJudge->user_judge_id == Auth::user()->id)
                active

            @endif
        @endif" data-pr="" data-val="{{$a+3}}" onclick="activeCrown('{{$key+1}}',$(this))">
                                3rd
                              </button>
                              <button type="button" class="btn btn-secondary abc_{{$key+1}} py-2 px-4 mini-btn rounded-pill btn_judge  @if(Auth::check())
            @if((@$response->userJudge->competition_user_id == $response->id &&  @$response->userJudge->expression == 4) && @$response->userJudge->user_judge_id == Auth::user()->id)
                active
            @else
            @if($key==0)
            'active'
            @endif
            @endif
        @endif" data-pr="" data-val="{{$a+4}}" onclick="activeCrown('{{$key+1}}',$(this))">

                                4th
                              </button>
                            </div>
                            {{-- <div class="video-btn-group">
                              <button type="button" class="btn btn-secondary abc_{{$key+1}} py-2 px-4 mini-btn rounded-pill btn_judge  @if(Auth::check())

            @if((@$response->userJudge->competition_user_id == $response->id &&  @$response->userJudge->expression == 5) && @$response->userJudge->user_judge_id == Auth::user()->id)
                active

            @endif
        @endif" data-pr="" data-val="{{$a+5}}" onclick="activeCrown('{{$key+1}}',$(this))">
                                5th
                              </button>
                              <button type="button" class="btn btn-secondary abc_{{$key+1}} py-2 px-4 mini-btn rounded-pill btn_judge  @if(Auth::check())
            @if((@$response->userJudge->competition_user_id == $response->id &&  @$response->userJudge->expression == 6) && @$response->userJudge->user_judge_id == Auth::user()->id)
                active
            @else
            @if($key==0)
            'active'
            @endif
            @endif
        @endif" data-pr="" data-val="{{$a+6}}" onclick="activeCrown('{{$key+1}}',$(this))">

                                6th
                              </button>
                            </div> --}}
                            <label for="" class="video-owner-label">Posted by:</label>
                            <div class="media">
                              <img class="mr-3" src="{{($response->user->userDetail->profile_photo) ? config('constant.BUCKET_URL').$response->user->userDetail->profile_photo : asset('assets/images/no-image.png')}}" alt="Generic placeholder image">
                              <div class="media-body">
                                <h5 class="mt-0"><a href="javascript::void()">{{$response->user->userDetail->username}}.</a></h5>
                                <!-- <p>10 wins</p> -->
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  @php $a=0; @endphp
                  <input type="hidden" name="" value="@if($key == 0) {{$a+1}} @endif" class="crown_check">
                  <!-- <input type="hidden" name="" value="{{($key == 0) ? $a+1 : ''}}" class="crown_check"> -->
                  <input type="hidden" name="crown_[{{$key+1}}]" value="{{($key == 0) ? $a+1 : ''}}" class="crown_d_{{$key+1}} crown_p">
                   <input type="hidden" name="competition_user_id[{{$key+1}}]" value="{{$response->id}}" class="competition_user_id[{{$key+1}}]">
                @endforeach

                <input type="hidden" name="competition_id" value="{{request()->q}}" class="competition_id">
              @endif
            </div>

          </div>
        </form>
        </div>
      </div>
      <div class="row grid-divider">
        <div class="col-lg-8 col-md-6 col-sm-12 mb-4 mb-sm-0">
          <div class="review-tab">
            <nav>
              <div class="nav nav-tabs" id="review-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-comment-tab" data-toggle="tab" href="#nav-comment" role="tab"
                   aria-controls="nav-comment" aria-selected="true">Comment (<span class="comment_count" style="margin:0;"></span>)</a>
                <a class="nav-item nav-link" id="nav-share-tab" data-toggle="tab" href="#nav-share"
                   role="tab" aria-controls="nav-share" aria-selected="false">Share (<span id="sharedCount" style="margin:0;"></span>)</a>
              </div>
            </nav>
            <div class="tab-content" id="review-tabContent">
              <div class="tab-pane fade show active" id="nav-comment" role="tabpanel" aria-labelledby="nav-comment-tab">
                <ul class="review-list">
                  <li>
                    <div class="review-content comment_sectionAjax">
                    </div>
                  </li>
                </ul>
                  @if(Auth::check())
                <div class="text-center mb-4">
{{--                  <a class="see-all" href="">View All</a>--}}
                        <h1 class="text-muted my-5">Post a Comment</h1>
                        <div class="form-group">
                            <textarea type="text" class="form-control" id="user_comment"
                                      placeholder="Type Your Comment..." cols="20" rows="5"
                                      autofocus="true"></textarea>
                            <input type="hidden" id="hidden_competition_id" value="{{ $competition->id }}">
                            <button type="button" id="post_comment_btn" class="btn btn-primary float-right">Post</button>
                        </div>
                </div>
                  @else
                      <h5 class="text-center ">Please <button class="btn btn-primary rounded-pill" data-toggle="modal"
                                         data-target="#signUpModal" data-backdrop="static">
                                                Sign Up</button>
                          To Post Comment</h5>
                      @endif
              </div>
              <div class="tab-pane fade" id="nav-share" role="tabpanel" aria-labelledby="nav-share-tab">
                <ul class="review-list">
                  <li>
                    <div class="review-content">
                      <div class="media">
                        <img class="mr-3" src="../assets/images/user-icon.png" alt="Generic placeholder image">
                        <div class="media-body">
                          <h5 class="mt-0">Johnny S</h5>
                          <p>Few seconds ago</p>
                        </div>
                      </div>
                      <p>
                        This is a very looooooooooooooooooooooong comment to appreciate the contestants of this competition in a cool way!
                      </p>
                    </div>
                  </li>
                </ul>
                <div class="text-center mb-4">
{{--                  <a class="see-all" href="">View All</a>--}}
                    <h1 class="text-muted my-5">Sharing Post Section</h1>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
          <div class="theme-timer d-flex justify-content-around align-items-center">
            <div id="flipdown" class="flipdown"></div>
            <!-- <img src="../assets/images/timer.png" alt="timer.png"> -->
          </div>
          <div class="statistics-table">
            <h3>Competition Statistics</h3>
            <ul>
              <li>
                <span>Total Votes</span>
                <span>{{$competition_user_judge/4}} Votes</span>
              </li>
              <li>
                <span>1st (as of now)</span>
                <span class="user-name">{{@$competition_user->user->userDetail->name}}</span>
              </li>
              <li>
                <span>Tier</span>
                <span>{{$competition->competitionUserDetail->tier->name}}</span>
              </li>
             <!--  <li>
                <span>Genre</span>
                <span>Soprano</span>
              </li> -->
              <li>
                <span>Category</span>
                <span>{{$competition->tier->name}}</span>
              </li>
              <!-- <li>
                <span>Genre</span>
                <span>Soprano</span>
              </li> -->
            </ul>
            <div class="text-center">
                @if(Auth::check())
              <button type="button" class="btn btn-primary rounded-pill" data-toggle="modal"
                 data-target="#shareModal" data-backdrop="static" onclick="getShareableLink()">Share</button>
                @else
                    <p>Please <button class="btn btn-primary rounded-pill" data-toggle="modal" data-target="#signUpModal" data-backdrop="static">
                            Sign Up</button> to share Link</p>
                @endif
                <!-- Modal -->
                <div class="modal fade" id="shareModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Shareable Link</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body" id="getShareableLink">

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" id="closeShareBox" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection
@include('frontend.users.js-css-blades.select2')
@include('frontend.users.js-css-blades.form-validation')
@include('frontend.users.js-css-blades.flipdown')
@section('script')
    @parent

    <script>
        var user_id = "{{ Auth::user()->id }}";
        var competition_id = $(".competition_id").val();
        $("#closeShareBox").click(function(){
            $("#getShareableLink").html("");
        });
        $.ajax({
            url: "{{route('user.getShareableLinkCountAll')}}",
            type: 'get',
            dataType:'json',
            data:{
                user_id:user_id,
                competition_id:competition_id
            },
            success: function ( response ) {
                console.log(response.sharedCount);
                $("#sharedCount").html(response.sharedCount);
            },
        });
        function copyUrl() {

            var copyText = document.getElementById("sharedURL");
            copyText.select();
            copyText.setSelectionRange(0, 99999)
            document.execCommand("copy");
            $("#copyTxt").text("copied");

            $.ajax({
                url: "{{route('user.getShareableLinkCount')}}",
                type: 'get',
                dataType:'json',
                data:{
                    user_id:user_id,
                    competition_id:competition_id
                },
                success: function ( response ) {
                    console.log(response.sharedCount);
                    $("#sharedCount").html(response.sharedCount);
                },
            });
        }
        function getShareableLink(){
            $.ajax({
                url: "{{route('user.getShareableLink')}}",
                type: 'get',
                dataType:'json',
                success: function ( response ) {
                    console.log(response.sharedUrl);
                    $("#getShareableLink").append("" +
                        "<textarea id=\"sharedURL\" rows='2' cols='85' readonly='readonly' style='border:0;'>"+
                            response.sharedUrl+
                        "</textarea>" +
                        "<br>" +
                        "<button id=\"copyTxt\" onclick=\"copyUrl()\" class='btn btn-primary btn-sm'>Copy</button>");

                },
            });
        }
      $(document).ready(function(){
        errorHide();

      });
      function errorHide(){
        setTimeout(function(){ $('.error').fadeOut();$('.error').html(''); }, 3000);
      }

    function activeCrown(id,this_){
      var a = 0;
      $('.crown_check').each(function(key,item){
        if($(item).val() == this_.data('val')){
          $('.error').html('Sorry you have already selected');
          $('.error').show();
          errorHide();
          // alert('');
          a = 1;
          return false;
        }
      })
      if(a == 0){
        $('.abc_'+id).removeClass('active');
        this_.addClass('active');
        $('.crown_d_'+id).val(this_.data('val'));
        $('.crown_d_'+id).prev('.crown_check').val(this_.data('val'));
      }
    }
  var wavesurfer = [];
  @if($competition)
    @foreach($competition->activeCompetitionDetail as $key => $response)

      @if($competition->competition_media == 'audio')
        wavesurfer[{{$key}}] = renderWaveForm('{{config("constant.BUCKET_URL")}}'+"{{$response->userContent->file}}", '#waveform_{{$key}}');
      @endif
    @endforeach
  @endif
  function renderWaveForm(url, parentSelector) {
      var domEl = document.createElement('div')
      document.querySelector(parentSelector).appendChild(domEl)

      var wavesurferr = WaveSurfer.create({
        container: domEl,
        waveColor: 'red',
        progressColor: 'purple',
        hideScrollbar: true,
        xhr: {
        cache: "default",
        mode: "cors",
        method: "GET",
        // credentials: "gong_bucket",
        headers: [
          { key: "cache-control", value: "no-cache" },
          { key: "Access-Control-Allow-Origin", value: "*" },
          { key: "Origin", value: "http://localhost:8000" },
          { key: "Origin", value: "*" },
          { key: "pragma", value: "no-cache" }
        ]
      }
      });
      wavesurferr.load(url);
      return wavesurferr;
    }
    // $("form").submit(function(e){
    //     e.preventDefault();
    // });
    $('video, audio').mediaelementplayer({
            // Do not forget to put a final slash (/)
            pluginPath: 'https://cdnjs.com/libraries/mediaelement/',
            // this will allow the CDN to use Flash without restrictions
            // (by default, this is set as `sameDomain`)
            shimScriptAccess: 'always'
            // more configuration
        });

      $(document).ready(function(){

          $.ajax({
              url: "{{route('comment_all')}}",
              type: 'get',
              dataType:'json',
              success: function ( response ) {
                  $('.comment_count').text(response.total);
                  for (var i=0;i<response.details.length;i++){
                          $('.comment_sectionAjax').append(
                              '<div class="media">\n' +
                              '<img class="mr-3" src="https://storage.googleapis.com/gong_bucket/'+response.details[i].profile_photo+'" alt="image">\n' +
                              '<div class="media-body">\n' +
                              '<h5 class="mt-0"><a href="#">'+response.details[i].username+'</a></h5>\n' +
                              '<p>Few seconds ago</p>\n' +
                              '</div>\n' +
                              '</div>\n' +
                              '<p>' +response.details[i].content+
                              '</p>');

                  }
              },
          });
      });
      $('#post_comment_btn').click(function(){
          var comment = $('#user_comment').val();
          var competition_id = $('#hidden_competition_id').val();

          $.ajax({
              url: "{{route('comment')}}",
              type: 'post',
              data : {
                  'comment':comment,
                  'competition_id':competition_id,
              },
              dataType:'json',
              success: function ( response ) {
                  console.log(response.total);
                  $('.comment_sectionAjax').append(
                      '<div class="media">\n' +
                      '<img class="mr-3" src="https://storage.googleapis.com/gong_bucket/'+response.userDetails.profile_photo+'" alt="image">\n' +
                      '<div class="media-body">\n' +
                      '<h5 class="mt-0"><a href="#">'+response.userDetails.username+'</a></h5>\n' +
                      '<p>Few seconds ago</p>\n' +
                      '</div>\n' +
                      '</div>\n' +
                      '<p>' +response.comment+
                      '</p>');
                  $('.comment_count').text(response.total);
              },
              // error: function (errors) {
              //     $('.error_sc2_ss').html('<ul><li class="alert alert-danger">'+errors.responseJSON.message+'</li></ul>')
              //
              //     setTimeout(function(){ $(".error_sc2_ss").html(''); }, 3000);
              //     $('.code_again').html('Send code again');
              //     // $('.send_code_again').html('Verify');
              // }
          });
      })
    $('.btn_submit').click(function(){
       @if(Auth::guest())
        $('#signUpModal').modal('show');
       @else
        $("form").submit();
        @endif
     // window.location.replace("{{route('home')}}");
    })


  document.addEventListener('DOMContentLoaded', () => {
  // Unix timestamp (in seconds) to count down to
  var there = new Date("{{$competition->end_at}}");
  var here = changeTimezone(there, "{{date_default_timezone_get()}}");
  var twoDaysFromNow = (here/ 1000);
  // Set up FlipDown
  // console.log(here);
  var flipdown = new FlipDown(twoDaysFromNow)
    // Start the countdown
    .start()
    // Do something when the countdown ends
    .ifEnded(() => {
      console.log('The countdown has ended!');
    });
});
  function changeTimezone(date, ianatz) {

  // suppose the date is 12:00 UTC
  var invdate = new Date(date.toLocaleString('en-US', {
    timeZone: ianatz
  }));

  // then invdate will be 07:00 in Toronto
  // and the diff is 5 hours
  var diff = date.getTime() - invdate.getTime();

  // so 12:00 in Toronto is 17:00 UTC
  return new Date(date.getTime() + diff);

}
function likeMedia(key,c_id,u_id){
      $('.like_spinner_'+key).show();
      axios({
              method: 'post',
              url: "{{route('user.content.add_like')}}",
              data:{'competition_user_id':c_id,'user_id':u_id},
              responseType: 'json'
            }).then(function (response) {
              $('.total_like_'+key).html(response.data.total_like);
            }).catch(function (error) {
          $('#signUpModal').modal("show");
      }).finally(function () {
              $('.like_spinner_'+key).hide();
          });
    }
    $(document).on('click','.play-btn',function(){
      $(this).find('i').toggleClass("fa-play fa-pause");
    })
</script>
@endsection
