@extends('layouts.master')
@section('content')
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-sm-12">
        <div class="user-card">
          <img src="../assets/images/user-icon.png" alt="">
          <div>
            <label for="">John Doe</label>
          </div>
          <p>Competition Winner</p>
        </div>
        <div class="d-flex flex-lg-column justify-content-sm-between justify-content-center flex-wrap">
          <div class="user-info">
            <div class="info-field">
              <span><i class="fa fa-hand-scissors-o"></i>Search me with </span> <label>johndoe</label>
            </div>
            <div class="info-field">
              <span><i class="fa fa-envelope-o"></i>Email at </span> <a href="mailto:johndoe@company.com">johndoe@company.com </a>
            </div>
            <div class="info-field">
              <span><i class="fa fa-phone"></i>Reach me at  </span> <label>0123456789</label>
            </div>
            <div class="info-field">
              <span><i class="fa fa-venus"></i>Gender is</span> <label> Male</label>
            </div>
            <div class="info-field">
              <span><i class="fa fa-calendar"></i>Wish me on  </span> <label>01-01-1990</label>
            </div>
            <div class="info-field">
              <span><i class="fa fa-home"></i>Lives in </span> <label>United States</label>
            </div>
          </div>
          <div class="competition-btn-group">
            <div class=" py-3 text-center">
              <button class="btn btn-primary rounded-pill">Submit for Competition</button>
            </div>
            <div class=" py-3 text-center mb-4">
              <button class="btn btn-primary rounded-pill">Create Competition</button>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-9 col-sm-12">
        <div class="competition-from">
          <nav class="form-steps-nav">
            <div class="nav nav-tabs" id="competitor-nav-tab" role="tablist">
              <a class="nav-item nav-link " id="nav-Social-tab" data-toggle="tab" href="#nav-Social" role="tab" aria-selected="true">Social</a>
              <a class="nav-item nav-link active" id="nav-Competitions-tab" data-toggle="tab" href="#nav-Competitions" role="tab" aria-selected="false">Competitions</a>
              <a class="nav-item nav-link" id="nav-Shop-tab" data-toggle="tab" href="#nav-Shop" role="tab" aria-selected="false">Shop</a>
              <a class="nav-item nav-link" id="nav-About-tab" data-toggle="tab" href="#nav-About" role="tab" aria-selected="false">About</a>
            </div>
          </nav>
          <div class="tab-content form-steps-content" id="competitor-nav-tabContent">
            <div class="tab-pane fade" id="nav-Social" role="tabpanel" aria-labelledby="nav-Social-tab">
              <div class="container">
                <div class="row">
                  <div class="col-12">
                    <div class="competition-title align-items-end mb-2 d-flex justify-content-between">
                      <h2>
                        Submission Successful
                      </h2>
                    </div>
                    <p class="title-lead">
                      Your video has been submitted for Public competition. You will be notified when the public competition starts.
                    </p>
                    <div class="container">
                      <div class="row pb-4 mb-5">
                        <div class="col-12 col-sm-4 offset-md-4 align-self-center">
                          <form action="" class="theme-form">
                            <div class="input-field">
                              <label for="code" class="text-dark">Code</label>
                              <div class="input-group mb-4">
                                <input type="text" class="form-control border-dark" id="code">
                              </div>
                            </div>
                            <div class="text-center">
                              <button class="py-2 btn btn-primary rounded-pill">Submit</button>
                            </div>
                          </form>
                        </div>
                        <div class="col-12 my-4">
                          <p class="border-with-text fancy"><span>OR</span></p>
                        </div>
                        <div class="col-12">
                          <div class="competition-title align-items-end mb-2 d-flex justify-content-between">
                            <h2>
                              Join an Existing Competition
                            </h2>
                          </div>
                        </div>
                        <div class="container">
                          <div class="row">
                            <div class="col-md-6 mb-4">
                              <div class="join-list" data-toggle="modal" data-target="#join-list-modal">
                                <div class="d-flex justify-content-between align-items-center">
                                  <h4>Creative Competition</h4>
                                  <i class="fa fa-microphone"></i>
                                </div>
                                <table class="table">
                                  <tr>
                                    <td>
                                      <i class="fa fa-list-ol"></i>
                                      <span>2Round</span>
                                    </td>
                                    <td>
                                      <i class="fa fa-user"></i>
                                      <span>8 Participants</span>
                                    </td>
                                    <td>
                                      <i class="fa fa-clock-o"></i>
                                      <span>24 hours</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <i class="fa fa-user-plus"></i>
                                      <span>Duet</span>
                                    </td>
                                    <td>
                                      <i class="fa fa-user-times"></i>
                                      <span>Elimination</span>
                                    </td>
                                    <td>
                                      <i class="fa fa-clock-o"></i>
                                      <span>2 days</span>
                                    </td>
                                  </tr>
                                </table>
                              </div>
                            </div>
                            <div class="col-md-6 mb-4">
                              <div class="join-list" data-toggle="modal" data-target="#join-list-modal">
                                <div class="d-flex justify-content-between align-items-center">
                                  <h4>Creative Competition</h4>
                                  <i class="fa fa-microphone"></i>
                                </div>
                                <table class="table">
                                  <tr>
                                    <td>
                                      <i class="fa fa-list-ol"></i>
                                      <span>2Round</span>
                                    </td>
                                    <td>
                                      <i class="fa fa-user"></i>
                                      <span>8 Participants</span>
                                    </td>
                                    <td>
                                      <i class="fa fa-clock-o"></i>
                                      <span>24 hours</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <i class="fa fa-user-plus"></i>
                                      <span>Duet</span>
                                    </td>
                                    <td>
                                      <i class="fa fa-user-times"></i>
                                      <span>Elimination</span>
                                    </td>
                                    <td>
                                      <i class="fa fa-clock-o"></i>
                                      <span>2 days</span>
                                    </td>
                                  </tr>
                                </table>
                              </div>
                            </div>
                            <div class="col-md-6 mb-4">
                              <div class="join-list" data-toggle="modal" data-target="#join-list-modal">
                                <div class="d-flex justify-content-between align-items-center">
                                  <h4>Creative Competition</h4>
                                  <i class="fa fa-microphone"></i>
                                </div>
                                <table class="table">
                                  <tr>
                                    <td>
                                      <i class="fa fa-list-ol"></i>
                                      <span>2Round</span>
                                    </td>
                                    <td>
                                      <i class="fa fa-user"></i>
                                      <span>8 Participants</span>
                                    </td>
                                    <td>
                                      <i class="fa fa-clock-o"></i>
                                      <span>24 hours</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <i class="fa fa-user-plus"></i>
                                      <span>Duet</span>
                                    </td>
                                    <td>
                                      <i class="fa fa-user-times"></i>
                                      <span>Elimination</span>
                                    </td>
                                    <td>
                                      <i class="fa fa-clock-o"></i>
                                      <span>2 days</span>
                                    </td>
                                  </tr>
                                </table>
                              </div>
                            </div>
                            <div class="col-md-6 mb-4">
                              <div class="join-list" data-toggle="modal" data-target="#join-list-modal">
                                <div class="d-flex justify-content-between align-items-center">
                                  <h4>Creative Competition</h4>
                                  <i class="fa fa-microphone"></i>
                                </div>
                                <table class="table">
                                  <tr>
                                    <td>
                                      <i class="fa fa-list-ol"></i>
                                      <span>2Round</span>
                                    </td>
                                    <td>
                                      <i class="fa fa-user"></i>
                                      <span>8 Participants</span>
                                    </td>
                                    <td>
                                      <i class="fa fa-clock-o"></i>
                                      <span>24 hours</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <i class="fa fa-user-plus"></i>
                                      <span>Duet</span>
                                    </td>
                                    <td>
                                      <i class="fa fa-user-times"></i>
                                      <span>Elimination</span>
                                    </td>
                                    <td>
                                      <i class="fa fa-clock-o"></i>
                                      <span>2 days</span>
                                    </td>
                                  </tr>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade  show active" id="nav-Competitions" role="tabpanel" aria-labelledby="nav-Competitions-tab">
              <div class="competition-tab">
                <div class="mb-3">
                  <div class="red-title-bg">
                    <div class="d-flex justify-content-sm-between justify-content-center align-items-center">
                      <h2 class="m-0">Active Competitions <span>info-circle</span></h2>
                      <a href="">View all</a>
                    </div>
                  </div>
                  <div class="active-competition-videos">
                    <div class="row">
                      <div class="col-md-4">
                        <div class="video-card">
                          <img src="../assets/images/premier-placeholder.png" alt="">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="video-card">
                          <img src="../assets/images/premier-placeholder.png" alt="">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="video-card">
                          <img src="../assets/images/premier-placeholder.png" alt="">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="mb-3">
                  <div class="gray-title-bg">
                    <div class="d-flex justify-content-sm-between justify-content-center align-items-center">
                      <h2 class="m-0">Active Competitions <span>info-circle</span></h2>
                      <a href="">View all</a>
                    </div>
                  </div>
                  <div class="active-competition-videos">
                    <div class="row">
                      <div class="col-md-4">
                        <div class="video-card">
                          <img src="../assets/images/premier-placeholder.png" alt="">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
            <div class="tab-pane fade" id="nav-Shop" role="tabpanel" aria-labelledby="nav-Shop-tab">Shop</div>
            <div class="tab-pane fade" id="nav-About" role="tabpanel" aria-labelledby="nav-About-tab">Shop</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="footer mt-5">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-6 col-12 text-center ">
        <div class="footer-widget d-flex align-items-center justify-content-center pb-4 pb-sm-0 h-100">
          <img class="logo" src="../assets/images/footer-logo.png" alt="footer-logo">
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-12 text-center text-sm-left">
        <div class="footer-widget">
          <h3>GONG</h3>
          <ul>
            <li><a href="">Advertise</a></li>
            <li><a href="">Investor Portal</a></li>
            <li><a href="">Press</a></li>
            <li><a href="">Releases</a></li>
            <li><a href="">Contact</a></li>
            <li><a href="">Settings</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-12 text-center text-sm-left">
        <div class="footer-widget">
          <h3>JOIN COMPETITIONS</h3>
          <ul>
            <li><a href="">Login</a> </li>
            <li><a href="">Register</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-12 text-center text-sm-left">
        <div class="footer-widget">
          <h3>FOLLOW US</h3>
          <ul class="social-media">
            <li>
              <a style="" href="">
                <img src="../assets/images/fb.png" alt="">
              </a>
            </li>
            <li>
              <a style="" href="">
                <img src="../assets/images/twitter.png" alt="">
              </a>
            </li>
            <li>
              <a style="" href="">
                <img src="../assets/images/insta.png" alt="">
              </a>
            </li>
            <li>
              <a style="" href="">
                <img src="../assets/images/pin.png" alt="">
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="copy-right">
          <p>
            2020, Gong. All Rights reserved.
          </p>
        </div>
      </div>
    </div>
  </div>
</div>

<!--Join list modal start-->

<div class="modal fade" id="join-list-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="max-width: 525px; width: 100%; margin: 10.75rem auto;" role="document">
    <div class="modal-content join-list modal-list">
      <div class="modal-body p-1">
        <div class="table-responsive">
          <table class="table ">
            <tr>
              <td>
                <p>Competition Name</p>
                <label for="">Hunt for Talent</label>
              </td>
              <td>
                <p>Upload Type</p>
                <label for="">Audio</label>
              </td>
              <td>
                <p>Number of Competitors</p>
                <label for="">8 Participants</label>
              </td>
            </tr>
            <tr>
              <td>
                <p>Competition Name</p>
                <label for="">Hunt for Talent</label>
              </td>
              <td>
                <p>Upload Type</p>
                <label for="">Audio</label>
              </td>
              <td>
                <p>Number of Competitors</p>
                <label for="">8 Participants</label>
              </td>
            </tr>
            <tr>
              <td>
                <p>Competition Name</p>
                <label for="">Hunt for Talent</label>
              </td>
              <td>
                <p>Upload Type</p>
                <label for="">Audio</label>
              </td>
              <td>
                <p>Number of Competitors</p>
                <label for="">8 Participants</label>
              </td>
            </tr>
          </table>
        </div>
      </div>
      <div class="modal-footer justify-content-center border-0 p-0">
        <button type="button" class="btn btn-primary rounded-pill ">Join</button>
        <button type="button" class="btn btn-secondary rounded-pill" data-dismiss="modal">cancel</button>
      </div>
    </div>
  </div>
</div>
<!--Join list modal End-->

@endsection
@include('frontend.users.js-css-blades.select2')
@include('frontend.users.js-css-blades.form-validation')
@include('frontend.users.js-css-blades.flipdown')