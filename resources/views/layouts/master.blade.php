<!DOCTYPE html>
<html lang="en">
@include('frontend.users.layout.header')
<body>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T8HCMK9"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

@include('frontend.users.layout.top_nav')

<div class="content">

    @yield('content')

</div>


@if(!auth()->check())


    <div class="modal fade" id="signUpModal" tabindex="-1" role="dialog" aria-labelledby="signUpModalLabel"
         aria-hidden="true">

        <div class="modal-dialog" role="document">

            <div class="modal-content theme-modal sign-up-modal"
                 style="background: url({{asset('assets/images/modal-bg.png')}}) no-repeat center;background-size: cover">

                <div class="modal-body">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">&times;</span>

                    </button>

                    <span class="modal-divider">

                   OR

                 </span>

                    <div class="container">

                        <div class="row align-items-center">

                            <div class="col-md-6 col-sm-6">

                                <div class="already-account">

                                    <p>Already have an account?</p>

                                    <a href="javascript:;" class="login_model_cl" data-toggle="modal"
                                       data-target="#loginModal">Log In</a>

                                </div>

                            </div>

                            <div class="col-md-6 col-sm-6 text-center bg-red py-5">

                                <form action="{{route('user.doRegister')}}" method="post" id="signupForm">
                                

                                    <h2 class="modal-title">Sign Up</h2>

                                    <p class="title-description">

                                        Please enter your details to sign up and be a part of great community.

                                    </p>

                                    <div class="error_signup_1">

                                    </div>

                                    <div class="input-field">

                                        <label for="email">Email*</label>

                                        <div class="input-group mb-3">

                                            <div class="input-pre-icon">

                                                <i class="fa fa-envelope"></i>

                                            </div>

                                            <input type="email" name="email" class="form-control" id="email">

                                        </div>

                                    </div>

                                    <div class="input-field chuspic">

                                        <label for="Phone">Phone*</label>

                                        <div class="input-group mb-3">

                                            <!--  <div class="input-pre-icon">

                                                 <i class="fa fa-phone"></i>

                                             </div> -->

                                            <input type="tel" autocomplete="off" name="phone"
{{--                                                   oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"--}}
                                                    oninput="phoneFormatter()"
                                                   class="form-control telephone phone" id="Phone">

                                        </div>

                                    </div>

                                    <div class="input-field">

                                        <label for="Password">Password*</label>

                                        <div class="input-group mb-3">

                                            <div class="input-pre-icon">

                                                <i class="fa fa-key"></i>

                                            </div>

                                            <input type="password" class="form-control" name="password" id="Password">

                                        </div>

                                    </div>

                                    <div class="input-field">

                                        <label for="confirm-password">Confirm Password*</label>

                                        <div class="input-group mb-3">

                                            <div class="input-pre-icon">

                                                <i class="fa fa-key"></i>

                                            </div>

                                            <input type="password" class="form-control" name="password_confirmation"
                                                   id="password_confirmation">

                                        </div>

                                    </div>

                                    <button class="btn btn-primary rounded-pill mt-3 btn_signup_1">Sign Up</button>

                                    <button class="btn btn-primary rounded-pill mt-3 btn_login">


<a href="{{ URL('/login/facebook') }}" class="btn btn-facebook" class="btn btn-facebook"><i class="fa fa-facebook"></i> Facebook</a>

</button>

                                </form>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>



    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel"
         aria-hidden="true">

        <div class="modal-dialog" role="document">

            <div class="modal-content theme-modal login-modal"
                 style="background: url({{asset('assets/images/modal-bg.png')}}) no-repeat center;background-size: cover">

                <div class="modal-body">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">&times;</span>

                    </button>

                    <span class="modal-divider">

                   OR

                 </span>

                    <div class="container">

                        <div class="row align-items-center">

                            <div class="col-md-6 col-sm-6 text-center bg-red py-5">

                                <form action="{{route('user.doLogin')}}" method="post" id="loginForm"
                                      autocomplete="off">

                                    <h2 class="modal-title">Log In</h2>

                                    <p class="title-description">

                                        to your account

                                    </p>

                                    <div class="login_errors"></div>

                                    <div class="input-field">

                                        <label for="login-email">Email*</label>

                                        <div class="input-group mb-3">

                                            <div class="input-pre-icon">

                                                <i class="fa fa-envelope"></i>

                                            </div>

                                            <input type="email" name="email" class="form-control" id="email_login">

                                        </div>

                                    </div>

                                    <div class="input-field">

                                        <label for="login-password">Password*</label>

                                        <div class="input-group mb-3">

                                            <div class="input-pre-icon">

                                                <i class="fa fa-key"></i>

                                            </div>

                                            <input type="password" name="password" class="form-control"
                                                   id="password_login">

                                        </div>

                                    </div>
                                    <div class="input-field">

                                        <!-- <label for="login-password">Password*</label> -->
                                        <div class="checkbox checkbox-primary checkbox-inline mr-3">
                                            <input type="checkbox" class="" name="remember_me" value="" checked="">
                                            <label for="aaa">Remember me </label>
                                        </div>
                                    </div>

                                    <button class="btn btn-primary rounded-pill mt-3 btn_login">

                                        Login

                                    </button>

                                  

                                </form>

                            </div>

                            <div class="col-md-6 col-sm-6">

                                <div class="already-account mb-0 mb-sm-4">

                                    <p>Don't have an account?</p>

                                    <a href="javascript:;" data-toggle="modal" class="s_modal"
                                       data-target="#signUpModal"> Sign Up</a>

                                       

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="modal fade" id="verification-phone-1" tabindex="-1" role="dialog"
         aria-labelledby="verification-phone-1Label" aria-hidden="true">
        <div class="modal-dialog verify-wrapper" role="document">
            <div class="modal-content dialog">
                <div class="">
                    <div class="error_sc2_ss"></div>
                    <button type="" class="close" data-dismiss="modal" aria-label="Close">×</button>
                    <h3>Please enter the 4-digit verification code we sent via SMS:</h3>
                    <span>We want to make sure its you</span>
                    <div id="form">
                        <input type="text" name="code_name1" class="code_name1" maxLength="1" size="1" min="0" max="9"
                               pattern="[0-9]{1}"/>
                        <input type="text" name="code_name2" class="code_name2" maxLength="1" size="1" min="0" max="9"
                               pattern="[0-9]{1}"/><input type="text" name="code_name3" class="code_name3" maxLength="1"
                                                          size="1" min="0" max="9" pattern="[0-9]{1}"/><input
                            type="text" name="code_name4" class="code_name4" maxLength="1" size="1" min="0" max="9"
                            pattern="[0-9]{1}"/>
                        <button class="btn btn-primary btn-embossed btn_code_v_ddd">Verify</button>
                    </div>

                    <div>
                        Didn't receive the code?<br/>
                        <a href="javascript:;" class="code_again">Send code again</a><br/>
                        <a href="javascript:;" id="chang_ph_no">Change phone number</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="verification-phone-2" tabindex="-1" role="dialog"
         aria-labelledby="verification-phone-2Label" aria-hidden="true">
        <div class="modal-dialog verify-wrapper" role="document">
            <div class="modal-content dialog">
                <div class="aaffs_dfdf"></div>
                <div class="">
                    <button class="close" data-dismiss="modal">×</button>
                    <h3>Change Phone Number</h3>
                    <div id="form2" class="text-center chuspic">
                        <input class="form-control mb-3 phone_no_a telephone_A" name="phone_no_a" type="tel"
                               oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"/>
                        <button class="btn btn-primary btn-embossed btn_phone_chan_ddd mt-4">Confirm</button>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="signUpModal2" tabindex="-1" role="dialog" aria-labelledby="signUpModalLabel"
         aria-hidden="true">

        <div class="modal-dialog" role="document">

            <div class="modal-content theme-modal sign-up-modal"
                 style="background: url({{asset('assets/images/modal-bg.png')}}) no-repeat center;background-size: cover">

                <div class="modal-body">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">&times;</span>

                    </button>

                    <span class="modal-divider">

            &

          </span>

                    <div class="container">

                        <div class="row align-items-center">

                            <div class="col-md-6 col-sm-6">

                                <div class="already-account mb-0 mb-sm-4">

                                    <p>Already have an account?</p>

                                    <a href="javascript:;" class="login_btn_signup2">Log In</a>

                                </div>


                            </div>

                            <div class="col-md-6 col-sm-6 text-center bg-red py-5">


                                <form action="{{route('user.addUserDetail')}}" method="post" id="signUpForm2"
                                      enctype="multipart/form-data">
                                      {{csrf_field()}}

                                    <input type="hidden" name="user_id" value="" id="user_id">

                                    <input type="hidden" name="gender" value="male" id="gender">

                                    <div class="upload-user-img">

                                        <img src="{{asset('assets/images/user-placeholder.png')}}" alt=""
                                             id="userImagePreview">

                                        <span>

                                              <i class="material-icons">

                                              photo_camera

                                            </i>

                                            </span>

                                        <input type="file" id="profile_photo" accept="image/*" name="profile_photo">

                                    </div>

                                    <div class="error_signup_2">

                                    </div>

                                    <div class="input-field">

                                        <label for="Name">First Name</label>

                                        <div class="input-group mb-3">

                                            <div class="input-pre-icon">

                                                <i class="fa fa-user"></i>

                                            </div>

                                            <input type="text" class="form-control" name="first_name" id="first_name">

                                        </div>

                                    </div>
                                    <div class="input-field">

                                        <label for="Name">Last Name</label>

                                        <div class="input-group mb-3">

                                            <div class="input-pre-icon">

                                                <i class="fa fa-user"></i>

                                            </div>

                                            <input type="text" class="form-control" name="last_name" id="last_name">

                                        </div>

                                    </div>
                                    <input type="hidden" name="file_canvas" value="" id="file_canvas">

                                    <div class="input-field">

                                        <label for="Username">Username</label>

                                        <div class="input-group mb-3">

                                            <div class="input-pre-icon">

                                                <i class="fa fa-scissors" aria-hidden="true"></i>

                                            </div>

                                            <input type="text" class="form-control" name="username" id="username">

                                        </div>

                                    </div>

                                    <div class="input-field sponserBasedFiled dobForAgent">

                                        <label for="Date of Birth">Date of Birth</label>

                                        <div class="input-group mb-3">

                                            <div class="input-pre-icon">

                                                <i class="fa fa-calendar"></i>

                                            </div>

                                            <input type="date" class="form-control" name="date_of_birth"
                                                   id="date_of_birth">

                                        </div>

                                    </div>
                                    <div class="user-gender mb-3 sponserBasedFiled">

                                        <label for=""> I'm a</label>

                                        <div class="mb-4 mb-sm-0">

                                            <button type="button"
                                                    class="btn btn-primary rounded-pill mr-2 gender-selector"
                                                    data-value="male">

                                                <img src="{{asset('assets/images/male-icon.png')}}" alt="male"> Male

                                            </button>

                                            <button type="button" class="btn btn-light rounded-pill gender-selector"
                                                    data-value="female">

                                                <img src="{{asset('assets/images/female-icon.png')}}" alt="female">
                                                Female

                                            </button>

                                        </div>

                                    </div>
                                    <div class="user-gender mb-3">

                                        <label for=""> Subscribe to App Notification</label>
                                        <div class="mb-4 mb-sm-0">
                                            <label class="radio-inline">
                                                <input type="radio" name="app_notify" value="1" checked> yes
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="app_notify" value="0"> no
                                            </label>
                                        </div>

                                    </div>
                                    <div class="user-gender mb-3">
{{--for subscription--}}
                                        <label for=""> Subscribe to Text Notification</label>
                                        <div class="mb-4 mb-sm-0">
                                            <label class="radio-inline">
                                                <input type="radio" name="text_notify" value="1" checked> yes
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="text_notify" value="0"> no
                                            </label>
                                        </div>

                                    </div>
                                    <div class="input-field">

                                        <label for="Country">Country</label>

                                        <div class="input-group mb-3">

                                            <div class="input-pre-icon">

                                                <img src="{{asset('assets/images/country-icon.png')}}" alt="genre-icon"
                                                     class="country-icon">

                                            </div>


                                             

                                               <select name="country_id" class="form-control white-icon country_sign_up"
                                                    id="Country">
                                                @if(@$countries)
                                                    @if($location == null)
                                                        @php
                                                            $location['phonecode'] = '';
                                                        @endphp
                                                    @endif()
                                                    @foreach($countries as $country)

                                                        <option value="{{$country->id}}"
                                                            {{($country->iso2 == $location['phonecode']) ? 'selected' : ''}}>
                                                            {{ucFirst($country->name)}}
                                                        </option>
                                                    @endforeach
                                                @endif

                                            </select>


                                            </div>


                                        </div>

                                    </div>

                                    <div class="input-field">

                                        <label for="region">Region</label>

                                        <div class="input-group mb-3">

                                            <div class="input-pre-icon">

                                                <img src="{{asset('assets/images/country-icon.png')}}" alt="genre-icon"
                                                     class="country-icon">

                                            </div>

                                            <select name="region_id" class="form-control white-icon states_sign_up"
                                                    id="region">

                                            </select>

                                        </div>

                                    </div>

                                    <button class="btn btn-primary rounded-pill mt-3 btn_signup_2">Complete Sign Up
                                    </button>

                                </form>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!--    Sign Up Modal Start-->
    <div class="modal fade" id="croppingModal" tabindex="-1" role="dialog" aria-labelledby="modalExpample-5Label"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content sign-up-modal" style="background: #940000;">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="text-white">&times;</span>
                    </button>
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-12 pt-5 text-center">
                                <form action="" style="max-width: 720px;">
                                    <div class="text-center mb-5">
                                        <div class="upload-user-img preview rounded-circle">

                                        </div>
                                    </div>
                                    <div class="">
                                        <img style="max-width:  520px; width: 100%" id="image"
                                             src="{{asset('assets/images/upload-placeholder.png')}}" alt="">
                                    </div>

                                    <div class="mt-3 mb-3">
                                        <!-- <button type="button" class="btn btn-primary">
                                          Back
                                        </button> -->
                                        <button type="button" class="btn btn-primary save_crop">
                                            Save
                                        </button>
                                        <!-- <button type="button" class="btn btn-primary">
                                          Next
                                        </button> -->
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--    Sign Up Modal End-->




    <div class="modal fade" id="signUpModal_3" tabindex="-1" role="dialog" aria-labelledby="signUpModal-3Label"
         aria-hidden="true">

        <div class="modal-dialog" role="document">

            <div class="modal-content theme-modal sign-up-modal"
                 style="background: url({{asset('assets/images/modal-bg.png')}}) no-repeat center;background-size: cover">

                <div class="modal-body">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">&times;</span>

                    </button>

                    <!-- <span class="modal-divider">

                              OR

                            </span> -->

                    <div class="container">

                        <div class="row align-items-center">


                            <div class="col-md-6 col-sm-6">

                                <div class="already-account">

                                    <p>Please provide further details to be a part of great community.</p>

                                </div>

                            </div>

                            <div class="col-md-6 col-sm-6 text-center bg-red py-5">

                                <form action="" method="post" id="signupForm">

                                    <h2 class="modal-title">Sign Up</h2>

                                    <p class="title-description">

                                        Hi, Which of the following best describes you?

                                    </p>


                                    <div class="sign-up-selection">

                                        <div class="media active" onclick="changeIMG(this,'talent')">

                                            <img class="on-active mr-2"
                                                 src="{{asset('assets/images/Talent-form-icon.png')}}" alt="Talent">

                                            <img class="de-active mr-2"
                                                 src="{{asset('assets/images/Talent-form-icon-1.png')}}" alt="Talent">

                                            <input type="hidden" name="role" class="user_role" value="talent">

                                            <div class="media-body">

                                                <h5 class="mt-0">Talent</h5>

                                                <p>

                                                    (I want to showcase my talents, find an agent, compete, build a fan
                                                    base, and be able to collect tips and sell my content and
                                                    materials.)

                                                </p>

                                            </div>

                                        </div>

                                        <div class="media" onclick="changeIMG(this,'fan')">

                                            <img class="on-active mr-2"
                                                 src="{{asset('assets/images/fan-form-icon.png')}}" alt="fan">

                                            <img class="de-active mr-2"
                                                 src="{{asset('assets/images/fan-form-icon-1.png')}}" alt="fan">

                                            <div class="media-body">

                                                <h5 class="mt-0">Fan</h5>

                                                <p>

                                                    (I want to watch to watch funny or incredible content, support my
                                                    friends, and be entertained.)

                                                </p>

                                            </div>

                                        </div>

                                        <div class="media agentType" onclick="changeIMG(this,'agent')">

                                            <img class="on-active mr-2"
                                                 src="{{asset('assets/images/agent-form-icon.png')}}" alt="agent">

                                            <img class="de-active mr-2"
                                                 src="{{asset('assets/images/agent-form-icon-1.png')}}" alt="agent">

                                            <div class="media-body">

                                                <h5 class="mt-0">Agent</h5>

                                                <p>

                                                    (I want to promote my business, create special private competitions
                                                    for talent and connect with everyone.)

                                                </p>

                                            </div>

                                        </div>

                                        <div class="media sponserType" onclick="changeIMG(this,'sponser')">

                                            <img class="on-active mr-2"
                                                 src="{{asset('assets/images/Sponsor-form-icon.png')}}" alt="Sponsor">

                                            <img class="de-active mr-2"
                                                 src="{{asset('assets/images/Sponsor-form-icon-1.png')}}" alt="Sponsor">

                                            <div class="media-body">

                                                <h5 class="mt-0">Sponsor</h5>

                                                <p>

                                                    (I am here to promote my business, create special private
                                                    competitions for talent and connect with everyone.)

                                                </p>

                                            </div>

                                        </div>

                                    </div>


                                    <button class="btn btn-primary rounded-pill mt-3 btn_signup_role_3">Sign Up</button>

                                </form>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>


    @include('frontend.users.js-css-blades.form-validation')
    @include('frontend.users.js-css-blades.select2')

@endif


<!--Bootstrap 4 JS files-->
<script src="https://code.jquery.com/jquery-3.2.1.min.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<!--Bootstrap 4 JS files-->

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    // $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"
        integrity="sha256-3blsJd4Hli/7wCQ+bmgXfOdK7p/ZUMtPXY08jmxSSgk=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"
        integrity="sha256-pTxD+DSzIwmwhOqTFN+DB+nHjO4iAsbgfyFq5K5bcE0=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.2.15/mediaelement-and-player.min.js"
        integrity="sha256-8/UOjLaSyiqm0IvIvezgdQ4B7tMGZHUW6071T8EgmsI=" crossorigin="anonymous"></script>
<script src="{{asset('assets/js/wavesurfer.js')}}"></script>
<script src="{{asset('assets/js/jquery.waitablebutton.js')}}"></script>
@if(!auth()->check())
    @include('frontend.users.js-css-blades.sweetalert')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css"
          integrity="sha256-jKV9n9bkk/CTP8zbtEtnKaKf+ehRovOYeKoyfthwbC8=" crossorigin="anonymous"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"
            integrity="sha256-CgvH7sz3tHhkiVKh05kSUgG97YtzYNnWt6OXcmYzqHY=" crossorigin="anonymous"></script>
@endif
<script src="{{asset('assets/js/custom.js')}}"></script>
@include('frontend.users.pages.modal.reporting-modal')

@include('frontend.users.js-css-blades.telephone')
<script src="http://localhost/gong-backend-development/assets/js/typeahead.bundle.min.js"></script>

<script type="text/javascript">
    var users = [];
    $.ajax({
            url:"{{ Route('user.getAllActiveUsers') }}",
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (response) {
                console.log(response.user_details);
                $.each(response.user_details, function(i) {
                    users.push(response.user_details[i].username);
                });
            }
        });

    var users = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: users
    });

    $('.typeahead').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'states',
            source: users
        });
    @if(!auth()->check())
    function phoneFormatter() {
        $('.phone').on('input', function () {
            var number = $(this).val().replace(/[^\d]/g, '')
            if (number.length == 7) {
                number = number.replace(/(\d{3})(\d{4})/, "$1-$2");
                //US & Canada Formatting
            } else if (number.length == 10) {
                number = number.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
            }
            //France Formatting
            else if (number.length == 11) {
                number = number.replace(/(\d{2})(\d{1})(\d{2})(\d{2})(\d{2})(\d{2})/, "+$1 $2 $3 $4 $5 $6");
            }
            //German Formattiing
            else if (number.length == 13) {
                number = number.replace(/(\d{2})(\d{3})(\d{8})/, "+$1 $2 $3");
            }
            $(this).val(number)
        });
    };

    $(".sponserBasedFiled").removeClass('remove_fields');

    $('.sponserType').click(function () {
        $(".sponserBasedFiled").addClass('remove_fields');
        $(".dobForAgent").html('<input type="hidden" name="spoonser_role_hidden" value="spoonser"/>');
    });

    $('.agentType').click(function () {
        $(".sponserBasedFiled").addClass('remove_fields');
        $(".dobForAgent").html('<input type="hidden" name="agent_role_hidden" value="agent"/>');
    });

    $(document).ready(function () {


    // $("#verification-phone-2").modal("show");
        $('.login_model_cl').click(function () {
            $('#signUpModal2').modal('hide');
            $('#signUpModal').modal('hide');
        });
        $('.login_btn_signup2').click(function () {
            $('#loginModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#signUpModal2').modal('hide');
        })
        $('.s_modal').click(function () {
            $('#loginModal').modal('hide');
            $('#signUpModal').modal('hide');
            $('#signUpModal2').modal('hide');
        });
        $("#signUpModal").on('show.bs.modal', function () {
            $('#signUpModal').find('.help-block').html('');
            // $('#signupForm')[0].reset();
        });
        $("#signUpModal2").on('show.bs.modal', function () {
            $('#signUpModal2').find('.help-block').html('');
            $("#signUpModal").modal('hide');
            $('#signUpForm2')[0].reset();
        });
        $("#signUpModal_3").on('show.bs.modal', function () {
            // $('#signUpModal2').find('.help-block').html('');
            $("#signUpModal").modal('hide');
            $('#signUpForm2')[0].reset();
        });
        $("#loginModal").on('show.bs.modal', function () {
            // $('#loginForm').reset();
            $('#loginModal').find('.help-block').html('');
        });
        $("#signupForm").validate({
            rules: {
                phone: {
                    required: true,
                    maxlength: 15,
                },
                password: {
                    required: true,
                    minlength: 6
                },
                password_confirmation: {
                    required: true,
                    minlength: 6,
                    equalTo: "#Password"
                },
                email: {
                    required: true,
                    email: true
                },
            },
            messages: {
                // username1: {
                //  required: "Please enter a username",
                //  minlength: "Your username must consist of at least 2 characters"
                // },
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 6 characters long"
                },
                password_confirmation: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 6 characters long",
                    equalTo: "Please enter the same password as above"
                },
                email: "Please enter a valid email address",
                // agree1: "Please accept our policy"
            },
            errorElement: "em",
            errorPlacement: function (error, element) {
                // Add the `help-block` class to the error element
                error.addClass("help-block");

                // Add `has-feedback` class to the parent div.form-group
                // in order to add icons to inputs
                element.parents(".col-sm-5").addClass("has-feedback");

                if (element.prop("type") === "checkbox") {
                    error.insertAfter(element.parent("label"));
                } else {
                    error.insertAfter(element);
                }

                // Add the span element, if doesn't exists, and apply the icon classes to it.
                if (!element.next("span")[0]) {
                    $("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").insertAfter(element);
                }
            },
            success: function (label, element) {
                // Add the span element, if doesn't exists, and apply the icon classes to it.
                if (!$(element).next("span")[0]) {
                    $("<span class='glyphicon glyphicon-ok form-control-feedback'></span>").insertAfter($(element));
                }
            },
            highlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
                $(element).next("span").addClass("glyphicon-remove").removeClass("glyphicon-ok");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
                $(element).next("span").addClass("glyphicon-ok").removeClass("glyphicon-remove");
            },
            submitHandler: function (form) {
                $('.btn_signup_1').html(loader());
                $('.btn_signup_1').attr('disabled', true);
                let data = $(form).serialize();
                let url = $(form).attr('action');
                $.ajax({
                    url: url,
                    method: 'post',
                    data: data,
                    dataType: "json",
                    success: function (response) {
                        $('.btn_signup_1').html('Sign Up');
                        $('.btn_signup_1').attr('disabled', false);
                        if (response.status == 1) {
                            $("#signUpModal_3").modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                            $('#user_id').val(response.data.user_id);
                        }
                        if (response.status == 2) {
                            var ht = '<ul>'
                            $.each(response.errors, function (key, item) {
                                ht += "<li class='alert alert-danger'>" + item + "</li>";
                            });
                            setTimeout(function () {
                                $(".error_signup_1").html('');
                            }, 3000);
                            $(".error_signup_1").html(ht);
                        }
                    }
                });
            }
        });

        $('#signUpForm2').validate({
            rules: {
                first_name: {
                    required: true,
                    // minlength: 5
                },
                last_name: {
                    required: true,
                    // minlength: 5
                },
                username: {
                    required: true,
                    minlength: 5
                },
                date_of_birth: {
                    required: true,
                },
                profile_photo: {
                    required: true,
                    // accept:"image/png"
                },
                region: {
                    required: true,
                },
                country: {
                    required: true,
                },
            },
            messages: {
                first_name: {
                    required: "Please provide first name",
                    // minlength: "Name must be minimum 5 characters long"
                },
                last_name: {
                    required: "Please provide last name",
                    // minlength: "Name must be minimum 5 characters long"
                },
                username: {
                    required: "Please provide username",
                    minlength: "Username must be minimum 5 characters long"
                },
                date_of_birth: "Please provide date of birth",
                profile_photo: {
                    required: "Please provide profile photo",
                    // accept: "Only images of type jpg,png and jpeg are allowed"
                },
                region: "Please provide region",
                country: "Please select country",
                gender: "Please select gender"
            },
            errorElement: "em",
            errorPlacement: function (error, element) {
                // Add the `help-block` class to the error element
                error.addClass("help-block");

                // Add `has-feedback` class to the parent div.form-group
                // in order to add icons to inputs
                element.parents(".col-sm-5").addClass("has-feedback");

                if (element.prop("type") === "checkbox") {
                    error.insertAfter(element.parent("label"));
                } else {
                    error.insertAfter(element);
                }

                // Add the span element, if doesn't exists, and apply the icon classes to it.
                if (!element.next("span")[0]) {
                    $("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").insertAfter(element);
                }
            },
            success: function (label, element) {
                // Add the span element, if doesn't exists, and apply the icon classes to it.
                if (!$(element).next("span")[0]) {
                    $("<span class='glyphicon glyphicon-ok form-control-feedback'></span>").insertAfter($(element));
                }
            },
            function(element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
                $(element).next("span").addClass("glyphicon-remove").removeClass("glyphicon-ok");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
                $(element).next("span").addClass("glyphicon-ok").removeClass("glyphicon-remove");
            },
            submitHandler: function (form) {
                $('.btn_signup_2').html(loader());
                $('.btn_signup_2').attr('disabled', true);
                let url = $(form).attr('action');
                let data = new FormData(document.getElementById('signUpForm2'));
                $.ajax({
                    url: url,
                    data: data,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    dataType: 'json',
                    success: function (response) {
                        $('.btn_signup_2').html('Complete Sign Up');
                        $('.btn_signup_2').attr('disabled', false);
                        if (response.status == 1) {

                            // $('#signUpModal2').modal({
                            //     backdrop: 'static',
                            //     keyboard: false
                            // });
                            if (response.role == 0) {
                                location.reload();
                            } else {
                                window.location.href = "{{route('user.dashboard')}}?is_login=1";
                            }
                        }
                        if (response.status == 0) {
                            var ht = '<ul>';
                            ht += "<li class='alert alert-danger'>" + response.message + "</li>";
                            ht += '</ul>'
                            $(".error_signup_2").html(ht);
                            setTimeout(function () {
                                $(".error_signup_2").html('');
                            }, 3000);
                        }
                    },
                    error: function (errors) {

                        var ht = '<ul>'
                        $.each(errors.responseJSON.errors, function (key, item) {
                            ht += "<li class='alert alert-danger'>" + item + "</li>";
                        });
                        ht += '</ul>';
                        setTimeout(function () {
                            $(".error_signup_2").html('');
                        }, 3000);

                        $(".error_signup_2").html(ht);
                        $('.btn_signup_2').html('Complete Sign Up');
                        $('.btn_signup_2').attr('disabled', false);
                    }
                });
            }
        });

        $("#loginForm").validate({
            rules: {
                password: {
                    required: true,
                    minlength: 6
                },
                email: {
                    required: true,
                    email: true
                },
            },
            messages: {
                // username1: {
                //  required: "Please enter a username",
                //  minlength: "Your username must consist of at least 2 characters"
                // },
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 6 characters long"
                },
                email: "Please enter a valid email address",
                // agree1: "Please accept our policy"
            },
            errorElement: "em",
            errorPlacement: function (error, element) {
                // Add the `help-block` class to the error element
                error.addClass("help-block");

                // Add `has-feedback` class to the parent div.form-group
                // in order to add icons to inputs
                element.parents(".col-sm-5").addClass("has-feedback");

                if (element.prop("type") === "checkbox") {
                    error.insertAfter(element.parent("label"));
                } else {
                    error.insertAfter(element);
                }

                // Add the span element, if doesn't exists, and apply the icon classes to it.
                if (!element.next("span")[0]) {
                    $("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").insertAfter(element);
                }
            },
            success: function (label, element) {
                // Add the span element, if doesn't exists, and apply the icon classes to it.
                if (!$(element).next("span")[0]) {
                    $("<span class='glyphicon glyphicon-ok form-control-feedback'></span>").insertAfter($(element));
                }
            },
            highlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
                $(element).next("span").addClass("glyphicon-remove").removeClass("glyphicon-ok");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
                $(element).next("span").addClass("glyphicon-ok").removeClass("glyphicon-remove");
            },
            submitHandler: function (form) {
                $('.btn_login').html(loader());
                $('.btn_login').attr('disabled', true);
                let data = $(form).serialize();
                let url = $(form).attr('action');
                $.ajax({
                    url: url,
                    method: 'post',
                    data: data,
                    dataType: "json",
                    success: function (response) {
                        if (response.status == 1) {
                            location.reload();
                            // $("#signUpModal2").modal('show');
                            // $('#user_id').val(response.data.user_id);
                        }
                        if (response.status == 0) {
                            var ht = '<ul><li class="alert alert-danger">' + response.message + '</li></ul>';
                            $('.login_errors').html(ht);
                            setTimeout(function () {
                                $(".login_errors").html('');
                            }, 3000);

                        }
                        $('.btn_login').html('Login');
                        $('.btn_login').attr('disabled', false);

                    },
                    errors: function (response) {
                        $('.btn_login').html('Login');
                        $('.btn_login').attr('disabled', false);
                        if (response.status == 2) {
                            var ht = '<ul>'
                            $.each(response.errors, function (key, item) {
                                ht += "<li class='alert alert-danger'>" + item + "</li>";
                            });
                            setTimeout(function () {
                                $(".login_errors").html('');
                            }, 3000);

                            $(".login_errors").html(ht);
                        }
                    }
                });
            }
        });


        $('.btn_signup_role_3').click(function (e) {

            e.preventDefault();
            swal({
                title: "Are you sure that you want to continue as a " + $('.user_role').val(),
                // text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        $(this).html(loader());
                        axios({
                            method: 'get',
                            url: "{{route('user.role')}}?role=" + $('.user_role').val() + "&user_id=" + $('#user_id').val(),
                            responseType: 'json'
                        }).then(function (response) {
                            $('.btn_signup_role_3').html('SignUp');
                            if (response.data.status == 1) {
                                $("#verification-phone-1").modal({
                                    backdrop: 'static',
                                    keyboard: false
                                });
                                var country_val = $('.country_sign_up').val();
                                axios({
                                    method: 'get',
                                    url: "{{route('get-states')}}?id=" + country_val,
                                    responseType: 'json'
                                }).then(function (response) {
                                    var selected = '';
                                    var data = [];
                                    $.each(response.data.states, function (key, item) {
                                        selected += '<option selected="selected" value="' + item.id + '">' + item.name + '</option>';
                                    })
                                    $('.states_sign_up').html(selected).trigger('change');
                                })
                                    .catch(function (error) {
                                    })
                                    .finally(function () {
                                        // $('.add-instructor').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').prop('disabled', false);
                                    });
                                $("#signUpModal_3").modal("hide");
                            }
                        })
                            .catch(function (error) {
                                $('.btn_signup_role_3').html('SignUp');
                            })
                            .finally(function () {
                                $('.btn_signup_role_3').html('SignUp');
                            });
                    } else {
                        $('.btn_signup_role_3').html('SignUp');
                    }
                });

        });

        $('.code_again').click(function () {
            $(this).html(loader());
            $.ajax({
                url: "{{route('user.phone.send-verify')}}",
                type: 'post',
                data: {'id': $('#user_id').val()},
                dataType: 'json',
                success: function (response) {
                    if (response.status == 1) {
                        $('.error_sc2_ss').html('<ul><li class="alert alert-success">Code Send in your phone number</li></ul>');
                        setTimeout(function () {
                            $(".error_sc2_ss").html('');
                        }, 3000);
                        $('.code_again').html('Send code again');
                    }
                },
                error: function (errors) {
                    $('.error_sc2_ss').html('<ul><li class="alert alert-danger">' + errors.responseJSON.message + '</li></ul>')

                    setTimeout(function () {
                        $(".error_sc2_ss").html('');
                    }, 3000);
                    $('.code_again').html('Send code again');
                    // $('.send_code_again').html('Verify');
                }
            });
        });

        $('.btn_phone_chan_ddd').click(function () {
            if ($('.telephone_A').val() == "") {
                $('.err_em').removeClass('hide-display');
                return false;
            }
            var n = $('.iti__selected-dial-code').html();
            n += $('.telephone_A').val();
            $('.btn_phone_chan_ddd').html(loader());
            $.ajax({
                url: "{{route('user.phone.change_number')}}",
                type: 'post',
                data: {'number': n, 'id': $('#user_id').val()},
                dataType: 'json',
                success: function (response) {
                    if (response.status == 1) {
                        $("#verification-phone-1").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $("#verification-phone-2").modal('hide');
                        $('.btn_phone_chan_ddd').html('Confirm');
                    }
                    if (response.status == 2) {
                        var ht = '<ul>'
                        $.each(response.errors, function (key, item) {
                            ht += "<li class='alert alert-danger'>" + item + "</li>";
                        });
                        setTimeout(function () {
                            $(".aaffs_dfdf").html('');
                        }, 3000);

                        $(".aaffs_dfdf").html(ht);
                        $('.btn_phone_chan_ddd').html('Confirm');
                    }
                },
                error: function (errors) {

                    $('.btn_phone_chan_ddd').html('Confirm');
                }
            });
        });
        $('#chang_ph_no').click(function () {
            $("#verification-phone-2").modal({
                backdrop: 'static',
                keyboard: false
            });
            $("#verification-phone-1").modal('hide');
        });
        $('.btn_code_v_ddd').click(function () {
            $(this).html(loader());
            var a = $('.code_name1').val() + $('.code_name2').val() + $('.code_name3').val() + $('.code_name4').val();
            $.ajax({
                url: "{{route('user.phone.verify')}}",
                type: 'post',
                data: {'code': a, 'id': $("#user_id").val()},
                dataType: 'json',
                success: function (response) {
                    if (response.status == 1) {
                        $("#signUpModal2").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $("#verification-phone-1").modal('hide');
                        $("#verification-phone-2").modal('hide');
                    }
                    if (response.status == 0) {
                        $('.error_sc2_ss').html('<ul><li class="alert alert-danger">Your code is Invalid</li></ul>');
                        setTimeout(function () {
                            $(".error_sc2_ss").html('');
                        }, 3000);
                        $('.btn_code_v_ddd').html('Verify');
                    }
                },
                error: function (errors) {
                    $('.error_sc2_ss').html('<ul><li class="alert alert-danger">' + errors.responseJSON.message + '</li></ul>')

                    setTimeout(function () {
                        $(".error_sc2_ss").html('');
                    }, 3000);
                    $('.btn_code_v_ddd').html('Verify');
                }
            });
        })
        $('.btn_skip_modal_verification').click(function () {
            location.reload();
        })


        function changeIMG(e, a) {
            $('.media').removeClass('active');
            $('.user_role').val(a);
            $(e).addClass('active');
        }


        document.querySelector('#profile_photo').addEventListener('change', function (e) {
            let imagePreview = document.getElementById('userImagePreview');
            if (this.value === "") {
                imagePreview.src = "{{asset('assets/images/user-placeholder.png')}}";
            } else {
                imagePreview.src = window.URL.createObjectURL(this.files[0]);
            }
        });

        $(document).on('click', '.gender-selector', function (e) {
            let selectedValue = $(this).attr('data-value');
            $('.gender-selector').removeClass('btn-primary').addClass('btn-light');
            $(this).addClass('btn-primary');
            $('#gender').val(selectedValue);
        });
        // $('.country_sign_up').select2({
        //           placeholder: 'Select an Country',
        //           dropdownParent: $("#signUpModal2")
        // });
        // var states_select = $('.states_sign_up').select2({
        //   placeholder: 'Select an Region',
        //   dropdownParent: $("#signUpModal2")
        // });
        $('.country_sign_up').change(function () {
            axios({
                method: 'get',
                url: "{{route('get-states')}}?id=" + $(this).val(),
                responseType: 'json'
            }).then(function (response) {
                var selected = '';
                var data = [];
                $.each(response.data.states, function (key, item) {
                    selected += '<option selected="selected" value="' + item.id + '">' + item.name + '</option>';
                })
                $('.states_sign_up').html(selected).trigger('change');
            })
                .catch(function (error) {
                })
                .finally(function () {
                    // $('.add-instructor').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').prop('disabled', false);
                });
        });
    });
    $("#signUpModal").on('shown', function () {
        $('.error_signup_1').html('');

    });
    $("#loginModal").on('shown', function () {
        $('.login_errors').html('');

    });
    $("#signUpModal2").on('shown', function () {
        $('.error_signup_2').html('');

    });
    $("#signUpModal_3").on('shown', function () {

    });


    function changeIMG(e, a) {
        $('.media').removeClass('active');
        $('.user_role').val(a);
        $(e).addClass('active');
    }

    var $modal = $('#croppingModal');

    var image = document.getElementById('image');

    var cropper;


    $(document).on("change", "#profile_photo", function (e) {

        var files = e.target.files;
        // console.log(files);
        var done = function (url) {

            image.src = url;
            $modal.modal({
                backdrop: 'static',
                keyboard: false
            });

        };
        var reader;

        var file;

        var url;


        if (files && files.length > 0) {

            file = files[0];


            if (URL) {

                done(URL.createObjectURL(file));

            } else if (FileReader) {

                reader = new FileReader();

                reader.onload = function (e) {

                    done(reader.result);

                };

                reader.readAsDataURL(file);

            }

        }

    });

    $modal.on('shown.bs.modal', function () {
        console.log(image);
        cropper = new Cropper(image, {

            aspectRatio: 1,

            viewMode: 0,
            autoCrop: false,
            preview: '.preview'

        });

    }).on('hidden.bs.modal', function () {
        cropper.destroy();
        cropper = null;
    });
    $('.save_crop').click(function () {
        $modal.modal('hide');
        canvas = cropper.getCroppedCanvas({
            maxWidth: 4096,
            maxHeight: 4096,
        });
        $('#userImagePreview').attr('src', canvas.toDataURL());
        $('#file_canvas').val(canvas.toDataURL());
    });

    @endif


</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-FDDRJCEMWL"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'G-FDDRJCEMWL');
</script>
<!-- Google Tag Manager -->
<script>(function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start':
                new Date().getTime(), event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-T8HCMK9');</script>
<!-- End Google Tag Manager -->

@yield('script')

@include('frontend.users.layout.footer')

</body>
</html>
