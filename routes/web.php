<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/clear-cache', function () {
    $exitCode = Artisan::call('cache:clear');
    return "Cache is cleared";
});

Route::get('/clear-config', function () {
    $exitCode = Artisan::call('config:clear');
    return "Configuration Cache is cleared";
});
Route::get('/clear-route', function () {
    $exitCode = Artisan::call('route:clear');
    return "Route Cache is cleared";
});

Route::get('/clear-view', function () {
    $exitCode = Artisan::call('view:clear');
    return "View Cache is cleared";
});
Route::get("/time", function () {
    return date("h:i:s a", $_SERVER['REQUEST_TIME']);
});

Route::get('scheduler', 'ArtisanController@handle');

Route::get('privacy', 'FrontEndController@privacy')->name('privacy');
Route::get('term-condition', 'FrontEndController@termOfCondition')->name('term-condition');
Route::get('/', 'HomeController@index')->name('home');

Route::resource('albums', 'AlbumsController');
Route::resource('photos', 'PhotosController');

// Socail Logins
Route::get('login/{provider}', 'SocailAuthController@redirectToProvider');
Route::get('login/{provider}/callback', 'SocailAuthController@handleProviderCallback');

Route::get('get-competition-detail', 'UserController@getDetailCompetition')->name('user.get-competition-detail');

Route::get('get-competiton-all/{cat}', 'UserController@show_all')->name('user.show-all');
Route::post('user/do-login', 'UserController@doLogin')->name('user.doLogin');
Route::post('user/verify-code', 'UserController@verifyPhoneNumber')->name('user.verify_phone_number');
Route::get('user/role', 'UserController@userRole')->name('user.role');
Route::post('user/do-register', 'UserController@doregister')->name('user.doRegister');
Route::post('user/add-user-detail', 'UserController@addUserDetail')->name('user.addUserDetail');
Route::get('get-states', 'UserController@getStates')->name('get-states');
Route::get('voting', ['as' => 'voting', 'uses' => 'UserController@votingMedia']);
Route::post('comment', ['as' => 'comment', 'uses' => 'CompetitionController@getCommentPost']);
Route::get('comment_all', ['as' => 'comment_all', 'uses' => 'CompetitionController@getAllCommentPost']);
Route::group(['middleware' => 'auth', 'prefix' => 'user', 'as' => 'user.'], function () {
    Route::get('do-logout', ['as' => 'doLogout', 'uses' => 'UserController@doLogout']);
    Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'UserController@dashboard']);
    Route::post('competitions', ['as' => 'competitions', 'uses' => 'UserController@ShowDatatable']);
    Route::get('profile', ['as' => 'profile', 'uses' => 'UserFollowController@index']);
    Route::post('follow', ['as' => 'follow', 'uses' => 'UserFollowController@followUser']);
    Route::get('getShareableLink', ['as' => 'getShareableLink', 'uses' => 'UserController@getShareableLink']);
    Route::get('getShareableLinkCount', ['as' => 'getShareableLinkCount', 'uses' => 'UserController@getShareableLinkCount']);
    Route::get('getShareableLinkCountAll', ['as' => 'getShareableLinkCountAll', 'uses' => 'UserController@getShareableLinkCountAll']);

    Route::group(['prefix' => 'content', 'as' => 'content.'], function () {
        Route::post('add', ['as' => 'add', 'uses' => 'UserContentController@addContent'])->middleware(['role:user']);
        Route::get('get-genres', ['as' => 'get-genres', 'uses' => 'UserContentController@getGenres']);
        Route::post('add_like', ['as' => 'add_like', 'uses' => 'UserContentController@addUserLikes']);
        Route::post('judge', ['as' => 'judge', 'uses' => 'UserContentController@competitionUserJudge']);
        Route::get('get-user-competition', ['as' => 'get-user-competition', 'uses' => 'UserContentController@getUserCompetition']);
    });
    Route::group(['prefix' => 'competition', 'as' => 'competition.'], function () {
        Route::get('create', ['as' => 'create', 'uses' => 'CompetitionController@index'])->middleware(['role:user|fan|agent|sponsor']);
        Route::get('get-public-competition', ['as' => 'get-public-competition', 'uses' => 'CompetitionController@getPublicCompetition']);
        Route::get('get-public-competition-detail', ['as' => 'get-public-competition-detail', 'uses' => 'CompetitionController@getPublicCompetitionDetail']);
        Route::post('save', ['as' => 'save', 'uses' => 'CompetitionController@saveCompetion'])->middleware(['role:user|fan|agent|sponsor']);
        Route::get('private/join', ['as' => 'private_join', 'uses' => 'CompetitionController@joinPrivate']);
        Route::get('verify-code', ['as' => 'verify_code', 'uses' => 'CompetitionController@codeVerify']);
        Route::post('join_private_competition', ['as' => 'join_private_competition', 'uses' => 'CompetitionController@joinPrivateCompetition']);
        Route::post('join_public_competition', ['as' => 'join_public_competition', 'uses' => 'CompetitionController@joinPublicCompetition']);

    });
    Route::group(['prefix' => 'report', 'as' => 'report.'], function () {
        Route::post('save', ['as' => 'save', 'uses' => 'AbuseReportController@saveReport']);
    });
    Route::get('getAllActiveUsers', ['as' => 'getAllActiveUsers', 'uses' => 'HomeController@getUserDetails']);
});
Route::group(['prefix' => 'phone', 'as' => 'user.phone.'], function () {
    Route::post('send-verify', ['as' => 'send-verify', 'uses' => 'HomeController@sendVerifyCode']);
    Route::post('change_number', ['as' => 'change_number', 'uses' => 'HomeController@changeNumber']);
    Route::post('verify', ['as' => 'verify', 'uses' => 'HomeController@VerifyCode']);
});
/******************* Test Route *************/
Route::get('email-check', ['uses' => 'TestController@emailTest']);
Route::get('phone-test', ['uses' => 'TestController@phoneTest']);
Route::get('account-detail', ['uses' => 'TestController@index']);
Route::get('testing', ['uses' => 'TestController@competitionDetailForTesting']);
Route::get('video-test', ['uses' => 'TestController@videoTest']);
Route::get('pp', ['uses' => 'TestController@pp']);
